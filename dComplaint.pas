unit dComplaint;

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet;

type
  TdmComplaint = class(TDataModule)
    tAdmCompl: TpFIBDataSet;
    dsAdmCompl: TDataSource;
    tAdm1Compl: TpFIBDataSet;
    dsAdm1Compl: TDataSource;
    tG1Compl: TpFIBDataSet;
    dsG1Compl: TDataSource;
    tU1Compl: TpFIBDataSet;
    dsU1Compl: TDataSource;
    tMCompl: TpFIBDataSet;
    dsMCompl: TDataSource;
    tG1ComplID: TFIBIntegerField;
    tG1ComplCASE_ID: TFIBIntegerField;
    tG1ComplREGISTERED_DATE: TFIBDateTimeField;
    tG1ComplDOCUMENT_TYPE_ID: TFIBIntegerField;
    tG1ComplPARTY_TYPE_ID: TFIBIntegerField;
    tG1ComplPARTY_NAME: TFIBStringField;
    tG1ComplPASS_UP_DATE: TFIBDateTimeField;
    tG1ComplCOURT_II_ID: TFIBIntegerField;
    tG1ComplCOURT_II_DATE: TFIBDateTimeField;
    tG1ComplRESULT_II_ID: TFIBIntegerField;
    tG1ComplRETURNED_DATE: TFIBDateTimeField;
    tG1ComplCOMMENTS: TFIBStringField;
    tG1ComplTIMING_UP: TFIBDateTimeField;
    tU1ComplID: TFIBIntegerField;
    tU1ComplCASE_ID: TFIBIntegerField;
    tU1ComplREGISTERED_DATE: TFIBDateTimeField;
    tU1ComplDOCUMENT_TYPE_ID: TFIBIntegerField;
    tU1ComplPARTY_TYPE_ID: TFIBIntegerField;
    tU1ComplPARTY_NAME: TFIBStringField;
    tU1ComplPASS_UP_DATE: TFIBDateTimeField;
    tU1ComplCOURT_II_ID: TFIBIntegerField;
    tU1ComplCOURT_II_DATE: TFIBDateTimeField;
    tU1ComplVERDICT_II_ID: TFIBIntegerField;
    tU1ComplVERDICT_II_ID2: TFIBIntegerField;
    tU1ComplVERDICT_II_ID3: TFIBIntegerField;
    tU1ComplRETURNED_DATE: TFIBDateTimeField;
    tU1ComplCOMMENTS: TFIBStringField;
    tU1ComplTIMING_UP: TFIBDateTimeField;
    tAdmComplID: TFIBIntegerField;
    tAdmComplCASE_ID: TFIBIntegerField;
    tAdmComplREGISTERED_DATE: TFIBDateTimeField;
    tAdmComplDOCUMENT_TYPE_ID: TFIBIntegerField;
    tAdmComplPARTY_TYPE_ID: TFIBIntegerField;
    tAdmComplPART_ID: TFIBIntegerField;
    tAdmComplPASS_UP_DATE: TFIBDateTimeField;
    tAdmComplHIGHER_ECHELON_ID: TFIBIntegerField;
    tAdmComplHIGHER_ECHELON_DATE: TFIBDateTimeField;
    tAdmComplHIGHER_ECHELON_RESULT_ID: TFIBIntegerField;
    tAdmComplRETURNED_DATE: TFIBDateTimeField;
    tAdmComplCOMMENTS: TFIBStringField;
    tAdmComplPARTY_NAME: TFIBStringField;
    tAdmComplEVENT_ID: TFIBIntegerField;
    tAdmComplDOCUMENT_KIND_ID: TFIBIntegerField;
    tAdmComplHIGHER_ECHELON_RESULT_ID2: TFIBIntegerField;
    tAdmComplHIGHER_ECHELON_RESULT_ID3: TFIBIntegerField;
    tAdm1ComplID: TFIBIntegerField;
    tAdm1ComplCASE_ID: TFIBIntegerField;
    tAdm1ComplAPPEAL_DECREE_ID: TFIBIntegerField;
    tAdm1ComplAPPEAL_RESULT_DATE: TFIBDateTimeField;
    tAdm1ComplCOMMENTS: TFIBStringField;
    tAdm1ComplDECLARANT_NAME: TFIBStringField;
    tAdm1ComplDECLARANT_STATUS_ID: TFIBIntegerField;
    tAdm1ComplPASS_UP_DATE: TFIBDateTimeField;
    tAdm1ComplREGISTERED_DATE: TFIBDateTimeField;
    tAdm1ComplRETURNED_DATE: TFIBDateTimeField;
    tAdm1ComplAPPEAL_DECREE_ID2: TFIBIntegerField;
    tAdm1ComplDOCUMENT_KIND_ID: TFIBIntegerField;
    tAdm1ComplAPPEAL_DECREE_ID3: TFIBIntegerField;
    tMComplID: TFIBIntegerField;
    tMComplCASE_ID: TFIBIntegerField;
    tMComplPASS_UP_DATE: TFIBDateTimeField;
    tMComplENTRY_DATE: TFIBDateTimeField;
    tMComplRESULT_UP_ID: TFIBIntegerField;
    tMComplJUDGE_ID: TFIBIntegerField;
    tMComplRESULT_UP_DATE: TFIBDateTimeField;
    tMComplRETURNED_DATE: TFIBDateTimeField;
    tMComplDECLARANT_NAME: TFIBStringField;
    tMComplCOMMENTS: TFIBStringField;
    tComplKind: TpFIBDataSet;
    dsComplKind: TDataSource;
    tComplKindCONTENTID: TFIBIntegerField;
    tComplKindPREFIX: TFIBStringField;
    tComplKindNAME: TFIBStringField;
    tComplType: TpFIBDataSet;
    tComplTypeCONTENTID: TFIBIntegerField;
    tComplTypePREFIX: TFIBStringField;
    tComplTypeNAME: TFIBStringField;
    dsComplType: TDataSource;
  private
    { Private declarations }
    function tComplaint(aCatalog: integer):TpFIBDataSet;
  public
    { Public declarations }
    function NewComplaint(aCatalog: integer): Int64;
    procedure OpenTable(aCatalog: integer);
    procedure CloseTable(aCatalog: integer);
    procedure ComplKindReopen(aCatalog: integer);
  end;

var
  dmComplaint: TdmComplaint;

implementation

uses dMain, fMRLetter;

{$R *.dfm}
procedure TdmComplaint.ComplKindReopen(aCatalog: integer);
var R: string;
begin
  case aCatalog of
    0: R:='CATALOGID = 5130';
    1: R:='CATALOGID = 5131';
    2: R:='CATALOGID = 91031';
    3: R:='CATALOGID = 91031 and CONTENTID <>910310000';
    4: R:='';
  end;
  if R<>'' then tComplKind.ReOpenWP([R]);
end;

function TdmComplaint.tComplaint(aCatalog: integer):TpFIBDataSet;
begin
  case aCatalog of
    0: Result:=tG1Compl;
    1: Result:=tU1Compl;
    2: Result:=tAdmCompl;
    3: Result:=tAdm1Compl;
    4: Result:=tMCompl;
  end;
end;


function TdmComplaint.NewComplaint(aCatalog: integer): Int64;
var aComplaintId: Int64;
begin
  aComplaintId:=dmMain.dbUni.Gen_Id('GLOBAL_GENERATOR', 1);
  case aCatalog of
    0: begin
      tG1Compl.Open;
      tG1Compl.Insert;
      tG1ComplID.Value              :=aComplaintId;
      tG1ComplCASE_ID.Value         :=frmMRLetter.tLetterCASEID.Value;
      tG1ComplREGISTERED_DATE.Value :=frmMRLetter.tLetterLETTERDATE.Value;
      tG1ComplDOCUMENT_TYPE_ID.Value:=51300000;
      tG1ComplPARTY_NAME.Value      :=frmMRLetter.tLetterSENDER.Value;
      tG1Compl.Post;
      tG1Compl.Edit;
    end;
    1: begin
      tU1Compl.Open;
      tU1Compl.Insert;
      tU1ComplID.Value              :=aComplaintId;
      tU1ComplCASE_ID.Value         :=frmMRLetter.tLetterCASEID.Value;
      tU1ComplREGISTERED_DATE.Value :=frmMRLetter.tLetterLETTERDATE.Value;
      tU1ComplDOCUMENT_TYPE_ID.Value:=51310000;
      tU1ComplPARTY_NAME.Value      :=frmMRLetter.tLetterSENDER.Value;
      tU1Compl.Post;
      tU1Compl.Edit;
    end;
    2: begin
      tAdmCompl.Open;
      tAdmCompl.Insert;
      tAdmComplID.Value              :=aComplaintId;
      tAdmComplCASE_ID.Value         :=frmMRLetter.tLetterCASEID.Value;
      tAdmComplREGISTERED_DATE.Value :=frmMRLetter.tLetterLETTERDATE.Value;
      tAdmComplDOCUMENT_TYPE_ID.Value:=40290001;
      tAdmComplDOCUMENT_TYPE_ID.Value:=910310000;
      tAdmComplPARTY_NAME.Value      :=frmMRLetter.tLetterSENDER.Value;
      tAdmComplPARTY_TYPE_ID.Value   :=40210000;
//      tAdmComplPART_ID.Value         :=????�
      tAdmCompl.Post;
      tAdmCompl.Edit;
    end;
    3: begin
      tAdm1Compl.Open;
      tAdm1Compl.Insert;
      tAdm1ComplID.Value                  :=aComplaintId;
      tAdm1ComplCASE_ID.Value             :=frmMRLetter.tLetterCASEID.Value;
      tAdm1ComplREGISTERED_DATE.Value     :=frmMRLetter.tLetterLETTERDATE.Value;
      tAdm1ComplDOCUMENT_KIND_ID.Value    :=910310001;
      tAdm1ComplDECLARANT_NAME.Value      :=frmMRLetter.tLetterSENDER.Value;
      tAdm1ComplDECLARANT_STATUS_ID.Value :=40210000;
      tAdm1Compl.Post;
      tAdm1Compl.Edit;
    end;
    4: begin
      tMCompl.Open;
      tMCompl.Insert;
      tMComplID.Value              :=aComplaintId;
      tMComplCASE_ID.Value         :=frmMRLetter.tLetterCASEID.Value;
      tMComplENTRY_DATE.Value      :=frmMRLetter.tLetterLETTERDATE.Value;
      tMComplDECLARANT_NAME.Value  :=frmMRLetter.tLetterSENDER.Value;
      tMCompl.Post;
      tMCompl.Edit;
    end;
  end;
  Result:=aComplaintId;
end;

procedure TdmComplaint.OpenTable(aCatalog: integer);
begin
  with tComplaint(aCatalog) do
  begin
    Open;
    Edit;
  end;
end;

procedure TdmComplaint.CloseTable(aCatalog: integer);
begin
  with tComplaint(aCatalog) do
  begin
    if State in [dsInsert, dsEdit] then Post;
    Close;
  end;
end;

end.
