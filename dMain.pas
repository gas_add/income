unit dMain;

interface

uses
  SysUtils, Classes, FIBDatabase, pFIBDatabase, DB, FIBDataSet, pFIBDataSet,
  FIBQuery, pFIBQuery, pFIBStoredProc;

type
  TCatalogType = (G1, U1, M, Adm, Adm1);
type
  TdmMain = class(TDataModule)
    dbUni: TpFIBDatabase;
    dbAdd: TpFIBDatabase;
    trMain: TpFIBTransaction;
    tPostType: TpFIBDataSet;
    dsPostType: TDataSource;
    tG1CaseType: TpFIBDataSet;
    dsG1CaseType: TDataSource;
    tG1CaseTypeCONTENTID: TFIBIntegerField;
    tG1CaseTypePREFIX: TFIBStringField;
    tG1CaseTypeNAME: TFIBStringField;
    tUser: TpFIBDataSet;
    dsUser: TDataSource;
    tUserGROUPCONTENTID: TFIBIntegerField;
    tUserUSERNAME: TFIBStringField;
    tUserUSERPSW: TFIBStringField;
    tUserVA_CODE: TFIBStringField;
    spG1_CASE_GBP: TpFIBStoredProc;
    tPARTS_TYPE: TpFIBDataSet;
    dsPARTS_TYPE: TDataSource;
    tPARTS_STATUS: TpFIBDataSet;
    dsPARTS_STATUS: TDataSource;
    tPARTS_TYPECONTENTID: TFIBIntegerField;
    tPARTS_TYPEPREFIX: TFIBStringField;
    tPARTS_TYPENAME: TFIBStringField;
    tPARTS_STATUSCONTENTID: TFIBIntegerField;
    tPARTS_STATUSPREFIX: TFIBStringField;
    tPARTS_STATUSNAME: TFIBStringField;
    qAllUni: TpFIBQuery;
    trAdd: TpFIBTransaction;
    qAllAdd: TpFIBQuery;
    tAutoPostEss: TpFIBDataSet;
    tAutoPostEssID: TFIBIntegerField;
    tAutoPostEssPOSTID: TFIBIntegerField;
    tAutoPostEssPOSTVALUE: TFIBStringField;
    dsAutoPostEss: TDataSource;
    tAutoPostRes: TpFIBDataSet;
    dsAutoPostRes: TDataSource;
    tAutoPostName: TpFIBDataSet;
    dsAutoPostName: TDataSource;
    tAutoPostNameID: TFIBIntegerField;
    tAutoPostNamePOSTID: TFIBIntegerField;
    tAutoPostNamePOSTVALUE: TFIBStringField;
    tAutoPostResID: TFIBIntegerField;
    tAutoPostResPOSTID: TFIBIntegerField;
    tAutoPostResPOSTVALUE: TFIBStringField;
    tCategoryList: TpFIBDataSet;
    dsCategoryList: TDataSource;
    tCategoryListCONTENTID: TFIBIntegerField;
    tCategoryListPREFIX: TFIBStringField;
    tCategoryListNAME: TFIBStringField;
    tDocType: TpFIBDataSet;
    tDocTypeOBJECTID: TFIBIntegerField;
    tDocTypeCODE: TFIBStringField;
    tDocTypeDOCTYPENAME: TFIBStringField;
    tDocTypeARCHFLAG: TFIBIntegerField;
    tDocTypeORIGINALTYPE: TFIBIntegerField;
    tDocTypePOSTTYPE: TFIBIntegerField;
    tDocTypeFORM: TFIBIntegerField;
    tDocTypeUNIDOCTYPE: TFIBIntegerField;
    dsDocType: TDataSource;
    tSetting: TpFIBDataSet;
    dsSetting: TDataSource;
    tSettingVERS: TFIBIntegerField;
    tOutJournal: TpFIBDataSet;
    dsOutJournal: TDataSource;
    tPostTypeOBJECTID: TFIBIntegerField;
    tPostTypePOSTTYPENAME: TFIBStringField;
    tPostTypeARCHIVED: TFIBSmallIntField;
    tPostTypeJOURNALID: TFIBSmallIntField;
    tPostTypePOSTFORMID: TFIBSmallIntField;
    tPostTypeADDTOCASE: TFIBSmallIntField;
    tCategory2: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBStringField1: TFIBStringField;
    FIBStringField2: TFIBStringField;
    dsCategory2: TDataSource;
    procedure DataModuleCreate(Sender: TObject);

  private
    { Private declarations }
    procedure UpdateBase;
  public
    { Public declarations }
    UserId: Int64;
    function Catalog(CardId: integer): TCatalogType;
    function GetCatalogName(CardId: integer): string;
    function ColFullCaseNum(CardId: integer): string;
    function ColShortCaseNum(CardId: integer): string;
    function ColYear(CardId: integer): string;
    procedure InitDB;
    function IsAdmin: boolean;
    function GetPostTypePrefix(aPostTypeId: integer): string;
    procedure PARTS_TYPEReopen(aCatalog: integer);
    function GetConfigValue(akey: string): string;
  end;

var
  dmMain: TdmMain;
  Scanner: integer;
  Sequentially: integer;

implementation

{$R *.dfm}

function TdmMain.GetConfigValue(aKey: string): string;
var
  lConfig: TStringList;
begin
  lConfig:=TStringList.Create;
  lConfig.LoadFromFile('config.ini');
  Result := lConfig.Values[aKey];
end;

procedure TdmMain.PARTS_TYPEReopen(aCatalog: integer);
var R: integer;
begin
  case aCatalog of
    0: R:=5071;
    1: R:=5085;
    2,3: R:=4021;
    4: R:=0;
  end;
  if R>0 then tPARTS_TYPE.ReOpenWP([R]);
end;   

function TdmMain.IsAdmin: boolean;
begin
  Result:=(dmMain.UserId=3025);
end;

function TdmMain.Catalog(CardId: integer): TCatalogType;
begin
  Case CardId of
    14:  Result:=G1;
    15:  Result:=U1;
    20:  Result:=M;
    12:  Result:=Adm;
    24:  Result:=Adm1;
    else  Result:=G1;
  end;
end;

function TdmMain.GetCatalogName(CardId: integer): string;
begin
  Case CardId of
    14:  Result:='G1';
    15:  Result:='U1';
    20:  Result:='M';
    12:  Result:='Adm';
    24:  Result:='Adm1';
  end;
end;

function TdmMain.ColFullCaseNum(CardId: integer): string;
begin
  Case Catalog(CardId) of
    G1:   Result:='FULL_NUMBER';
    U1:   Result:='FULL_NUMBER';
    M:    Result:='FULL_NUMBER';
    Adm:  Result:='CASE_NUMBER';
    Adm1: Result:='CASE_NUMBER';
  end;
end;

function TdmMain.ColShortCaseNum(CardId: integer): string;
begin
  Case Catalog(CardId) of
    G1:   Result:='SHORT_NUMBER';
    U1:   Result:='SHORT_NUMBER';
    M:    Result:='REG_NUMBER';
    Adm:  Result:='ORDINAL_NUMBER';
    Adm1: Result:='ORDINAL_NUMBER';
  end;
end;

function TdmMain.ColYear(CardId: integer): string;
begin
  Case Catalog(CardId) of
    G1:   Result:='YEAR_REG';
    U1:   Result:='YEAR_REG';
    M:    Result:='REG_YEAR';
    Adm:  Result:='REG_YEAR';
    Adm1: Result:='REG_YEAR';
  end;
end;

procedure TdmMain.InitDB;
var
  lConfig: TStringList;
begin
  dbUni.Connected:=false;
  dbAdd.Connected:=false;
  trMain.Active:=false;
  trAdd.Active:=false;

  lConfig:=TStringList.Create;
  lConfig.LoadFromFile('config.ini');
  dbUni.DBName:=lConfig.Values['DB'];
  dbAdd.DBName:=lConfig.Values['DBADD'];
  Scanner:= StrToInt(lConfig.Values['Scanner']);
  Sequentially := StrToInt(lConfig.Values['Sequentially']);

  dbUni.Connected:=true;
  dbAdd.Connected:=true;
  trMain.Active:=true;
  trAdd.Active:=true;
end;

procedure TdmMain.UpdateBase;
begin
  try
    tSetting.Open;
  except
    qAllAdd.SQL.Text:='CREATE TABLE SETTINGS (VERS INTEGER)';
    qAllAdd.ExecWP([]);
    qAllAdd.SQL.Text:='insert into SETTINGS (VERS) values (1)';
    qAllAdd.ExecWP([]);
    qAllAdd.SQL.Text:='ALTER TABLE LETTER ADD INTYPE SMALLINT';
    qAllAdd.ExecWP([]);
    dbAdd.CommitRetaining;
  end;
  tSetting.Close;

    
end;

procedure TdmMain.DataModuleCreate(Sender: TObject);
begin
  InitDB;
  tPostType.Open;
//  tDocType.Open;
  tG1CaseType.Open;
  tUser.Open;
//  tPARTS_TYPE.OpenWP;
  tPARTS_STATUS.Open;
  tAutoPostEss.Open;
  tAutoPostRes.Open;
  tAutoPostName.Open;
  if Scanner=1 then tDocType.Open;
  UpdateBase;
end;

function TdmMain.GetPostTypePrefix(aPostTypeId: integer): string;
begin
  qAllAdd.SQL.Text:='select j.code from posttype p left join journal j on (p.journalid=j.objectid) where p.OBJECTID='+ IntToStr(aPostTypeId);
  qAllAdd.ExecQuery;
  Result:=qAllAdd.Fields[0].AsString;
  qAllAdd.Close;
end;

end.
