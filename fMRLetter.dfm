object frmMRLetter: TfrmMRLetter
  Left = 206
  Top = 165
  ActiveControl = vCaseType
  Caption = 'frmMRLetter'
  ClientHeight = 453
  ClientWidth = 852
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object JvWizard1: TJvWizard
    Left = 0
    Top = 0
    Width = 852
    Height = 453
    ActivePage = JvWizardInteriorPage2
    ButtonBarHeight = 42
    ButtonStart.Caption = 'To &Start Page'
    ButtonStart.NumGlyphs = 1
    ButtonStart.Width = 85
    ButtonLast.Caption = 'To &Last Page'
    ButtonLast.NumGlyphs = 1
    ButtonLast.Width = 85
    ButtonBack.Caption = '< &'#1053#1072#1079#1072#1076
    ButtonBack.NumGlyphs = 1
    ButtonBack.Width = 75
    ButtonNext.Caption = '&'#1044#1072#1083#1077#1077' >'
    ButtonNext.NumGlyphs = 1
    ButtonNext.Width = 75
    ButtonFinish.Caption = '&'#1047#1072#1074#1077#1088#1096#1080#1090#1100
    ButtonFinish.NumGlyphs = 1
    ButtonFinish.ModalResult = 1
    ButtonFinish.Width = 75
    ButtonCancel.Caption = #1054#1090#1084#1077#1085#1072
    ButtonCancel.NumGlyphs = 1
    ButtonCancel.ModalResult = 2
    ButtonCancel.Width = 75
    ButtonHelp.Caption = '&Help'
    ButtonHelp.NumGlyphs = 1
    ButtonHelp.Width = 75
    ShowRouteMap = True
    OnCancelButtonClick = JvWizard1CancelButtonClick
    DesignSize = (
      852
      453)
    object JvWizardInteriorPage1: TJvWizardInteriorPage
      Header.ParentFont = False
      Header.Title.Color = clNone
      Header.Title.Text = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1103' '#1074#1093#1086#1076#1103#1097#1077#1075#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Header.Title.Anchors = [akLeft, akTop, akRight]
      Header.Title.Font.Charset = DEFAULT_CHARSET
      Header.Title.Font.Color = clWindowText
      Header.Title.Font.Height = -16
      Header.Title.Font.Name = 'MS Sans Serif'
      Header.Title.Font.Style = [fsBold]
      Header.Subtitle.Color = clNone
      Header.Subtitle.Text = #1054#1089#1085#1086#1074#1085#1099#1077' '#1088#1077#1082#1074#1080#1079#1080#1090#1099
      Header.Subtitle.Anchors = [akLeft, akTop, akRight, akBottom]
      Header.Subtitle.Font.Charset = DEFAULT_CHARSET
      Header.Subtitle.Font.Color = clWindowText
      Header.Subtitle.Font.Height = -11
      Header.Subtitle.Font.Name = 'MS Sans Serif'
      Header.Subtitle.Font.Style = []
      VisibleButtons = [bkNext, bkCancel]
      Caption = #1054#1089#1085#1086#1074#1085#1099#1077' '#1088#1077#1082#1074#1080#1079#1080#1090#1099
      OnNextButtonClick = JvWizardInteriorPage1NextButtonClick
      object Panel5: TPanel
        Left = 0
        Top = 70
        Width = 675
        Height = 341
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label8: TLabel
          Left = 10
          Top = 14
          Width = 97
          Height = 13
          Caption = #1044#1072#1090#1072' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080'*'
        end
        object Label1: TLabel
          Left = 10
          Top = 37
          Width = 151
          Height = 13
          Caption = #1044#1072#1090#1072' '#1086#1090#1087#1088#1072#1074#1082#1080' '#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1077#1084
        end
        object Label9: TLabel
          Left = 303
          Top = 14
          Width = 130
          Height = 13
          Caption = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1086#1085#1085#1099#1081' '#1085#1086#1084#1077#1088'*'
        end
        object Label10: TLabel
          Left = 303
          Top = 37
          Width = 139
          Height = 13
          Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1081' '#8470' '#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1103
        end
        object Label4: TLabel
          Left = 11
          Top = 83
          Width = 116
          Height = 13
          Caption = #1042#1080#1076' '#1082#1086#1088#1088#1077#1089#1087#1086#1085#1076#1077#1085#1094#1080#1080'*'
        end
        object Label6: TLabel
          Left = 11
          Top = 106
          Width = 70
          Height = 13
          Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100'*'
        end
        object Label32: TLabel
          Left = 11
          Top = 130
          Width = 54
          Height = 13
          Caption = #1055#1086#1089#1090#1091#1087#1080#1083#1086
        end
        object Label33: TLabel
          Left = 11
          Top = 59
          Width = 40
          Height = 13
          Caption = #1046#1091#1088#1085#1072#1083
        end
        object lJournal: TLabel
          Left = 175
          Top = 59
          Width = 45
          Height = 13
          Caption = 'lJournal'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lCaseID: TLabel
          Left = 11
          Top = 154
          Width = 48
          Height = 13
          Caption = #1042' '#1076#1077#1083#1086' '#8470
        end
        object vDateReg: TJvDBDatePickerEdit
          Left = 174
          Top = 10
          Width = 99
          Height = 21
          AllowNoDate = True
          DataField = 'LETTERDATE'
          DataSource = dsLetter
          TabOrder = 0
        end
        object vRegNumber: TDBEdit
          Left = 448
          Top = 10
          Width = 59
          Height = 21
          DataField = 'NUMBER'
          DataSource = dsLetter
          TabOrder = 2
        end
        object cbAutoNum: TCheckBox
          Left = 510
          Top = 12
          Width = 161
          Height = 17
          Caption = #1055#1088#1080#1089#1074#1086#1080#1090#1100' '#1072#1074#1090#1086#1084#1072#1090#1080#1095#1077#1089#1082#1080
          Checked = True
          State = cbChecked
          TabOrder = 1
          OnClick = cbAutoNumClick
        end
        object DBEdit2: TDBEdit
          Left = 448
          Top = 33
          Width = 224
          Height = 21
          DataField = 'INCNUMBER'
          DataSource = dsLetter
          TabOrder = 4
        end
        object DBEdit3: TDBEdit
          Left = 174
          Top = 103
          Width = 475
          Height = 21
          DataField = 'SENDER'
          DataSource = dsLetter
          TabOrder = 6
        end
        object vPostType: TJvDBLookupCombo
          Left = 174
          Top = 80
          Width = 499
          Height = 21
          DataField = 'POSTTYPE'
          DataSource = dsLetter
          LookupField = 'OBJECTID'
          LookupDisplay = 'POSTTYPENAME'
          LookupSource = dmMain.dsPostType
          TabOrder = 5
          OnChange = vPostTypeChange
        end
        object JvDBDateEdit1: TJvDBDateEdit
          Left = 174
          Top = 33
          Width = 99
          Height = 21
          DataField = 'OUTDATE'
          DataSource = dsLetter
          ShowNullDate = False
          TabOrder = 3
        end
        object Button3: TButton
          Left = 651
          Top = 102
          Width = 21
          Height = 21
          Caption = '...'
          TabOrder = 7
          OnClick = Button3Click
        end
        object JvDBComboBox1: TJvDBComboBox
          Left = 174
          Top = 126
          Width = 499
          Height = 21
          DataField = 'INTYPE'
          DataSource = dsLetter
          Items.Strings = (
            #1087#1086' '#1087#1086#1095#1090#1077
            #1085#1072' '#1083#1080#1095#1085#1086#1084' '#1087#1088#1080#1077#1084#1077
            #1082#1091#1088#1100#1077#1088#1086#1084
            #1087#1086' '#1101#1083#1077#1082#1090#1088#1086#1085#1085#1086#1081' '#1087#1086#1095#1090#1077
            #1087#1086' '#1092#1072#1082#1089#1091
            #1090#1077#1083#1077#1092#1086#1085#1086#1075#1088#1072#1084#1084#1072)
          TabOrder = 8
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5')
          ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
          ListSettings.OutfilteredValueFont.Color = clRed
          ListSettings.OutfilteredValueFont.Height = -11
          ListSettings.OutfilteredValueFont.Name = 'Tahoma'
          ListSettings.OutfilteredValueFont.Style = []
        end
        object bCaseId: TButton
          Left = 651
          Top = 148
          Width = 21
          Height = 21
          Caption = '...'
          TabOrder = 11
        end
        object eCaseId: TEdit
          Left = 174
          Top = 149
          Width = 451
          Height = 21
          TabOrder = 9
          Text = 'eCaseId'
        end
        object bCaseId2: TButton
          Left = 628
          Top = 148
          Width = 21
          Height = 21
          Caption = '->'
          TabOrder = 10
          OnClick = bCaseId2Click
        end
      end
    end
    object JvWizardInteriorPage2: TJvWizardInteriorPage
      Header.ParentFont = False
      Header.Title.Color = clNone
      Header.Title.Text = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1103' '#1074#1093#1086#1076#1103#1097#1077#1075#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Header.Title.Anchors = [akLeft, akTop, akRight]
      Header.Title.Font.Charset = DEFAULT_CHARSET
      Header.Title.Font.Color = clWindowText
      Header.Title.Font.Height = -16
      Header.Title.Font.Name = 'MS Sans Serif'
      Header.Title.Font.Style = [fsBold]
      Header.Subtitle.Color = clNone
      Header.Subtitle.Text = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1103' '#1088#1077#1082#1074#1080#1079#1080#1090#1086#1074' '#1076#1077#1083#1072
      Header.Subtitle.Anchors = [akLeft, akTop, akRight, akBottom]
      Header.Subtitle.Font.Charset = DEFAULT_CHARSET
      Header.Subtitle.Font.Color = clWindowText
      Header.Subtitle.Font.Height = -11
      Header.Subtitle.Font.Name = 'MS Sans Serif'
      Header.Subtitle.Font.Style = []
      EnabledButtons = [bkStart, bkLast, bkNext, bkFinish, bkCancel, bkHelp]
      Caption = #1056#1077#1082#1074#1080#1079#1080#1090#1099' '#1076#1077#1083#1072
      OnNextButtonClick = JvWizardInteriorPage2NextButtonClick
      object pCaseIfo: TPanel
        Left = 0
        Top = 70
        Width = 675
        Height = 341
        Align = alClient
        TabOrder = 0
        object Label7: TLabel
          Left = 6
          Top = 4
          Width = 145
          Height = 13
          Caption = #1042#1080#1076' '#1075#1088#1072#1078#1076'-'#1075#1086' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072'*'
        end
        object Label11: TLabel
          Left = 6
          Top = 78
          Width = 54
          Height = 13
          Caption = #1057#1091#1097#1085#1086#1089#1090#1100'*'
        end
        object Label13: TLabel
          Left = 6
          Top = 101
          Width = 61
          Height = 13
          Caption = #1057#1091#1084#1084#1072' '#1080#1089#1082#1072
        end
        object Label14: TLabel
          Left = 6
          Top = 124
          Width = 46
          Height = 13
          Caption = #1055#1086#1096#1083#1080#1085#1072
        end
        object Label15: TLabel
          Left = 6
          Top = 170
          Width = 30
          Height = 13
          Caption = #1057#1091#1076#1100#1103
        end
        object Label16: TLabel
          Left = 6
          Top = 193
          Width = 87
          Height = 13
          Caption = #1055#1086#1084#1086#1097#1085#1080#1082' '#1089#1091#1076#1100#1080
        end
        object Label17: TLabel
          Left = 6
          Top = 216
          Width = 54
          Height = 13
          Caption = #1057#1077#1082#1088#1077#1090#1072#1088#1100
        end
        object Label2: TLabel
          Left = 6
          Top = 147
          Width = 88
          Height = 13
          Caption = #1055#1086#1096#1083#1080#1085#1091' '#1091#1087#1083#1072#1090#1080#1083
        end
        object Label5: TLabel
          Left = 6
          Top = 56
          Width = 107
          Height = 13
          Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103' '#1091#1090#1086#1095#1085#1077#1085#1080#1077
        end
        object Label50: TLabel
          Left = 6
          Top = 27
          Width = 53
          Height = 13
          Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
        end
        object vCaseType: TJvDBLookupCombo
          Left = 169
          Top = 0
          Width = 499
          Height = 21
          DataField = 'CASE_TYPE_ID'
          DataSource = dsCase
          LookupField = 'CONTENTID'
          LookupDisplay = 'NAME'
          LookupSource = dmMain.dsG1CaseType
          TabOrder = 0
        end
        object JvDBLookupCombo6: TJvDBLookupCombo
          Left = 169
          Top = 166
          Width = 499
          Height = 21
          DataField = 'JUDGE_ID'
          DataSource = dsCase
          LookupField = 'GROUPCONTENTID'
          LookupDisplay = 'USERNAME'
          LookupSource = dmMain.dsUser
          TabOrder = 6
          OnCloseUp = JvDBLookupComboUserCloseUp
          OnDropDown = JvDBLookupComboUserDropDown
        end
        object JvDBLookupCombo7: TJvDBLookupCombo
          Left = 169
          Top = 189
          Width = 499
          Height = 21
          DataField = 'ADJUTANT_ID'
          DataSource = dsCase
          LookupField = 'GROUPCONTENTID'
          LookupDisplay = 'USERNAME'
          LookupSource = dmMain.dsUser
          TabOrder = 7
          OnCloseUp = JvDBLookupComboUserCloseUp
          OnDropDown = JvDBLookupComboUserDropDown
        end
        object JvDBLookupCombo8: TJvDBLookupCombo
          Left = 169
          Top = 212
          Width = 499
          Height = 21
          DataField = 'CLERK'
          DataSource = dsCase
          LookupField = 'GROUPCONTENTID'
          LookupDisplay = 'USERNAME'
          LookupSource = dmMain.dsUser
          TabOrder = 8
          OnCloseUp = JvDBLookupComboUserCloseUp
          OnDropDown = JvDBLookupComboUserDropDown
        end
        object DBEdit5: TDBEdit
          Left = 169
          Top = 97
          Width = 499
          Height = 21
          DataField = 'SUIT_MONEY'
          DataSource = dsClaim
          TabOrder = 3
        end
        object DBEdit6: TDBEdit
          Left = 169
          Top = 120
          Width = 499
          Height = 21
          DataField = 'DUTY'
          DataSource = dsCase
          TabOrder = 4
          OnExit = DBEdit6Exit
        end
        object JvDBLookupComboEdit2: TJvDBLookupComboEdit
          Left = 169
          Top = 74
          Width = 499
          Height = 21
          LookupDisplay = 'POSTVALUE'
          LookupField = 'POSTVALUE'
          LookupSource = dmMain.dsAutoPostEss
          TabOrder = 2
          Text = 'JvDBLookupComboEdit2'
          UseRecordCount = True
          OnKeyDown = JvDBLookupComboEdit2KeyDown
          DataField = 'REQUIREMENTS'
          DataSource = dsClaim
        end
        object DBEdit1: TDBEdit
          Left = 169
          Top = 143
          Width = 499
          Height = 21
          DataField = 'DUTY_WHO'
          DataSource = dsCase
          TabOrder = 5
        end
        object JvDBLookupCombo4: TJvDBLookupCombo
          Left = 169
          Top = 47
          Width = 499
          Height = 21
          DisplayAllFields = True
          DataField = 'CATEGORY_ID2'
          DataSource = dsClaim
          LookupField = 'CONTENTID'
          LookupDisplay = 'NAME'
          LookupSource = dmMain.dsCategory2
          RightTrimmedLookup = True
          TabOrder = 1
          OnDropDown = Catalog2Reopen
        end
        object JvDBLookupCombo17: TJvDBLookupCombo
          Left = 169
          Top = 24
          Width = 499
          Height = 21
          DropDownCount = 20
          DropDownWidth = 800
          DisplayAllFields = True
          DataField = 'CATEGORY_ID'
          DataSource = dsClaim
          LookupField = 'CONTENTID'
          LookupDisplay = 'PREFIX; NAME'
          LookupSource = dmMain.dsCategoryList
          RightTrimmedLookup = True
          TabOrder = 9
          OnDropDown = CatalogListReopen
        end
      end
    end
    object JvWizardInteriorPage3: TJvWizardInteriorPage
      Header.ParentFont = False
      Header.Title.Color = clNone
      Header.Title.Text = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1103' '#1074#1093#1086#1076#1103#1097#1077#1075#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Header.Title.Anchors = [akLeft, akTop, akRight]
      Header.Title.Font.Charset = DEFAULT_CHARSET
      Header.Title.Font.Color = clWindowText
      Header.Title.Font.Height = -16
      Header.Title.Font.Name = 'MS Sans Serif'
      Header.Title.Font.Style = [fsBold]
      Header.Subtitle.Color = clNone
      Header.Subtitle.Text = #1057#1090#1086#1088#1086#1085#1099' '#1087#1086' '#1076#1077#1083#1091
      Header.Subtitle.Anchors = [akLeft, akTop, akRight, akBottom]
      Header.Subtitle.Font.Charset = DEFAULT_CHARSET
      Header.Subtitle.Font.Color = clWindowText
      Header.Subtitle.Font.Height = -11
      Header.Subtitle.Font.Name = 'MS Sans Serif'
      Header.Subtitle.Font.Style = []
      EnabledButtons = [bkStart, bkLast, bkNext, bkFinish, bkCancel, bkHelp]
      Caption = #1057#1090#1086#1088#1086#1085#1099' '#1087#1086' '#1076#1077#1083#1091
      OnEnterPage = JvWizardInteriorPage3EnterPage
      OnNextButtonClick = JvWizardInteriorPage3NextButtonClick
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 298
        Width = 675
        Height = 113
        Align = alClient
        DataSource = dsPart
        DynProps = <>
        FooterParams.Color = clWindow
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 2
        Columns = <
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'Parts_type'
            Footers = <>
            Title.Caption = #1042#1080#1076' '#1083#1080#1094#1072
            Width = 169
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'NAME'
            Footers = <>
            Title.Caption = #1060#1048#1054
            Width = 480
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 70
        Width = 675
        Height = 203
        Align = alTop
        TabOrder = 0
        object Label3: TLabel
          Left = 11
          Top = 9
          Width = 46
          Height = 13
          Caption = #1042#1080#1076' '#1083#1080#1094#1072
        end
        object Label12: TLabel
          Left = 11
          Top = 53
          Width = 108
          Height = 13
          Caption = #1060#1048#1054'/'#1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        end
        object Label18: TLabel
          Left = 11
          Top = 75
          Width = 133
          Height = 13
          Caption = #1040#1076#1088#1077#1089' ('#1052#1077#1089#1090#1086#1085#1072#1093#1086#1078#1076#1077#1085#1080#1077')'
        end
        object Label19: TLabel
          Left = 11
          Top = 178
          Width = 106
          Height = 13
          Caption = #1060#1048#1054' '#1087#1088#1077#1076#1089#1090#1072#1074#1080#1090#1077#1083#1103
        end
        object Label20: TLabel
          Left = 11
          Top = 31
          Width = 61
          Height = 13
          Caption = #1057#1090#1072#1090#1091#1089' '#1083#1080#1094#1072
        end
        object Label48: TLabel
          Left = 11
          Top = 134
          Width = 45
          Height = 13
          Caption = #1058#1077#1083#1077#1092#1086#1085
        end
        object Label49: TLabel
          Left = 11
          Top = 156
          Width = 97
          Height = 13
          Caption = #1069#1083#1077#1082#1090#1088#1086#1085#1085#1072#1103' '#1087#1086#1095#1090#1072
        end
        object Label51: TLabel
          Left = 11
          Top = 115
          Width = 43
          Height = 13
          Caption = #1055#1072#1089#1087#1086#1088#1090
        end
        object JvDBLookupCombo1: TJvDBLookupCombo
          Left = 174
          Top = 5
          Width = 355
          Height = 21
          DataField = 'PARTS_TYPE_ID'
          DataSource = dsPart
          LookupField = 'CONTENTID'
          LookupDisplay = 'NAME'
          LookupSource = dmMain.dsPARTS_TYPE
          TabOrder = 0
        end
        object DBEdit4: TDBEdit
          Left = 174
          Top = 49
          Width = 499
          Height = 21
          DataField = 'NAME'
          DataSource = dsPart
          TabOrder = 2
        end
        object DBEdit8: TDBEdit
          Left = 174
          Top = 174
          Width = 499
          Height = 21
          DataField = 'REPRESENT'
          DataSource = dsPart
          TabOrder = 6
        end
        object DBMemo1: TDBMemo
          Left = 174
          Top = 71
          Width = 499
          Height = 36
          DataField = 'ADDRESS'
          DataSource = dsPart
          TabOrder = 3
        end
        object JvDBLookupCombo3: TJvDBLookupCombo
          Left = 174
          Top = 27
          Width = 499
          Height = 21
          DataField = 'PARTS_STATUS_ID'
          DataSource = dsPart
          LookupField = 'CONTENTID'
          LookupDisplay = 'NAME'
          LookupSource = dmMain.dsPARTS_STATUS
          TabOrder = 1
        end
        object Button1: TButton
          Left = 560
          Top = 2
          Width = 112
          Height = 25
          Caption = #1042#1099#1073#1088#1072#1090#1100' '#1080#1079' '#1082#1072#1090#1072#1083#1086#1075#1072
          TabOrder = 7
          OnClick = Button1Click
        end
        object eSMS_TELNUM: TDBEdit
          Left = 174
          Top = 130
          Width = 499
          Height = 21
          DataField = 'SMS_TELNUM'
          DataSource = dsPart
          TabOrder = 4
        end
        object DBEdit16: TDBEdit
          Left = 174
          Top = 152
          Width = 499
          Height = 21
          DataField = 'EMAIL_STR'
          DataSource = dsPart
          TabOrder = 5
        end
        object ePassport: TDBEdit
          Left = 174
          Top = 108
          Width = 499
          Height = 21
          DataField = 'PASSPORT'
          DataSource = dsPart
          TabOrder = 8
        end
      end
      object DBNavigator1: TDBNavigator
        Left = 0
        Top = 273
        Width = 675
        Height = 25
        DataSource = dsPart
        VisibleButtons = [nbInsert, nbPost, nbCancel, nbRefresh]
        Align = alTop
        TabOrder = 1
      end
    end
    object wiComplaint: TJvWizardInteriorPage
      Header.ParentFont = False
      Header.Title.Color = clNone
      Header.Title.Text = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1103' '#1074#1093#1086#1076#1103#1097#1077#1075#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Header.Title.Anchors = [akLeft, akTop, akRight]
      Header.Title.Font.Charset = DEFAULT_CHARSET
      Header.Title.Font.Color = clWindowText
      Header.Title.Font.Height = -16
      Header.Title.Font.Name = 'MS Sans Serif'
      Header.Title.Font.Style = [fsBold]
      Header.Subtitle.Color = clNone
      Header.Subtitle.Text = #1056#1077#1082#1074#1080#1079#1080#1090#1099' '#1078#1072#1083#1086#1073#1099
      Header.Subtitle.Anchors = [akLeft, akTop, akRight, akBottom]
      Header.Subtitle.Font.Charset = DEFAULT_CHARSET
      Header.Subtitle.Font.Color = clWindowText
      Header.Subtitle.Font.Height = -11
      Header.Subtitle.Font.Name = 'MS Sans Serif'
      Header.Subtitle.Font.Style = []
      Caption = #1046#1072#1083#1086#1073#1072' '#1087#1086' '#1076#1077#1083#1091
      OnEnterPage = wiComplaintEnterPage
      OnExitPage = wiComplaintExitPage
      OnNextButtonClick = wiComplaintNextButtonClick
      object pcComplaint: TJvPageControl
        Left = 0
        Top = 70
        Width = 675
        Height = 341
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        HideAllTabs = True
        object TabSheet1: TTabSheet
          Caption = 'TabSheet1'
          object Label34: TLabel
            Left = 11
            Top = 8
            Width = 152
            Height = 13
            Caption = #1042#1080#1076' '#1087#1088#1077#1076#1089#1090#1072#1074#1083#1077#1085#1080#1103' ('#1078#1072#1083#1086#1073#1099')*'
          end
          object Label35: TLabel
            Left = 11
            Top = 37
            Width = 166
            Height = 13
            Caption = #1060#1080#1086' ('#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077') '#1079#1072#1103#1074#1080#1090#1077#1083#1103'*'
          end
          object Label36: TLabel
            Left = 11
            Top = 66
            Width = 158
            Height = 13
            Caption = #1055#1088#1077#1076#1089#1090#1072#1074#1083#1077#1085#1080#1077' ('#1078#1072#1083#1086#1073#1091') '#1087#1086#1076#1072#1083
          end
          object JvDBLookupCombo5: TJvDBLookupCombo
            Left = 184
            Top = 4
            Width = 489
            Height = 21
            DataField = 'DOCUMENT_TYPE_ID'
            DataSource = dmComplaint.dsG1Compl
            LookupField = 'CONTENTID'
            LookupDisplay = 'NAME'
            LookupSource = dmComplaint.dsComplKind
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 184
            Top = 33
            Width = 489
            Height = 21
            DataField = 'PARTY_NAME'
            DataSource = dmComplaint.dsG1Compl
            TabOrder = 1
          end
          object JvDBLookupCombo9: TJvDBLookupCombo
            Left = 184
            Top = 62
            Width = 488
            Height = 21
            DataField = 'PARTY_TYPE_ID'
            DataSource = dmComplaint.dsG1Compl
            LookupField = 'CONTENTID'
            LookupDisplay = 'NAME'
            LookupSource = dmMain.dsPARTS_TYPE
            TabOrder = 2
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'TabSheet2'
          ImageIndex = 1
          object Label37: TLabel
            Left = 11
            Top = 8
            Width = 152
            Height = 13
            Caption = #1042#1080#1076' '#1087#1088#1077#1076#1089#1090#1072#1074#1083#1077#1085#1080#1103' ('#1078#1072#1083#1086#1073#1099')*'
          end
          object Label38: TLabel
            Left = 11
            Top = 37
            Width = 166
            Height = 13
            Caption = #1060#1080#1086' ('#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077') '#1079#1072#1103#1074#1080#1090#1077#1083#1103'*'
          end
          object Label39: TLabel
            Left = 11
            Top = 66
            Width = 158
            Height = 13
            Caption = #1055#1088#1077#1076#1089#1090#1072#1074#1083#1077#1085#1080#1077' ('#1078#1072#1083#1086#1073#1091') '#1087#1086#1076#1072#1083
          end
          object JvDBLookupCombo10: TJvDBLookupCombo
            Left = 184
            Top = 4
            Width = 489
            Height = 21
            DataField = 'DOCUMENT_TYPE_ID'
            DataSource = dmComplaint.dsU1Compl
            LookupField = 'CONTENTID'
            LookupDisplay = 'NAME'
            LookupSource = dmComplaint.dsComplKind
            TabOrder = 0
          end
          object DBEdit11: TDBEdit
            Left = 184
            Top = 33
            Width = 489
            Height = 21
            DataField = 'PARTY_NAME'
            DataSource = dmComplaint.dsU1Compl
            TabOrder = 1
          end
          object JvDBLookupCombo11: TJvDBLookupCombo
            Left = 184
            Top = 62
            Width = 488
            Height = 21
            DataField = 'PARTY_TYPE_ID'
            DataSource = dmComplaint.dsU1Compl
            LookupField = 'CONTENTID'
            LookupDisplay = 'NAME'
            LookupSource = dmMain.dsPARTS_TYPE
            TabOrder = 2
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'TabSheet3'
          ImageIndex = 2
          object Label40: TLabel
            Left = 11
            Top = 8
            Width = 121
            Height = 13
            Caption = #1058#1080#1087' '#1078#1072#1083#1086#1073#1099' ('#1087#1088#1086#1090#1077#1089#1090#1072')*'
          end
          object Label41: TLabel
            Left = 11
            Top = 67
            Width = 83
            Height = 13
            Caption = #1060#1080#1086' '#1079#1072#1103#1074#1080#1090#1077#1083#1103'*'
          end
          object Label42: TLabel
            Left = 11
            Top = 96
            Width = 158
            Height = 13
            Caption = #1055#1088#1077#1076#1089#1090#1072#1074#1083#1077#1085#1080#1077' ('#1078#1072#1083#1086#1073#1091') '#1087#1086#1076#1072#1083
          end
          object Label47: TLabel
            Left = 11
            Top = 37
            Width = 121
            Height = 13
            Caption = #1042#1080#1076' '#1087#1088#1086#1090#1077#1089#1090#1072' ('#1078#1072#1083#1086#1073#1099')*'
          end
          object JvDBLookupCombo12: TJvDBLookupCombo
            Left = 184
            Top = 4
            Width = 489
            Height = 21
            DataField = 'DOCUMENT_KIND_ID'
            DataSource = dmComplaint.dsAdmCompl
            LookupField = 'CONTENTID'
            LookupDisplay = 'NAME'
            LookupSource = dmComplaint.dsComplKind
            TabOrder = 0
          end
          object DBEdit12: TDBEdit
            Left = 184
            Top = 63
            Width = 489
            Height = 21
            DataField = 'PARTY_NAME'
            DataSource = dmComplaint.dsAdmCompl
            TabOrder = 1
          end
          object JvDBLookupCombo13: TJvDBLookupCombo
            Left = 184
            Top = 92
            Width = 488
            Height = 21
            DataField = 'PARTY_TYPE_ID'
            DataSource = dmComplaint.dsAdmCompl
            LookupField = 'CONTENTID'
            LookupDisplay = 'NAME'
            LookupSource = dmMain.dsPARTS_TYPE
            TabOrder = 2
          end
          object JvDBLookupCombo16: TJvDBLookupCombo
            Left = 184
            Top = 33
            Width = 489
            Height = 21
            DataField = 'DOCUMENT_TYPE_ID'
            DataSource = dmComplaint.dsAdmCompl
            LookupField = 'CONTENTID'
            LookupDisplay = 'NAME'
            LookupSource = dmComplaint.dsComplType
            TabOrder = 3
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'TabSheet4'
          ImageIndex = 3
          object Label43: TLabel
            Left = 11
            Top = 8
            Width = 121
            Height = 13
            Caption = #1058#1080#1087' '#1078#1072#1083#1086#1073#1099' ('#1087#1088#1086#1090#1077#1089#1090#1072')*'
          end
          object Label44: TLabel
            Left = 11
            Top = 37
            Width = 83
            Height = 13
            Caption = #1060#1080#1086' '#1079#1072#1103#1074#1080#1090#1077#1083#1103'*'
          end
          object Label45: TLabel
            Left = 11
            Top = 66
            Width = 158
            Height = 13
            Caption = #1055#1088#1077#1076#1089#1090#1072#1074#1083#1077#1085#1080#1077' ('#1078#1072#1083#1086#1073#1091') '#1087#1086#1076#1072#1083
          end
          object JvDBLookupCombo14: TJvDBLookupCombo
            Left = 184
            Top = 4
            Width = 489
            Height = 21
            DataField = 'DOCUMENT_KIND_ID'
            DataSource = dmComplaint.dsAdm1Compl
            LookupField = 'CONTENTID'
            LookupDisplay = 'NAME'
            LookupSource = dmComplaint.dsComplKind
            TabOrder = 0
          end
          object DBEdit13: TDBEdit
            Left = 184
            Top = 33
            Width = 489
            Height = 21
            DataField = 'DECLARANT_NAME'
            DataSource = dmComplaint.dsAdm1Compl
            TabOrder = 1
          end
          object JvDBLookupCombo15: TJvDBLookupCombo
            Left = 184
            Top = 62
            Width = 488
            Height = 21
            DataField = 'DECLARANT_STATUS_ID'
            DataSource = dmComplaint.dsAdm1Compl
            LookupField = 'CONTENTID'
            LookupDisplay = 'NAME'
            LookupSource = dmMain.dsPARTS_TYPE
            TabOrder = 2
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'TabSheet5'
          ImageIndex = 4
          object Label46: TLabel
            Left = 11
            Top = 13
            Width = 83
            Height = 13
            Caption = #1060#1080#1086' '#1079#1072#1103#1074#1080#1090#1077#1083#1103'*'
          end
          object DBEdit14: TDBEdit
            Left = 184
            Top = 9
            Width = 489
            Height = 21
            DataField = 'DECLARANT_NAME'
            DataSource = dmComplaint.dsMCompl
            TabOrder = 0
          end
        end
      end
    end
    object JvWizardInteriorPage4: TJvWizardInteriorPage
      Header.ParentFont = False
      Header.Title.Color = clNone
      Header.Title.Text = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1103' '#1074#1093#1086#1076#1103#1097#1077#1075#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Header.Title.Anchors = [akLeft, akTop, akRight]
      Header.Title.Font.Charset = DEFAULT_CHARSET
      Header.Title.Font.Color = clWindowText
      Header.Title.Font.Height = -16
      Header.Title.Font.Name = 'MS Sans Serif'
      Header.Title.Font.Style = [fsBold]
      Header.Subtitle.Color = clNone
      Header.Subtitle.Text = #1044#1086#1082#1091#1084#1077#1085#1090#1099
      Header.Subtitle.Anchors = [akLeft, akTop, akRight, akBottom]
      Header.Subtitle.Font.Charset = DEFAULT_CHARSET
      Header.Subtitle.Font.Color = clWindowText
      Header.Subtitle.Font.Height = -11
      Header.Subtitle.Font.Name = 'MS Sans Serif'
      Header.Subtitle.Font.Style = []
      EnabledButtons = [bkStart, bkLast, bkNext, bkFinish, bkCancel, bkHelp]
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099
      OnEnterPage = JvWizardInteriorPage4EnterPage
      object Panel3: TPanel
        Left = 0
        Top = 70
        Width = 675
        Height = 147
        Align = alTop
        TabOrder = 0
        object Label23: TLabel
          Left = 11
          Top = 8
          Width = 76
          Height = 13
          Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        end
        object Label24: TLabel
          Left = 11
          Top = 31
          Width = 76
          Height = 13
          Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        end
        object Label25: TLabel
          Left = 11
          Top = 54
          Width = 127
          Height = 13
          Caption = #1048#1079#1086#1073#1088#1072#1078#1077#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        end
        object Label26: TLabel
          Left = 11
          Top = 77
          Width = 92
          Height = 13
          Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#1076#1077#1083#1072' '#8470
        end
        object JvSpeedButton1: TJvSpeedButton
          Left = 611
          Top = 50
          Width = 20
          Height = 20
          Action = aViewImage
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00000000008080800000000000FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00000000008080800000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
            00008080800000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00000000000000000080808000FFFFFF00000000008080
            800000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00000000008080800000000000000000008080800080808000808080000000
            0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
            000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000080808000FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
            800000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C0000000
            0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
            8000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
            0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
            8000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000
            0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
            800000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C0000000
            0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
            0000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C0C0C000FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF0000000000C0C0C0000000000000000000C0C0C000C0C0C000FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00000000000000000080808000FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
        end
        object JvSpeedButton2: TJvSpeedButton
          Left = 631
          Top = 50
          Width = 20
          Height = 20
          Action = aLoadImage
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00000000000000000000000000000000000000000000000000000000000000
            00000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
            00000000000000FFFF00BFBFBF0000FFFF00BFBFBF0000FFFF00BFBFBF0000FF
            FF00BFBFBF0000FFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF000000
            0000FFFFFF000000000000FFFF00BFBFBF0000FFFF00BFBFBF0000FFFF00BFBF
            BF0000FFFF00BFBFBF0000FFFF0000000000FF00FF00FF00FF00FF00FF000000
            000000FFFF00FFFFFF000000000000FFFF00BFBFBF0000FFFF00BFBFBF0000FF
            FF00BFBFBF0000FFFF00BFBFBF0000FFFF0000000000FF00FF00FF00FF000000
            0000FFFFFF0000FFFF00FFFFFF00000000000000000000000000000000000000
            00000000000000000000000000000000000000000000FF00FF00FF00FF000000
            000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
            FF0000FFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
            0000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
            FF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
            000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF00000000000000
            000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF007F7F7F00000000000000000000000000000000007F7F7F00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        end
        object JvSpeedButton3: TJvSpeedButton
          Left = 651
          Top = 50
          Width = 20
          Height = 20
          Action = aDelImage
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF008080800080808000808080008080
            8000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF0080808000000080000000800000008000000080008080
            80008080800080808000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00000080000000FF000000FF000000FF000000FF000000FF000000
            FF000000FF00808080008080800080808000FF00FF00FF00FF00FF00FF00FF00
            FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
            FF000000FF000000FF000000800080808000FF00FF00FF00FF00FF00FF000000
            80000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
            FF000000FF000000FF000000FF008080800080808000FF00FF00000080000000
            FF000000FF000000FF00FFFFFF000000FF000000FF000000FF000000FF00FFFF
            FF00FFFFFF000000FF000000FF000000FF0080808000FF00FF00000080000000
            FF000000FF000000FF00FFFFFF00FFFFFF000000FF000000FF00FFFFFF00FFFF
            FF00FFFFFF000000FF000000FF000000FF0080808000808080000000FF000000
            FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF000000FF000000FF000000FF000000FF0000008000808080000000FF000000
            FF000000FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF000000
            FF000000FF000000FF000000FF000000FF0000008000808080000000FF000000
            FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
            FF000000FF000000FF000000FF000000FF0000008000808080000000FF000000
            FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFF
            FF000000FF000000FF000000FF000000FF0000008000FF00FF00000080000000
            FF000000FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFF
            FF00FFFFFF000000FF000000FF000000FF0080808000FF00FF00FF00FF000000
            FF000000FF000000FF00FFFFFF000000FF000000FF000000FF000000FF000000
            FF000000FF000000FF000000FF0000008000FF00FF00FF00FF00FF00FF000000
            80000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
            FF000000FF000000FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00000080000000FF000000FF000000FF000000FF000000FF000000FF000000
            FF000000FF0000008000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00000080000000FF000000FF000000FF000000FF000000
            800000008000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        end
        object vDocTextTypeDoc: TJvDBLookupCombo
          Left = 174
          Top = 4
          Width = 499
          Height = 21
          DataField = 'DOCTYPE'
          DataSource = dsDocText
          LookupField = 'OBJECTID'
          LookupDisplay = 'DOCTYPENAME'
          LookupSource = dmMain.dsDocType
          TabOrder = 0
        end
        object DBEdit9: TDBEdit
          Left = 174
          Top = 27
          Width = 499
          Height = 21
          DataField = 'DOCNAME'
          DataSource = dsDocText
          TabOrder = 1
        end
        object DBEdit10: TDBEdit
          Left = 174
          Top = 50
          Width = 435
          Height = 21
          DataField = 'FILENAME'
          DataSource = dsDocText
          PopupMenu = pmImageDoc
          TabOrder = 2
        end
        object vCaseNumber: TDBEdit
          Left = 174
          Top = 73
          Width = 499
          Height = 21
          DataField = 'CaseNum'
          DataSource = dsDocument
          PopupMenu = pmCaseNumber
          TabOrder = 3
        end
      end
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 242
        Width = 675
        Height = 169
        Align = alClient
        DataSource = dsDocText
        DynProps = <>
        FooterParams.Color = clWindow
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 2
        Columns = <
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'DOCNAME'
            Footers = <>
            Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
            Width = 408
          end
          item
            DynProps = <>
            EditButtons = <>
            FieldName = 'FILENAME'
            Footers = <>
            Title.Caption = #1060#1072#1081#1083
            Width = 239
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object DBNavigator2: TDBNavigator
        Left = 0
        Top = 217
        Width = 675
        Height = 25
        DataSource = dsDocText
        VisibleButtons = [nbInsert, nbEdit, nbPost, nbCancel, nbRefresh]
        Align = alTop
        TabOrder = 1
      end
    end
    object JvWizardInteriorPage5: TJvWizardInteriorPage
      Header.ParentFont = False
      Header.Title.Color = clNone
      Header.Title.Text = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1103' '#1074#1093#1086#1076#1103#1097#1077#1075#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Header.Title.Anchors = [akLeft, akTop, akRight]
      Header.Title.Font.Charset = DEFAULT_CHARSET
      Header.Title.Font.Color = clWindowText
      Header.Title.Font.Height = -16
      Header.Title.Font.Name = 'MS Sans Serif'
      Header.Title.Font.Style = [fsBold]
      Header.Subtitle.Color = clNone
      Header.Subtitle.Text = #1050#1086#1085#1090#1088#1086#1083#1100' '#1080#1089#1087#1086#1083#1085#1077#1085#1080#1103
      Header.Subtitle.Anchors = [akLeft, akTop, akRight, akBottom]
      Header.Subtitle.Font.Charset = DEFAULT_CHARSET
      Header.Subtitle.Font.Color = clWindowText
      Header.Subtitle.Font.Height = -11
      Header.Subtitle.Font.Name = 'MS Sans Serif'
      Header.Subtitle.Font.Style = []
      EnabledButtons = [bkStart, bkLast, bkNext, bkFinish, bkCancel, bkHelp]
      VisibleButtons = [bkBack, bkFinish, bkCancel]
      Caption = #1050#1086#1090#1088#1086#1083#1100' '#1080#1089#1087#1086#1083#1085#1077#1085#1080#1103
      OnEnterPage = JvWizardInteriorPage5EnterPage
      OnFinishButtonClick = JvWizardInteriorPage5FinishButtonClick
      object Panel4: TPanel
        Left = 0
        Top = 70
        Width = 675
        Height = 341
        Align = alClient
        Caption = 'Panel4'
        TabOrder = 0
        object Label21: TLabel
          Left = 11
          Top = 6
          Width = 133
          Height = 13
          Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        end
        object Label22: TLabel
          Left = 11
          Top = 32
          Width = 57
          Height = 13
          Caption = #1056#1077#1079#1086#1083#1102#1094#1080#1103
        end
        object Label27: TLabel
          Left = 10
          Top = 89
          Width = 76
          Height = 13
          Caption = #1044#1072#1090#1072' '#1087#1077#1088#1077#1076#1072#1095#1080
        end
        object Label28: TLabel
          Left = 306
          Top = 89
          Width = 50
          Height = 13
          Caption = #1055#1077#1088#1077#1076#1072#1085#1086
        end
        object Label29: TLabel
          Left = 10
          Top = 112
          Width = 95
          Height = 13
          Caption = #1050#1086#1085#1090#1088#1086#1083#1100#1085#1099#1081' '#1089#1088#1086#1082
        end
        object Label30: TLabel
          Left = 306
          Top = 112
          Width = 89
          Height = 13
          Caption = #1044#1072#1090#1072' '#1080#1089#1087#1086#1083#1085#1077#1085#1080#1103
        end
        object Label31: TLabel
          Left = 11
          Top = 138
          Width = 122
          Height = 26
          Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077'        '#1054#1090#1084#1077#1090#1082#1072' '#1086#1073' '#1080#1089#1087#1086#1083#1085#1077#1085#1080#1080
          WordWrap = True
        end
        object JvDBDatePickerEdit2: TJvDBDatePickerEdit
          Left = 174
          Top = 85
          Width = 96
          Height = 21
          AllowNoDate = True
          DataField = 'PASSDATE'
          DataSource = dsLetter
          DateFormat = 'dd.MM.yyyy'
          DateSeparator = '.'
          ShowCheckBox = True
          StoreDateFormat = True
          TabOrder = 2
          DesignSize = (
            94
            19)
        end
        object JvDBDatePickerEdit3: TJvDBDatePickerEdit
          Left = 174
          Top = 108
          Width = 96
          Height = 21
          AllowNoDate = True
          DataField = 'PLANDATE'
          DataSource = dsLetter
          DateFormat = 'dd.MM.yyyy'
          DateSeparator = '.'
          ShowCheckBox = True
          StoreDateFormat = True
          TabOrder = 4
          DesignSize = (
            94
            19)
        end
        object JvDBDatePickerEdit4: TJvDBDatePickerEdit
          Left = 406
          Top = 108
          Width = 99
          Height = 21
          AllowNoDate = True
          DataField = 'EXECUTIONDATE'
          DataSource = dsLetter
          DateFormat = 'dd.MM.yyyy'
          DateSeparator = '.'
          ShowCheckBox = True
          StoreDateFormat = True
          TabOrder = 5
          DesignSize = (
            97
            19)
        end
        object DBMemo3: TDBMemo
          Left = 174
          Top = 131
          Width = 499
          Height = 57
          DataField = 'DESCRIPTION'
          DataSource = dsLetter
          TabOrder = 6
        end
        object JvDBLookupCombo2: TJvDBLookupCombo
          Left = 406
          Top = 85
          Width = 267
          Height = 21
          DataField = 'RECIPIENT'
          DataSource = dsLetter
          LookupField = 'GROUPCONTENTID'
          LookupDisplay = 'USERNAME'
          LookupSource = dmMain.dsUser
          TabOrder = 3
        end
        object JvDBLookupComboEdit1: TJvDBLookupComboEdit
          Left = 174
          Top = 3
          Width = 499
          Height = 21
          LookupDisplay = 'POSTVALUE'
          LookupField = 'POSTVALUE'
          LookupSource = dmMain.dsAutoPostName
          TabOrder = 0
          Text = 'JvDBLookupComboEdit1'
          UseRecordCount = True
          DataField = 'LETTERNAME'
          DataSource = dsLetter
        end
        object JvDBLookupComboEdit3: TJvDBLookupComboEdit
          Left = 174
          Top = 27
          Width = 499
          Height = 21
          LookupDisplay = 'POSTVALUE'
          LookupField = 'POSTVALUE'
          LookupSource = dmMain.dsAutoPostRes
          TabOrder = 1
          Text = 'JvDBLookupComboEdit3'
          UseRecordCount = True
          DataField = 'INSTRUCTION'
          DataSource = dsLetter
        end
      end
    end
    object JvWizardRouteMapList1: TJvWizardRouteMapList
      Left = 0
      Top = 0
      Width = 177
      Height = 411
      Cursor = crHandPoint
      ActiveFont.Charset = DEFAULT_CHARSET
      ActiveFont.Color = clWindowText
      ActiveFont.Height = -11
      ActiveFont.Name = 'MS Sans Serif'
      ActiveFont.Style = [fsBold]
      HotTrackFont.Charset = DEFAULT_CHARSET
      HotTrackFont.Color = clNavy
      HotTrackFont.Height = -11
      HotTrackFont.Name = 'MS Sans Serif'
      HotTrackFont.Style = [fsUnderline]
      Rounded = True
    end
  end
  object tLetter: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE LETTER'
      'SET '
      '    LETTERDATE = :LETTERDATE,'
      '    NUMBER = :NUMBER,'
      '    INCNUMBER = :INCNUMBER,'
      '    LETTERNAME = :LETTERNAME,'
      '    LETTERTYPE = :LETTERTYPE,'
      '    PASSDATE = :PASSDATE,'
      '    INSTRUCTION = :INSTRUCTION,'
      '    PLANDATE = :PLANDATE,'
      '    EXECUTIONDATE = :EXECUTIONDATE,'
      '    DESCRIPTION = :DESCRIPTION,'
      '    OUTDATE = :OUTDATE,'
      '    SYSTEMUSER = :SYSTEMUSER,'
      '    POSTTYPE = :POSTTYPE,'
      '    DOCTYPE = :DOCTYPE,'
      '    ONINCOMINGLETTER = :ONINCOMINGLETTER,'
      '    COURTD = :COURTD,'
      '    DOCTEXT = :DOCTEXT,'
      '    LETTEROUTTYPE = :LETTEROUTTYPE,'
      '    ADDLETTERPAGECOUNT = :ADDLETTERPAGECOUNT,'
      '    DOCUMENTPAGECOUNT = :DOCUMENTPAGECOUNT,'
      '    OTHERPAGECOUNT = :OTHERPAGECOUNT,'
      '    CARDID = :CARDID,'
      '    CASEID = :CASEID,'
      '    SENDER = :SENDER,'
      '    RECIPIENT = :RECIPIENT,'
      '    CREATOR = :CREATOR,'
      '    INTYPE = :INTYPE,'
      '    COMPLAINTID = :COMPLAINTID'
      'WHERE'
      '    OBJECTID = :OLD_OBJECTID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    LETTER'
      'WHERE'
      '        OBJECTID = :OLD_OBJECTID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO LETTER('
      '    OBJECTID,'
      '    LETTERDATE,'
      '    NUMBER,'
      '    INCNUMBER,'
      '    LETTERNAME,'
      '    LETTERTYPE,'
      '    PASSDATE,'
      '    INSTRUCTION,'
      '    PLANDATE,'
      '    EXECUTIONDATE,'
      '    DESCRIPTION,'
      '    OUTDATE,'
      '    SYSTEMUSER,'
      '    POSTTYPE,'
      '    DOCTYPE,'
      '    ONINCOMINGLETTER,'
      '    COURTD,'
      '    DOCTEXT,'
      '    LETTEROUTTYPE,'
      '    ADDLETTERPAGECOUNT,'
      '    DOCUMENTPAGECOUNT,'
      '    OTHERPAGECOUNT,'
      '    CARDID,'
      '    CASEID,'
      '    SENDER,'
      '    RECIPIENT,'
      '    CREATOR,'
      '    INTYPE,'
      '    COMPLAINTID'
      ')'
      'VALUES('
      '    :OBJECTID,'
      '    :LETTERDATE,'
      '    :NUMBER,'
      '    :INCNUMBER,'
      '    :LETTERNAME,'
      '    :LETTERTYPE,'
      '    :PASSDATE,'
      '    :INSTRUCTION,'
      '    :PLANDATE,'
      '    :EXECUTIONDATE,'
      '    :DESCRIPTION,'
      '    :OUTDATE,'
      '    :SYSTEMUSER,'
      '    :POSTTYPE,'
      '    :DOCTYPE,'
      '    :ONINCOMINGLETTER,'
      '    :COURTD,'
      '    :DOCTEXT,'
      '    :LETTEROUTTYPE,'
      '    :ADDLETTERPAGECOUNT,'
      '    :DOCUMENTPAGECOUNT,'
      '    :OTHERPAGECOUNT,'
      '    :CARDID,'
      '    :CASEID,'
      '    :SENDER,'
      '    :RECIPIENT,'
      '    :CREATOR,'
      '    :INTYPE,'
      '    :COMPLAINTID'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    OBJECTID,'
      '    LETTERDATE,'
      '    NUMBER,'
      '    INCNUMBER,'
      '    LETTERNAME,'
      '    LETTERTYPE,'
      '    PASSDATE,'
      '    INSTRUCTION,'
      '    PLANDATE,'
      '    EXECUTIONDATE,'
      '    DESCRIPTION,'
      '    OUTDATE,'
      '    SYSTEMUSER,'
      '    POSTTYPE,'
      '    DOCTYPE,'
      '    ONINCOMINGLETTER,'
      '    COURTD,'
      '    DOCTEXT,'
      '    LETTEROUTTYPE,'
      '    ADDLETTERPAGECOUNT,'
      '    DOCUMENTPAGECOUNT,'
      '    OTHERPAGECOUNT,'
      '    CARDID,'
      '    CASEID,'
      '    SENDER,'
      '    RECIPIENT,'
      '    CREATOR,'
      '    INTYPE,'
      '    COMPLAINTID'
      'FROM'
      '    LETTER '
      ''
      ''
      ' WHERE '
      '        LETTER.OBJECTID = :OLD_OBJECTID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    OBJECTID,'
      '    LETTERDATE,'
      '    NUMBER,'
      '    INCNUMBER,'
      '    LETTERNAME,'
      '    LETTERTYPE,'
      '    PASSDATE,'
      '    INSTRUCTION,'
      '    PLANDATE,'
      '    EXECUTIONDATE,'
      '    DESCRIPTION,'
      '    OUTDATE,'
      '    SYSTEMUSER,'
      '    POSTTYPE,'
      '    DOCTYPE,'
      '    ONINCOMINGLETTER,'
      '    COURTD,'
      '    DOCTEXT,'
      '    LETTEROUTTYPE,'
      '    ADDLETTERPAGECOUNT,'
      '    DOCUMENTPAGECOUNT,'
      '    OTHERPAGECOUNT,'
      '    CARDID,'
      '    CASEID,'
      '    SENDER,'
      '    RECIPIENT,'
      '    CREATOR,'
      '    INTYPE,'
      '    COMPLAINTID'
      'FROM'
      '    LETTER '
      'where @@where%1=1@')
    AutoUpdateOptions.GeneratorName = 'LETTER_OBJECTID_GEN'
    Transaction = dmMain.trAdd
    Database = dmMain.dbAdd
    Left = 264
    Top = 9
    object tLetterOBJECTID: TFIBIntegerField
      FieldName = 'OBJECTID'
    end
    object tLetterLETTERDATE: TFIBDateTimeField
      FieldName = 'LETTERDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tLetterNUMBER: TFIBIntegerField
      FieldName = 'NUMBER'
    end
    object tLetterINCNUMBER: TFIBStringField
      FieldName = 'INCNUMBER'
      Size = 128
      EmptyStrToNull = True
    end
    object tLetterLETTERNAME: TFIBStringField
      FieldName = 'LETTERNAME'
      Size = 256
      EmptyStrToNull = True
    end
    object tLetterLETTERTYPE: TFIBIntegerField
      FieldName = 'LETTERTYPE'
    end
    object tLetterPASSDATE: TFIBDateTimeField
      FieldName = 'PASSDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tLetterINSTRUCTION: TFIBStringField
      FieldName = 'INSTRUCTION'
      Size = 1024
      EmptyStrToNull = True
    end
    object tLetterPLANDATE: TFIBDateTimeField
      FieldName = 'PLANDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tLetterEXECUTIONDATE: TFIBDateTimeField
      FieldName = 'EXECUTIONDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tLetterDESCRIPTION: TFIBStringField
      FieldName = 'DESCRIPTION'
      Size = 1024
      EmptyStrToNull = True
    end
    object tLetterOUTDATE: TFIBDateTimeField
      FieldName = 'OUTDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tLetterSYSTEMUSER: TFIBIntegerField
      FieldName = 'SYSTEMUSER'
    end
    object tLetterPOSTTYPE: TFIBIntegerField
      FieldName = 'POSTTYPE'
    end
    object tLetterDOCTYPE: TFIBIntegerField
      FieldName = 'DOCTYPE'
    end
    object tLetterONINCOMINGLETTER: TFIBIntegerField
      FieldName = 'ONINCOMINGLETTER'
    end
    object tLetterCOURTD: TFIBIntegerField
      FieldName = 'COURTD'
    end
    object tLetterDOCTEXT: TFIBIntegerField
      FieldName = 'DOCTEXT'
    end
    object tLetterLETTEROUTTYPE: TFIBSmallIntField
      FieldName = 'LETTEROUTTYPE'
    end
    object tLetterADDLETTERPAGECOUNT: TFIBIntegerField
      FieldName = 'ADDLETTERPAGECOUNT'
    end
    object tLetterDOCUMENTPAGECOUNT: TFIBIntegerField
      FieldName = 'DOCUMENTPAGECOUNT'
    end
    object tLetterOTHERPAGECOUNT: TFIBIntegerField
      FieldName = 'OTHERPAGECOUNT'
    end
    object tLetterCARDID: TFIBIntegerField
      FieldName = 'CARDID'
    end
    object tLetterCASEID: TFIBIntegerField
      FieldName = 'CASEID'
    end
    object tLetterSENDER: TFIBStringField
      FieldName = 'SENDER'
      Size = 256
      EmptyStrToNull = True
    end
    object tLetterRECIPIENT: TFIBIntegerField
      FieldName = 'RECIPIENT'
    end
    object tLetterCREATOR: TFIBIntegerField
      FieldName = 'CREATOR'
    end
    object tLetterINTYPE: TFIBSmallIntField
      FieldName = 'INTYPE'
    end
    object tLetterCOMPLAINTID: TFIBIntegerField
      FieldName = 'COMPLAINTID'
    end
  end
  object dsLetter: TDataSource
    DataSet = tLetter
    Left = 264
    Top = 39
  end
  object tCase: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE G1_CASE'
      'SET '
      '    FULL_NUMBER = :FULL_NUMBER,'
      '    CASE_FULL_NUMBER = :CASE_FULL_NUMBER,'
      '    SHORT_NUMBER = :SHORT_NUMBER,'
      '    YEAR_REG = :YEAR_REG,'
      '    M_FULL_NUMBER = :M_FULL_NUMBER,'
      '    M_SHORT_NUMBER = :M_SHORT_NUMBER,'
      '    M_YEAR_REG = :M_YEAR_REG,'
      '    CASE_TYPE_ID = :CASE_TYPE_ID,'
      '    ENTRY_DATE = :ENTRY_DATE,'
      '    ENTRY_TYPE_ID = :ENTRY_TYPE_ID,'
      '    PREV_FULL_NUMBER = :PREV_FULL_NUMBER,'
      '    FOLDER_ID = :FOLDER_ID,'
      '    O_SEND_DATE = :O_SEND_DATE,'
      '    O_ACT_EXEC_DATE = :O_ACT_EXEC_DATE,'
      '    S_DECISION_147_DATE = :S_DECISION_147_DATE,'
      '    S_DECISION_152_DATE = :S_DECISION_152_DATE,'
      '    S_PR_HEAR_RESULT_ID = :S_PR_HEAR_RESULT_ID,'
      '    S_PR_HEAR_RESULT_ID2 = :S_PR_HEAR_RESULT_ID2,'
      '    S_DECISION_153_DATE = :S_DECISION_153_DATE,'
      '    S_CLAIM_CHANGED_DATE = :S_CLAIM_CHANGED_DATE,'
      '    A_CASE_NUMBER_I = :A_CASE_NUMBER_I,'
      '    A_JUDGE_I_ID = :A_JUDGE_I_ID,'
      '    A_RESULT_I_DATE = :A_RESULT_I_DATE,'
      '    A_RESULT_I_ID = :A_RESULT_I_ID,'
      '    A_ORDER_TYPE_ID = :A_ORDER_TYPE_ID,'
      '    A_APPELANT_ID = :A_APPELANT_ID,'
      '    A_WHAT_APPEAL_DATE = :A_WHAT_APPEAL_DATE,'
      '    A_WHAT_APPEAL_ID = :A_WHAT_APPEAL_ID,'
      '    A_SUBSTANTIATION_ID = :A_SUBSTANTIATION_ID,'
      '    A_RETURNED_DATE = :A_RETURNED_DATE,'
      '    A_RET_JUDGE_ID = :A_RET_JUDGE_ID,'
      '    SUIT_MONEY = :SUIT_MONEY,'
      '    DUTY = :DUTY,'
      '    REQ_ESSENCE = :REQ_ESSENCE,'
      '    DECISION_DATE = :DECISION_DATE,'
      '    DECISION_ID = :DECISION_ID,'
      '    DECISION_ID2 = :DECISION_ID2,'
      '    RESULT_DATE = :RESULT_DATE,'
      '    RESULT_TYPE_ID = :RESULT_TYPE_ID,'
      '    COURT_STAFF_ID = :COURT_STAFF_ID,'
      '    REQ_DISAFF_DATE = :REQ_DISAFF_DATE,'
      '    REQ_DISAFF = :REQ_DISAFF,'
      '    DISORDER_DATE = :DISORDER_DATE,'
      '    RESULT_ID = :RESULT_ID,'
      '    RESULT_ID2 = :RESULT_ID2,'
      '    RESULT_ESSENCE = :RESULT_ESSENCE,'
      '    RETURN_OFFICE_DATE = :RETURN_OFFICE_DATE,'
      '    PAYMENT_BY_RESULT = :PAYMENT_BY_RESULT,'
      '    PAYMENT_DUTY = :PAYMENT_DUTY,'
      '    PAYMENT_COSTS = :PAYMENT_COSTS,'
      '    PAYMENT_PENALTY = :PAYMENT_PENALTY,'
      '    VALIDITY_DATE = :VALIDITY_DATE,'
      '    PREPARE_ARCH_DATE = :PREPARE_ARCH_DATE,'
      '    ARCHIVE_DATE = :ARCHIVE_DATE,'
      '    VOLUME_TO_SHELVE = :VOLUME_TO_SHELVE,'
      '    RETENTION_PERIOD = :RETENTION_PERIOD,'
      '    DESTROY_DATE = :DESTROY_DATE,'
      '    VOLUME_ARCHIVE = :VOLUME_ARCHIVE,'
      '    VOLUME_DECREE = :VOLUME_DECREE,'
      '    VOLUME_PLACE = :VOLUME_PLACE,'
      '    CATEGORY_ID = :CATEGORY_ID,'
      '    CATEGORY_ID2 = :CATEGORY_ID2,'
      '    STAT_LINE = :STAT_LINE,'
      '    JUDGE_ID = :JUDGE_ID,'
      '    CLERK = :CLERK,'
      '    COMMENT = :COMMENT,'
      '    SORT_ORDER = :SORT_ORDER,'
      '    SYS_STAT3_IS_MATERIAL = :SYS_STAT3_IS_MATERIAL,'
      '    ORIGIN_DATE = :ORIGIN_DATE,'
      '    PASS_WRITEXEC_DATE = :PASS_WRITEXEC_DATE,'
      '    WRITEXEC_COMMENT = :WRITEXEC_COMMENT,'
      '    RESULT_N_DATE = :RESULT_N_DATE,'
      '    RESULT_N_ID = :RESULT_N_ID,'
      '    S_PR_HEAR_DATE = :S_PR_HEAR_DATE,'
      '    S_HEARING_DATE = :S_HEARING_DATE,'
      '    SYS_EVENT_DATE = :SYS_EVENT_DATE,'
      '    SYS_EVENT_NAME_ID = :SYS_EVENT_NAME_ID,'
      '    SYS_EVENT_RESULT_ID = :SYS_EVENT_RESULT_ID,'
      '    FIRST_HEARING_DATE = :FIRST_HEARING_DATE,'
      '    CIRCUM_DEF_DATE = :CIRCUM_DEF_DATE,'
      '    CIRCUM_DEF_ID = :CIRCUM_DEF_ID,'
      '    COMPLICATION_DATE = :COMPLICATION_DATE,'
      '    PERIOD_OF_VALIDITY_ID = :PERIOD_OF_VALIDITY_ID,'
      '    ADJUTANT_ID = :ADJUTANT_ID,'
      '    CASE_ATTR = :CASE_ATTR,'
      '    THEFT_DAMAGE = :THEFT_DAMAGE,'
      '    ANOTHER_DAMAGE = :ANOTHER_DAMAGE,'
      '    AMOUNT_DAMAGE = :AMOUNT_DAMAGE,'
      '    A = :A,'
      '    RET_WRITEXEC_DATE = :RET_WRITEXEC_DATE,'
      '    EXEC_DATE = :EXEC_DATE,'
      '    EXEC_REASON = :EXEC_REASON,'
      '    EXEC_TYPE_COMMENT = :EXEC_TYPE_COMMENT,'
      '    DUTY_WHO = :DUTY_WHO,'
      '    PASS_WRITEXEC_DATE2 = :PASS_WRITEXEC_DATE2,'
      '    PASS_EXEC_DATE = :PASS_EXEC_DATE,'
      '    EXEC_MONEY = :EXEC_MONEY,'
      '    EXEC_MONEY2 = :EXEC_MONEY2,'
      '    WEIGHT = :WEIGHT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    G1_CASE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO G1_CASE('
      '    ID,'
      '    FULL_NUMBER,'
      '    CASE_FULL_NUMBER,'
      '    SHORT_NUMBER,'
      '    YEAR_REG,'
      '    M_FULL_NUMBER,'
      '    M_SHORT_NUMBER,'
      '    M_YEAR_REG,'
      '    CASE_TYPE_ID,'
      '    ENTRY_DATE,'
      '    ENTRY_TYPE_ID,'
      '    PREV_FULL_NUMBER,'
      '    FOLDER_ID,'
      '    O_SEND_DATE,'
      '    O_ACT_EXEC_DATE,'
      '    S_DECISION_147_DATE,'
      '    S_DECISION_152_DATE,'
      '    S_PR_HEAR_RESULT_ID,'
      '    S_PR_HEAR_RESULT_ID2,'
      '    S_DECISION_153_DATE,'
      '    S_CLAIM_CHANGED_DATE,'
      '    A_CASE_NUMBER_I,'
      '    A_JUDGE_I_ID,'
      '    A_RESULT_I_DATE,'
      '    A_RESULT_I_ID,'
      '    A_ORDER_TYPE_ID,'
      '    A_APPELANT_ID,'
      '    A_WHAT_APPEAL_DATE,'
      '    A_WHAT_APPEAL_ID,'
      '    A_SUBSTANTIATION_ID,'
      '    A_RETURNED_DATE,'
      '    A_RET_JUDGE_ID,'
      '    SUIT_MONEY,'
      '    DUTY,'
      '    REQ_ESSENCE,'
      '    DECISION_DATE,'
      '    DECISION_ID,'
      '    DECISION_ID2,'
      '    RESULT_DATE,'
      '    RESULT_TYPE_ID,'
      '    COURT_STAFF_ID,'
      '    REQ_DISAFF_DATE,'
      '    REQ_DISAFF,'
      '    DISORDER_DATE,'
      '    RESULT_ID,'
      '    RESULT_ID2,'
      '    RESULT_ESSENCE,'
      '    RETURN_OFFICE_DATE,'
      '    PAYMENT_BY_RESULT,'
      '    PAYMENT_DUTY,'
      '    PAYMENT_COSTS,'
      '    PAYMENT_PENALTY,'
      '    VALIDITY_DATE,'
      '    PREPARE_ARCH_DATE,'
      '    ARCHIVE_DATE,'
      '    VOLUME_TO_SHELVE,'
      '    RETENTION_PERIOD,'
      '    DESTROY_DATE,'
      '    VOLUME_ARCHIVE,'
      '    VOLUME_DECREE,'
      '    VOLUME_PLACE,'
      '    CATEGORY_ID,'
      '    CATEGORY_ID2,'
      '    STAT_LINE,'
      '    JUDGE_ID,'
      '    CLERK,'
      '    COMMENT,'
      '    SORT_ORDER,'
      '    SYS_STAT3_IS_MATERIAL,'
      '    ORIGIN_DATE,'
      '    PASS_WRITEXEC_DATE,'
      '    WRITEXEC_COMMENT,'
      '    RESULT_N_DATE,'
      '    RESULT_N_ID,'
      '    S_PR_HEAR_DATE,'
      '    S_HEARING_DATE,'
      '    SYS_EVENT_DATE,'
      '    SYS_EVENT_NAME_ID,'
      '    SYS_EVENT_RESULT_ID,'
      '    FIRST_HEARING_DATE,'
      '    CIRCUM_DEF_DATE,'
      '    CIRCUM_DEF_ID,'
      '    COMPLICATION_DATE,'
      '    PERIOD_OF_VALIDITY_ID,'
      '    ADJUTANT_ID,'
      '    CASE_ATTR,'
      '    THEFT_DAMAGE,'
      '    ANOTHER_DAMAGE,'
      '    AMOUNT_DAMAGE,'
      '    A,'
      '    RET_WRITEXEC_DATE,'
      '    EXEC_DATE,'
      '    EXEC_REASON,'
      '    EXEC_TYPE_COMMENT,'
      '    DUTY_WHO,'
      '    PASS_WRITEXEC_DATE2,'
      '    PASS_EXEC_DATE,'
      '    EXEC_MONEY,'
      '    EXEC_MONEY2,'
      '    WEIGHT'
      ')'
      'VALUES('
      '    :ID,'
      '    :FULL_NUMBER,'
      '    :CASE_FULL_NUMBER,'
      '    :SHORT_NUMBER,'
      '    :YEAR_REG,'
      '    :M_FULL_NUMBER,'
      '    :M_SHORT_NUMBER,'
      '    :M_YEAR_REG,'
      '    :CASE_TYPE_ID,'
      '    :ENTRY_DATE,'
      '    :ENTRY_TYPE_ID,'
      '    :PREV_FULL_NUMBER,'
      '    :FOLDER_ID,'
      '    :O_SEND_DATE,'
      '    :O_ACT_EXEC_DATE,'
      '    :S_DECISION_147_DATE,'
      '    :S_DECISION_152_DATE,'
      '    :S_PR_HEAR_RESULT_ID,'
      '    :S_PR_HEAR_RESULT_ID2,'
      '    :S_DECISION_153_DATE,'
      '    :S_CLAIM_CHANGED_DATE,'
      '    :A_CASE_NUMBER_I,'
      '    :A_JUDGE_I_ID,'
      '    :A_RESULT_I_DATE,'
      '    :A_RESULT_I_ID,'
      '    :A_ORDER_TYPE_ID,'
      '    :A_APPELANT_ID,'
      '    :A_WHAT_APPEAL_DATE,'
      '    :A_WHAT_APPEAL_ID,'
      '    :A_SUBSTANTIATION_ID,'
      '    :A_RETURNED_DATE,'
      '    :A_RET_JUDGE_ID,'
      '    :SUIT_MONEY,'
      '    :DUTY,'
      '    :REQ_ESSENCE,'
      '    :DECISION_DATE,'
      '    :DECISION_ID,'
      '    :DECISION_ID2,'
      '    :RESULT_DATE,'
      '    :RESULT_TYPE_ID,'
      '    :COURT_STAFF_ID,'
      '    :REQ_DISAFF_DATE,'
      '    :REQ_DISAFF,'
      '    :DISORDER_DATE,'
      '    :RESULT_ID,'
      '    :RESULT_ID2,'
      '    :RESULT_ESSENCE,'
      '    :RETURN_OFFICE_DATE,'
      '    :PAYMENT_BY_RESULT,'
      '    :PAYMENT_DUTY,'
      '    :PAYMENT_COSTS,'
      '    :PAYMENT_PENALTY,'
      '    :VALIDITY_DATE,'
      '    :PREPARE_ARCH_DATE,'
      '    :ARCHIVE_DATE,'
      '    :VOLUME_TO_SHELVE,'
      '    :RETENTION_PERIOD,'
      '    :DESTROY_DATE,'
      '    :VOLUME_ARCHIVE,'
      '    :VOLUME_DECREE,'
      '    :VOLUME_PLACE,'
      '    :CATEGORY_ID,'
      '    :CATEGORY_ID2,'
      '    :STAT_LINE,'
      '    :JUDGE_ID,'
      '    :CLERK,'
      '    :COMMENT,'
      '    :SORT_ORDER,'
      '    :SYS_STAT3_IS_MATERIAL,'
      '    :ORIGIN_DATE,'
      '    :PASS_WRITEXEC_DATE,'
      '    :WRITEXEC_COMMENT,'
      '    :RESULT_N_DATE,'
      '    :RESULT_N_ID,'
      '    :S_PR_HEAR_DATE,'
      '    :S_HEARING_DATE,'
      '    :SYS_EVENT_DATE,'
      '    :SYS_EVENT_NAME_ID,'
      '    :SYS_EVENT_RESULT_ID,'
      '    :FIRST_HEARING_DATE,'
      '    :CIRCUM_DEF_DATE,'
      '    :CIRCUM_DEF_ID,'
      '    :COMPLICATION_DATE,'
      '    :PERIOD_OF_VALIDITY_ID,'
      '    :ADJUTANT_ID,'
      '    :CASE_ATTR,'
      '    :THEFT_DAMAGE,'
      '    :ANOTHER_DAMAGE,'
      '    :AMOUNT_DAMAGE,'
      '    :A,'
      '    :RET_WRITEXEC_DATE,'
      '    :EXEC_DATE,'
      '    :EXEC_REASON,'
      '    :EXEC_TYPE_COMMENT,'
      '    :DUTY_WHO,'
      '    :PASS_WRITEXEC_DATE2,'
      '    :PASS_EXEC_DATE,'
      '    :EXEC_MONEY,'
      '    :EXEC_MONEY2,'
      '    :WEIGHT'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    FULL_NUMBER,'
      '    CASE_FULL_NUMBER,'
      '    SHORT_NUMBER,'
      '    YEAR_REG,'
      '    M_FULL_NUMBER,'
      '    M_SHORT_NUMBER,'
      '    M_YEAR_REG,'
      '    CASE_TYPE_ID,'
      '    ENTRY_DATE,'
      '    ENTRY_TYPE_ID,'
      '    PREV_FULL_NUMBER,'
      '    FOLDER_ID,'
      '    O_SEND_DATE,'
      '    O_ACT_EXEC_DATE,'
      '    S_DECISION_147_DATE,'
      '    S_DECISION_152_DATE,'
      '    S_PR_HEAR_RESULT_ID,'
      '    S_PR_HEAR_RESULT_ID2,'
      '    S_DECISION_153_DATE,'
      '    S_CLAIM_CHANGED_DATE,'
      '    A_CASE_NUMBER_I,'
      '    A_JUDGE_I_ID,'
      '    A_RESULT_I_DATE,'
      '    A_RESULT_I_ID,'
      '    A_ORDER_TYPE_ID,'
      '    A_APPELANT_ID,'
      '    A_WHAT_APPEAL_DATE,'
      '    A_WHAT_APPEAL_ID,'
      '    A_SUBSTANTIATION_ID,'
      '    A_RETURNED_DATE,'
      '    A_RET_JUDGE_ID,'
      '    SUIT_MONEY,'
      '    DUTY,'
      '    REQ_ESSENCE,'
      '    DECISION_DATE,'
      '    DECISION_ID,'
      '    DECISION_ID2,'
      '    RESULT_DATE,'
      '    RESULT_TYPE_ID,'
      '    COURT_STAFF_ID,'
      '    REQ_DISAFF_DATE,'
      '    REQ_DISAFF,'
      '    DISORDER_DATE,'
      '    RESULT_ID,'
      '    RESULT_ID2,'
      '    RESULT_ESSENCE,'
      '    RETURN_OFFICE_DATE,'
      '    PAYMENT_BY_RESULT,'
      '    PAYMENT_DUTY,'
      '    PAYMENT_COSTS,'
      '    PAYMENT_PENALTY,'
      '    VALIDITY_DATE,'
      '    PREPARE_ARCH_DATE,'
      '    ARCHIVE_DATE,'
      '    VOLUME_TO_SHELVE,'
      '    RETENTION_PERIOD,'
      '    DESTROY_DATE,'
      '    VOLUME_ARCHIVE,'
      '    VOLUME_DECREE,'
      '    VOLUME_PLACE,'
      '    CATEGORY_ID,'
      '    CATEGORY_ID2,'
      '    STAT_LINE,'
      '    JUDGE_ID,'
      '    CLERK,'
      '    COMMENT,'
      '    SORT_ORDER,'
      '    SYS_STAT3_IS_MATERIAL,'
      '    ORIGIN_DATE,'
      '    PASS_WRITEXEC_DATE,'
      '    WRITEXEC_COMMENT,'
      '    RESULT_N_DATE,'
      '    RESULT_N_ID,'
      '    S_PR_HEAR_DATE,'
      '    S_HEARING_DATE,'
      '    SYS_EVENT_DATE,'
      '    SYS_EVENT_NAME_ID,'
      '    SYS_EVENT_RESULT_ID,'
      '    FIRST_HEARING_DATE,'
      '    CIRCUM_DEF_DATE,'
      '    CIRCUM_DEF_ID,'
      '    COMPLICATION_DATE,'
      '    PERIOD_OF_VALIDITY_ID,'
      '    ADJUTANT_ID,'
      '    CASE_ATTR,'
      '    THEFT_DAMAGE,'
      '    ANOTHER_DAMAGE,'
      '    AMOUNT_DAMAGE,'
      '    A,'
      '    RET_WRITEXEC_DATE,'
      '    EXEC_DATE,'
      '    EXEC_REASON,'
      '    EXEC_TYPE_COMMENT,'
      '    DUTY_WHO,'
      '    PASS_WRITEXEC_DATE2,'
      '    PASS_EXEC_DATE,'
      '    EXEC_MONEY,'
      '    EXEC_MONEY2,'
      '    WEIGHT'
      'FROM'
      '    G1_CASE '
      'where(  '
      '  ID=:CaseId'
      '     ) and (     G1_CASE.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    FULL_NUMBER,'
      '    CASE_FULL_NUMBER,'
      '    SHORT_NUMBER,'
      '    YEAR_REG,'
      '    M_FULL_NUMBER,'
      '    M_SHORT_NUMBER,'
      '    M_YEAR_REG,'
      '    CASE_TYPE_ID,'
      '    ENTRY_DATE,'
      '    ENTRY_TYPE_ID,'
      '    PREV_FULL_NUMBER,'
      '    FOLDER_ID,'
      '    O_SEND_DATE,'
      '    O_ACT_EXEC_DATE,'
      '    S_DECISION_147_DATE,'
      '    S_DECISION_152_DATE,'
      '    S_PR_HEAR_RESULT_ID,'
      '    S_PR_HEAR_RESULT_ID2,'
      '    S_DECISION_153_DATE,'
      '    S_CLAIM_CHANGED_DATE,'
      '    A_CASE_NUMBER_I,'
      '    A_JUDGE_I_ID,'
      '    A_RESULT_I_DATE,'
      '    A_RESULT_I_ID,'
      '    A_ORDER_TYPE_ID,'
      '    A_APPELANT_ID,'
      '    A_WHAT_APPEAL_DATE,'
      '    A_WHAT_APPEAL_ID,'
      '    A_SUBSTANTIATION_ID,'
      '    A_RETURNED_DATE,'
      '    A_RET_JUDGE_ID,'
      '    SUIT_MONEY,'
      '    DUTY,'
      '    REQ_ESSENCE,'
      '    DECISION_DATE,'
      '    DECISION_ID,'
      '    DECISION_ID2,'
      '    RESULT_DATE,'
      '    RESULT_TYPE_ID,'
      '    COURT_STAFF_ID,'
      '    REQ_DISAFF_DATE,'
      '    REQ_DISAFF,'
      '    DISORDER_DATE,'
      '    RESULT_ID,'
      '    RESULT_ID2,'
      '    RESULT_ESSENCE,'
      '    RETURN_OFFICE_DATE,'
      '    PAYMENT_BY_RESULT,'
      '    PAYMENT_DUTY,'
      '    PAYMENT_COSTS,'
      '    PAYMENT_PENALTY,'
      '    VALIDITY_DATE,'
      '    PREPARE_ARCH_DATE,'
      '    ARCHIVE_DATE,'
      '    VOLUME_TO_SHELVE,'
      '    RETENTION_PERIOD,'
      '    DESTROY_DATE,'
      '    VOLUME_ARCHIVE,'
      '    VOLUME_DECREE,'
      '    VOLUME_PLACE,'
      '    CATEGORY_ID,'
      '    CATEGORY_ID2,'
      '    STAT_LINE,'
      '    JUDGE_ID,'
      '    CLERK,'
      '    COMMENT,'
      '    SORT_ORDER,'
      '    SYS_STAT3_IS_MATERIAL,'
      '    ORIGIN_DATE,'
      '    PASS_WRITEXEC_DATE,'
      '    WRITEXEC_COMMENT,'
      '    RESULT_N_DATE,'
      '    RESULT_N_ID,'
      '    S_PR_HEAR_DATE,'
      '    S_HEARING_DATE,'
      '    SYS_EVENT_DATE,'
      '    SYS_EVENT_NAME_ID,'
      '    SYS_EVENT_RESULT_ID,'
      '    FIRST_HEARING_DATE,'
      '    CIRCUM_DEF_DATE,'
      '    CIRCUM_DEF_ID,'
      '    COMPLICATION_DATE,'
      '    PERIOD_OF_VALIDITY_ID,'
      '    ADJUTANT_ID,'
      '    CASE_ATTR,'
      '    THEFT_DAMAGE,'
      '    ANOTHER_DAMAGE,'
      '    AMOUNT_DAMAGE,'
      '    A,'
      '    RET_WRITEXEC_DATE,'
      '    EXEC_DATE,'
      '    EXEC_REASON,'
      '    EXEC_TYPE_COMMENT,'
      '    DUTY_WHO,'
      '    PASS_WRITEXEC_DATE2,'
      '    PASS_EXEC_DATE,'
      '    EXEC_MONEY,'
      '    EXEC_MONEY2,'
      '    WEIGHT'
      'FROM'
      '    G1_CASE '
      'where '
      '  ID=:CaseId')
    AfterInsert = tCaseAfterInsert
    Transaction = dmMain.trMain
    Database = dmMain.dbUni
    DataSource = dsLetter
    Left = 320
    Top = 9
    object tCaseID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tCaseFULL_NUMBER: TFIBStringField
      FieldName = 'FULL_NUMBER'
      Size = 250
      EmptyStrToNull = True
    end
    object tCaseCASE_FULL_NUMBER: TFIBStringField
      FieldName = 'CASE_FULL_NUMBER'
      Size = 250
      EmptyStrToNull = True
    end
    object tCaseSHORT_NUMBER: TFIBIntegerField
      FieldName = 'SHORT_NUMBER'
    end
    object tCaseYEAR_REG: TFIBIntegerField
      FieldName = 'YEAR_REG'
    end
    object tCaseM_FULL_NUMBER: TFIBStringField
      FieldName = 'M_FULL_NUMBER'
      Size = 50
      EmptyStrToNull = True
    end
    object tCaseM_SHORT_NUMBER: TFIBIntegerField
      FieldName = 'M_SHORT_NUMBER'
    end
    object tCaseM_YEAR_REG: TFIBIntegerField
      FieldName = 'M_YEAR_REG'
    end
    object tCaseCASE_TYPE_ID: TFIBIntegerField
      FieldName = 'CASE_TYPE_ID'
    end
    object tCaseENTRY_DATE: TFIBDateTimeField
      FieldName = 'ENTRY_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseENTRY_TYPE_ID: TFIBIntegerField
      FieldName = 'ENTRY_TYPE_ID'
    end
    object tCasePREV_FULL_NUMBER: TFIBStringField
      FieldName = 'PREV_FULL_NUMBER'
      Size = 250
      EmptyStrToNull = True
    end
    object tCaseFOLDER_ID: TFIBIntegerField
      FieldName = 'FOLDER_ID'
    end
    object tCaseO_SEND_DATE: TFIBDateTimeField
      FieldName = 'O_SEND_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseO_ACT_EXEC_DATE: TFIBDateTimeField
      FieldName = 'O_ACT_EXEC_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseS_DECISION_147_DATE: TFIBDateTimeField
      FieldName = 'S_DECISION_147_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseS_DECISION_152_DATE: TFIBDateTimeField
      FieldName = 'S_DECISION_152_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseS_PR_HEAR_RESULT_ID: TFIBIntegerField
      FieldName = 'S_PR_HEAR_RESULT_ID'
    end
    object tCaseS_PR_HEAR_RESULT_ID2: TFIBIntegerField
      FieldName = 'S_PR_HEAR_RESULT_ID2'
    end
    object tCaseS_DECISION_153_DATE: TFIBDateTimeField
      FieldName = 'S_DECISION_153_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseS_CLAIM_CHANGED_DATE: TFIBDateTimeField
      FieldName = 'S_CLAIM_CHANGED_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseA_CASE_NUMBER_I: TFIBStringField
      FieldName = 'A_CASE_NUMBER_I'
      Size = 80
      EmptyStrToNull = True
    end
    object tCaseA_JUDGE_I_ID: TFIBIntegerField
      FieldName = 'A_JUDGE_I_ID'
    end
    object tCaseA_RESULT_I_DATE: TFIBDateTimeField
      FieldName = 'A_RESULT_I_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseA_RESULT_I_ID: TFIBIntegerField
      FieldName = 'A_RESULT_I_ID'
    end
    object tCaseA_ORDER_TYPE_ID: TFIBIntegerField
      FieldName = 'A_ORDER_TYPE_ID'
    end
    object tCaseA_APPELANT_ID: TFIBIntegerField
      FieldName = 'A_APPELANT_ID'
    end
    object tCaseA_WHAT_APPEAL_DATE: TFIBDateTimeField
      FieldName = 'A_WHAT_APPEAL_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseA_WHAT_APPEAL_ID: TFIBIntegerField
      FieldName = 'A_WHAT_APPEAL_ID'
    end
    object tCaseA_SUBSTANTIATION_ID: TFIBIntegerField
      FieldName = 'A_SUBSTANTIATION_ID'
    end
    object tCaseA_RETURNED_DATE: TFIBDateTimeField
      FieldName = 'A_RETURNED_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseA_RET_JUDGE_ID: TFIBIntegerField
      FieldName = 'A_RET_JUDGE_ID'
    end
    object tCaseSUIT_MONEY: TFIBFloatField
      FieldName = 'SUIT_MONEY'
    end
    object tCaseDUTY: TFIBFloatField
      FieldName = 'DUTY'
    end
    object tCaseREQ_ESSENCE: TFIBStringField
      FieldName = 'REQ_ESSENCE'
      Size = 1024
      EmptyStrToNull = True
    end
    object tCaseDECISION_DATE: TFIBDateTimeField
      FieldName = 'DECISION_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseDECISION_ID: TFIBIntegerField
      FieldName = 'DECISION_ID'
    end
    object tCaseDECISION_ID2: TFIBIntegerField
      FieldName = 'DECISION_ID2'
    end
    object tCaseRESULT_DATE: TFIBDateTimeField
      FieldName = 'RESULT_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseRESULT_TYPE_ID: TFIBIntegerField
      FieldName = 'RESULT_TYPE_ID'
    end
    object tCaseCOURT_STAFF_ID: TFIBIntegerField
      FieldName = 'COURT_STAFF_ID'
    end
    object tCaseREQ_DISAFF_DATE: TFIBDateTimeField
      FieldName = 'REQ_DISAFF_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseREQ_DISAFF: TFIBStringField
      FieldName = 'REQ_DISAFF'
      Size = 1024
      EmptyStrToNull = True
    end
    object tCaseDISORDER_DATE: TFIBDateTimeField
      FieldName = 'DISORDER_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseRESULT_ID: TFIBIntegerField
      FieldName = 'RESULT_ID'
    end
    object tCaseRESULT_ID2: TFIBIntegerField
      FieldName = 'RESULT_ID2'
    end
    object tCaseRESULT_ESSENCE: TFIBStringField
      FieldName = 'RESULT_ESSENCE'
      Size = 6144
      EmptyStrToNull = True
    end
    object tCaseRETURN_OFFICE_DATE: TFIBDateTimeField
      FieldName = 'RETURN_OFFICE_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCasePAYMENT_BY_RESULT: TFIBFloatField
      FieldName = 'PAYMENT_BY_RESULT'
    end
    object tCasePAYMENT_DUTY: TFIBFloatField
      FieldName = 'PAYMENT_DUTY'
    end
    object tCasePAYMENT_COSTS: TFIBFloatField
      FieldName = 'PAYMENT_COSTS'
    end
    object tCasePAYMENT_PENALTY: TFIBFloatField
      FieldName = 'PAYMENT_PENALTY'
    end
    object tCaseVALIDITY_DATE: TFIBDateTimeField
      FieldName = 'VALIDITY_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCasePREPARE_ARCH_DATE: TFIBDateTimeField
      FieldName = 'PREPARE_ARCH_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseARCHIVE_DATE: TFIBDateTimeField
      FieldName = 'ARCHIVE_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseVOLUME_TO_SHELVE: TFIBStringField
      FieldName = 'VOLUME_TO_SHELVE'
      Size = 50
      EmptyStrToNull = True
    end
    object tCaseRETENTION_PERIOD: TFIBStringField
      FieldName = 'RETENTION_PERIOD'
      Size = 50
      EmptyStrToNull = True
    end
    object tCaseDESTROY_DATE: TFIBDateTimeField
      FieldName = 'DESTROY_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseVOLUME_ARCHIVE: TFIBStringField
      FieldName = 'VOLUME_ARCHIVE'
      Size = 50
      EmptyStrToNull = True
    end
    object tCaseVOLUME_DECREE: TFIBStringField
      FieldName = 'VOLUME_DECREE'
      Size = 50
      EmptyStrToNull = True
    end
    object tCaseVOLUME_PLACE: TFIBStringField
      FieldName = 'VOLUME_PLACE'
      Size = 50
      EmptyStrToNull = True
    end
    object tCaseCATEGORY_ID: TFIBIntegerField
      FieldName = 'CATEGORY_ID'
    end
    object tCaseSTAT_LINE: TFIBIntegerField
      FieldName = 'STAT_LINE'
    end
    object tCaseJUDGE_ID: TFIBIntegerField
      FieldName = 'JUDGE_ID'
    end
    object tCaseCLERK: TFIBIntegerField
      FieldName = 'CLERK'
    end
    object tCaseCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 512
      EmptyStrToNull = True
    end
    object tCaseSORT_ORDER: TFIBIntegerField
      FieldName = 'SORT_ORDER'
    end
    object tCaseSYS_STAT3_IS_MATERIAL: TFIBIntegerField
      FieldName = 'SYS_STAT3_IS_MATERIAL'
    end
    object tCaseORIGIN_DATE: TFIBDateTimeField
      FieldName = 'ORIGIN_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCasePASS_WRITEXEC_DATE: TFIBDateTimeField
      FieldName = 'PASS_WRITEXEC_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseWRITEXEC_COMMENT: TFIBStringField
      FieldName = 'WRITEXEC_COMMENT'
      Size = 80
      EmptyStrToNull = True
    end
    object tCaseRESULT_N_DATE: TFIBDateTimeField
      FieldName = 'RESULT_N_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseRESULT_N_ID: TFIBIntegerField
      FieldName = 'RESULT_N_ID'
    end
    object tCaseS_PR_HEAR_DATE: TFIBDateTimeField
      FieldName = 'S_PR_HEAR_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseS_HEARING_DATE: TFIBDateTimeField
      FieldName = 'S_HEARING_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseSYS_EVENT_DATE: TFIBDateTimeField
      FieldName = 'SYS_EVENT_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseSYS_EVENT_NAME_ID: TFIBIntegerField
      FieldName = 'SYS_EVENT_NAME_ID'
    end
    object tCaseSYS_EVENT_RESULT_ID: TFIBIntegerField
      FieldName = 'SYS_EVENT_RESULT_ID'
    end
    object tCaseFIRST_HEARING_DATE: TFIBDateTimeField
      FieldName = 'FIRST_HEARING_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseCIRCUM_DEF_DATE: TFIBDateTimeField
      FieldName = 'CIRCUM_DEF_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseCIRCUM_DEF_ID: TFIBIntegerField
      FieldName = 'CIRCUM_DEF_ID'
    end
    object tCaseCOMPLICATION_DATE: TFIBDateTimeField
      FieldName = 'COMPLICATION_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCasePERIOD_OF_VALIDITY_ID: TFIBIntegerField
      FieldName = 'PERIOD_OF_VALIDITY_ID'
    end
    object tCaseADJUTANT_ID: TFIBIntegerField
      FieldName = 'ADJUTANT_ID'
    end
    object tCaseCASE_ATTR: TFIBIntegerField
      FieldName = 'CASE_ATTR'
    end
    object tCaseTHEFT_DAMAGE: TFIBFloatField
      FieldName = 'THEFT_DAMAGE'
    end
    object tCaseANOTHER_DAMAGE: TFIBFloatField
      FieldName = 'ANOTHER_DAMAGE'
    end
    object tCaseAMOUNT_DAMAGE: TFIBFloatField
      FieldName = 'AMOUNT_DAMAGE'
    end
    object tCaseA: TFIBIntegerField
      FieldName = 'A'
    end
    object tCaseRET_WRITEXEC_DATE: TFIBDateTimeField
      FieldName = 'RET_WRITEXEC_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseEXEC_DATE: TFIBDateTimeField
      FieldName = 'EXEC_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseEXEC_REASON: TFIBStringField
      FieldName = 'EXEC_REASON'
      Size = 80
      EmptyStrToNull = True
    end
    object tCaseEXEC_TYPE_COMMENT: TFIBStringField
      FieldName = 'EXEC_TYPE_COMMENT'
      Size = 80
      EmptyStrToNull = True
    end
    object tCaseDUTY_WHO: TFIBStringField
      FieldName = 'DUTY_WHO'
      Size = 80
      EmptyStrToNull = True
    end
    object tCasePASS_WRITEXEC_DATE2: TFIBDateTimeField
      FieldName = 'PASS_WRITEXEC_DATE2'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCasePASS_EXEC_DATE: TFIBDateTimeField
      FieldName = 'PASS_EXEC_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tCaseEXEC_MONEY: TFIBFloatField
      FieldName = 'EXEC_MONEY'
    end
    object tCaseEXEC_MONEY2: TFIBFloatField
      FieldName = 'EXEC_MONEY2'
    end
    object tCaseWEIGHT: TFIBFloatField
      FieldName = 'WEIGHT'
    end
    object tCaseCATEGORY_ID2: TFIBIntegerField
      FieldName = 'CATEGORY_ID2'
    end
  end
  object dsCase: TDataSource
    DataSet = tCase
    Left = 320
    Top = 39
  end
  object tPart: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE G1_PARTS'
      'SET '
      '    CASE_ID = :CASE_ID,'
      '    PARTS_TYPE_ID = :PARTS_TYPE_ID,'
      '    NAME = :NAME,'
      '    BIRTH_DATE = :BIRTH_DATE,'
      '    PASSPORT = :PASSPORT,'
      '    ADDRESS = :ADDRESS,'
      '    REQUIREMENTS = :REQUIREMENTS,'
      '    COMMENTS = :COMMENTS,'
      '    NATIVE = :NATIVE,'
      '    PLACE_EMPLOY = :PLACE_EMPLOY,'
      '    REPRESENT = :REPRESENT,'
      '    PARTS_STATUS_ID = :PARTS_STATUS_ID,'
      '    COSTS = :COSTS,'
      '    COSTS_REASON = :COSTS_REASON,'
      '    COSTS_DAYS = :COSTS_DAYS,'
      '    SMS_TELNUM = :SMS_TELNUM,'
      '    EMAIL_STR = :EMAIL_STR'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    G1_PARTS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO G1_PARTS('
      '    ID,'
      '    CASE_ID,'
      '    PARTS_TYPE_ID,'
      '    NAME,'
      '    BIRTH_DATE,'
      '    PASSPORT,'
      '    ADDRESS,'
      '    REQUIREMENTS,'
      '    COMMENTS,'
      '    NATIVE,'
      '    PLACE_EMPLOY,'
      '    REPRESENT,'
      '    PARTS_STATUS_ID,'
      '    COSTS,'
      '    COSTS_REASON,'
      '    COSTS_DAYS,'
      '    SMS_TELNUM,'
      '    EMAIL_STR'
      ')'
      'VALUES('
      '    :ID,'
      '    :CASE_ID,'
      '    :PARTS_TYPE_ID,'
      '    :NAME,'
      '    :BIRTH_DATE,'
      '    :PASSPORT,'
      '    :ADDRESS,'
      '    :REQUIREMENTS,'
      '    :COMMENTS,'
      '    :NATIVE,'
      '    :PLACE_EMPLOY,'
      '    :REPRESENT,'
      '    :PARTS_STATUS_ID,'
      '    :COSTS,'
      '    :COSTS_REASON,'
      '    :COSTS_DAYS,'
      '    :SMS_TELNUM,'
      '    :EMAIL_STR'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    PARTS_TYPE_ID,'
      '    NAME,'
      '    BIRTH_DATE,'
      '    PASSPORT,'
      '    ADDRESS,'
      '    REQUIREMENTS,'
      '    COMMENTS,'
      '    NATIVE,'
      '    PLACE_EMPLOY,'
      '    REPRESENT,'
      '    PARTS_STATUS_ID,'
      '    COSTS,'
      '    COSTS_REASON,'
      '    COSTS_DAYS,'
      '    SMS_TELNUM,'
      '    EMAIL_STR'
      'FROM'
      '    G1_PARTS '
      'where(  CASE_ID=:ID'
      '     ) and (     G1_PARTS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    PARTS_TYPE_ID,'
      '    NAME,'
      '    BIRTH_DATE,'
      '    PASSPORT,'
      '    ADDRESS,'
      '    REQUIREMENTS,'
      '    COMMENTS,'
      '    NATIVE,'
      '    PLACE_EMPLOY,'
      '    REPRESENT,'
      '    PARTS_STATUS_ID,'
      '    COSTS,'
      '    COSTS_REASON,'
      '    COSTS_DAYS,'
      '    SMS_TELNUM,'
      '    EMAIL_STR'
      'FROM'
      '    G1_PARTS '
      'where CASE_ID=:ID')
    AfterInsert = tPartAfterInsert
    Transaction = dmMain.trMain
    Database = dmMain.dbUni
    DataSource = dsCase
    Left = 392
    Top = 9
    object tPartID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tPartCASE_ID: TFIBIntegerField
      FieldName = 'CASE_ID'
    end
    object tPartPARTS_TYPE_ID: TFIBIntegerField
      FieldName = 'PARTS_TYPE_ID'
    end
    object tPartNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 250
      EmptyStrToNull = True
    end
    object tPartBIRTH_DATE: TFIBDateTimeField
      FieldName = 'BIRTH_DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tPartPASSPORT: TFIBStringField
      FieldName = 'PASSPORT'
      Size = 250
      EmptyStrToNull = True
    end
    object tPartADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Size = 250
      EmptyStrToNull = True
    end
    object tPartREQUIREMENTS: TFIBStringField
      FieldName = 'REQUIREMENTS'
      Size = 1024
      EmptyStrToNull = True
    end
    object tPartCOMMENTS: TFIBStringField
      FieldName = 'COMMENTS'
      Size = 1024
      EmptyStrToNull = True
    end
    object tPartNATIVE: TFIBStringField
      FieldName = 'NATIVE'
      Size = 80
      EmptyStrToNull = True
    end
    object tPartPLACE_EMPLOY: TFIBStringField
      FieldName = 'PLACE_EMPLOY'
      Size = 250
      EmptyStrToNull = True
    end
    object tPartREPRESENT: TFIBStringField
      FieldName = 'REPRESENT'
      Size = 80
      EmptyStrToNull = True
    end
    object tPartPARTS_STATUS_ID: TFIBIntegerField
      FieldName = 'PARTS_STATUS_ID'
    end
    object tPartCOSTS: TFIBFloatField
      FieldName = 'COSTS'
    end
    object tPartCOSTS_REASON: TFIBStringField
      FieldName = 'COSTS_REASON'
      Size = 250
      EmptyStrToNull = True
    end
    object tPartCOSTS_DAYS: TFIBIntegerField
      FieldName = 'COSTS_DAYS'
    end
    object tPartParts_type: TStringField
      FieldKind = fkLookup
      FieldName = 'Parts_type'
      LookupDataSet = dmMain.tPARTS_TYPE
      LookupKeyFields = 'CONTENTID'
      LookupResultField = 'NAME'
      KeyFields = 'PARTS_TYPE_ID'
      Size = 256
      Lookup = True
    end
    object tPartTELEFON_ADD: TFIBStringField
      FieldName = 'SMS_TELNUM'
      EmptyStrToNull = True
    end
    object tPartEMAIL_ADD: TFIBStringField
      FieldName = 'EMAIL_STR'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object dsPart: TDataSource
    DataSet = tPart
    Left = 392
    Top = 39
  end
  object pFIBQuery1: TpFIBQuery
    Transaction = dmMain.trMain
    Database = dmMain.dbAdd
    Left = 761
    Top = 16
  end
  object pmCaseNumber: TPopupMenu
    Left = 617
    Top = 24
    object N1: TMenuItem
      Caption = #1042#1099#1073#1088#1072#1090#1100
    end
    object N2: TMenuItem
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    end
  end
  object pmImageDoc: TPopupMenu
    Left = 665
    Top = 24
  end
  object ActionManager1: TActionManager
    Left = 704
    Top = 8
    StyleName = 'XP Style'
    object Gr1CaseDocumentList: TAction
      Category = 'Gr1'
      Caption = #1057#1087#1080#1089#1086#1082' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074' '#1076#1077#1083#1072
    end
    object aLoadImage: TFileOpen
      Category = 'Common'
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1079#1086#1073#1088#1072#1078#1077#1085#1080#1077
      Dialog.DefaultExt = '.tif'
      Dialog.Filter = #1048#1079#1086#1073#1088#1072#1078#1077#1085#1080#1103'|*.tif'
      Dialog.Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
      ImageIndex = 0
      BeforeExecute = aLoadImageBeforeExecute
      OnAccept = aLoadImageAccept
    end
    object aDelImage: TAction
      Category = 'Common'
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079#1086#1073#1088#1072#1078#1077#1085#1080#1077
      ImageIndex = 1
      OnExecute = aDelImageExecute
    end
    object aViewImage: TAction
      Category = 'Common'
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1080#1079#1086#1073#1088#1072#1078#1077#1085#1080#1103
      ImageIndex = 2
      OnExecute = aViewImageExecute
    end
  end
  object tDocText: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE DOCTEXT'
      'SET '
      '    ATTACH = :ATTACH,'
      '    FILENAME = :FILENAME,'
      '    CARDID = :CARDID,'
      '    DOCUMENTID = :DOCUMENTID,'
      '    DOCTYPE = :DOCTYPE,'
      '    DOCNAME = :DOCNAME,'
      '    LETTERID = :LETTERID'
      'WHERE'
      '    OBJECTID = :OLD_OBJECTID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    DOCTEXT'
      'WHERE'
      '        OBJECTID = :OLD_OBJECTID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO DOCTEXT('
      '    OBJECTID,'
      '    ATTACH,'
      '    FILENAME,'
      '    CARDID,'
      '    DOCUMENTID,'
      '    DOCTYPE,'
      '    DOCNAME,'
      '    LETTERID'
      ')'
      'VALUES('
      '    :OBJECTID,'
      '    :ATTACH,'
      '    :FILENAME,'
      '    :CARDID,'
      '    :DOCUMENTID,'
      '    :DOCTYPE,'
      '    :DOCNAME,'
      '    :LETTERID'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    OBJECTID,'
      '    ATTACH,'
      '    FILENAME,'
      '    CARDID,'
      '    DOCUMENTID,'
      '    DOCTYPE,'
      '    DOCNAME,'
      '    LETTERID'
      'FROM'
      '    DOCTEXT '
      'where(  LETTERID=:LETTERID'
      '     ) and (     DOCTEXT.OBJECTID = :OLD_OBJECTID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    OBJECTID,'
      '    ATTACH,'
      '    FILENAME,'
      '    CARDID,'
      '    DOCUMENTID,'
      '    DOCTYPE,'
      '    DOCNAME,'
      '    LETTERID'
      'FROM'
      '    DOCTEXT '
      'where LETTERID=:OBJECTID')
    AfterScroll = tDocTextAfterScroll
    BeforePost = tDocTextBeforePost
    Transaction = dmMain.trAdd
    Database = dmMain.dbAdd
    DataSource = dsLetter
    Left = 496
    Top = 9
    object tDocTextOBJECTID: TFIBIntegerField
      FieldName = 'OBJECTID'
    end
    object tDocTextATTACH: TFIBBlobField
      FieldName = 'ATTACH'
      Size = 8
    end
    object tDocTextFILENAME: TFIBStringField
      FieldName = 'FILENAME'
      Size = 256
      EmptyStrToNull = True
    end
    object tDocTextCARDID: TFIBIntegerField
      FieldName = 'CARDID'
    end
    object tDocTextDOCUMENTID: TFIBIntegerField
      FieldName = 'DOCUMENTID'
    end
    object tDocTextDOCTYPE: TFIBIntegerField
      FieldName = 'DOCTYPE'
    end
    object tDocTextDOCNAME: TFIBStringField
      FieldName = 'DOCNAME'
      Size = 512
      EmptyStrToNull = True
    end
    object tDocTextLETTERID: TFIBIntegerField
      FieldName = 'LETTERID'
    end
  end
  object tDocument: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT  @@Column%FULL_NUMBER@ as CaseNum'
      ' FROM @@Catalog%G1@_CASE'
      ' where ID=:ID')
    Transaction = dmMain.trMain
    Database = dmMain.dbUni
    Left = 536
    Top = 9
    object tDocumentCASENUM: TFIBStringField
      FieldName = 'CASENUM'
      Size = 250
      EmptyStrToNull = True
    end
  end
  object dsDocText: TDataSource
    AutoEdit = False
    DataSet = tDocText
    Left = 496
    Top = 39
  end
  object dsDocument: TDataSource
    DataSet = tDocument
    Left = 536
    Top = 39
  end
  object tExcistParty: TpFIBDataSet
    SelectSQL.Strings = (
      'select c.full_number, c.entry_date,'
      
        '    case when c.case_type_id=50520001 then '#39#1048#1089#1082#1086#1074#1086#1077' '#1079#1072#1103#1074#1083#1077#1085#1080#1077' '#39' ' +
        'else '#39#1047#1072#1103#1074#1083#1077#1085#1080#1077' '#39' end'
      
        '    ||(SELECT PLAINTIFF FROM g1_parts_on_case_or_full_number (c.' +
        'ID, '#39#39',1))'
      
        '    ||case when (SELECT respondent FROM g1_parts_on_case_or_full' +
        '_number (c.ID, '#39#39',1))='#39#39' then  '#39#39
      
        '      else '#39' '#1082' '#39'||(SELECT respondent FROM g1_parts_on_case_or_fu' +
        'll_number (c.ID, '#39#39',1)) end'
      '    ||'#39' '#39'|| coalesce(c.req_essence,'#39#39') as r, '
      '    c.req_essence, '
      '    j.username as judge'
      
        'from g1_case c left outer join groupcontent j on (c.judge_id = j' +
        '.groupcontentid)'
      'where c.id in (select p.case_id '
      '                from g1_parts p '
      '                where upper(p.name)starting with :pname'
      '                   or upper(p.name)starting with :pname2)')
    Transaction = dmMain.trMain
    Database = dmMain.dbUni
    Left = 272
    Top = 321
    object tExcistPartyFULL_NUMBER: TFIBStringField
      FieldName = 'FULL_NUMBER'
      Size = 250
      EmptyStrToNull = True
    end
    object tExcistPartyENTRY_DATE: TFIBDateTimeField
      FieldName = 'ENTRY_DATE'
    end
    object tExcistPartyR: TFIBStringField
      FieldName = 'R'
      Size = 21526
      EmptyStrToNull = True
    end
    object tExcistPartyJUDGE: TFIBStringField
      FieldName = 'JUDGE'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object DataSource1: TDataSource
    DataSet = tExcistParty
    Left = 272
    Top = 351
  end
  object tClaim: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE G1_CLAIM'
      'SET '
      '    CASE_ID = :CASE_ID,'
      '    CLAIM_TYPE_ID = :CLAIM_TYPE_ID,'
      '    REQUIREMENTS = :REQUIREMENTS,'
      '    RESULT_DATE = :RESULT_DATE,'
      '    RESULT_ESSENCE = :RESULT_ESSENCE,'
      '    RESULT_ID = :RESULT_ID,'
      '    DUMMY01 = :DUMMY01,'
      '    DUMMY02 = :DUMMY02,'
      '    CATEGORY_ID = :CATEGORY_ID,'
      '    CATEGORY_ID2 = :CATEGORY_ID2,'
      '    RESULT_ID2 = :RESULT_ID2,'
      '    A_WHAT_APPEAL_DATE = :A_WHAT_APPEAL_DATE,'
      '    A_WHAT_APPEAL_ID = :A_WHAT_APPEAL_ID,'
      '    STAT_LINE = :STAT_LINE,'
      '    A_APPELANT_ID = :A_APPELANT_ID,'
      '    WRIT_TYPE = :WRIT_TYPE,'
      '    COMMENT = :COMMENT,'
      '    A_JUDGE_I_ID = :A_JUDGE_I_ID,'
      '    DUMMY03 = :DUMMY03,'
      '    DUMMY04 = :DUMMY04,'
      '    DUMMY05 = :DUMMY05,'
      '    DUMMY06 = :DUMMY06,'
      '    ENTRY_DATE = :ENTRY_DATE,'
      '    SYS_COMPLAINT_TYPE = :SYS_COMPLAINT_TYPE,'
      '    SUIT_MONEY = :SUIT_MONEY'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    G1_CLAIM'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO G1_CLAIM('
      '    ID,'
      '    CASE_ID,'
      '    CLAIM_TYPE_ID,'
      '    REQUIREMENTS,'
      '    RESULT_DATE,'
      '    RESULT_ESSENCE,'
      '    RESULT_ID,'
      '    DUMMY01,'
      '    DUMMY02,'
      '    CATEGORY_ID,'
      '    CATEGORY_ID2,'
      '    RESULT_ID2,'
      '    A_WHAT_APPEAL_DATE,'
      '    A_WHAT_APPEAL_ID,'
      '    STAT_LINE,'
      '    A_APPELANT_ID,'
      '    WRIT_TYPE,'
      '    COMMENT,'
      '    A_JUDGE_I_ID,'
      '    DUMMY03,'
      '    DUMMY04,'
      '    DUMMY05,'
      '    DUMMY06,'
      '    ENTRY_DATE,'
      '    SYS_COMPLAINT_TYPE,'
      '    SUIT_MONEY'
      ')'
      'VALUES('
      '    :ID,'
      '    :CASE_ID,'
      '    :CLAIM_TYPE_ID,'
      '    :REQUIREMENTS,'
      '    :RESULT_DATE,'
      '    :RESULT_ESSENCE,'
      '    :RESULT_ID,'
      '    :DUMMY01,'
      '    :DUMMY02,'
      '    :CATEGORY_ID,'
      '    :CATEGORY_ID2,'
      '    :RESULT_ID2,'
      '    :A_WHAT_APPEAL_DATE,'
      '    :A_WHAT_APPEAL_ID,'
      '    :STAT_LINE,'
      '    :A_APPELANT_ID,'
      '    :WRIT_TYPE,'
      '    :COMMENT,'
      '    :A_JUDGE_I_ID,'
      '    :DUMMY03,'
      '    :DUMMY04,'
      '    :DUMMY05,'
      '    :DUMMY06,'
      '    :ENTRY_DATE,'
      '    :SYS_COMPLAINT_TYPE,'
      '    :SUIT_MONEY'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    CLAIM_TYPE_ID,'
      '    REQUIREMENTS,'
      '    RESULT_DATE,'
      '    RESULT_ESSENCE,'
      '    RESULT_ID,'
      '    DUMMY01,'
      '    DUMMY02,'
      '    CATEGORY_ID,'
      '    CATEGORY_ID2,'
      '    RESULT_ID2,'
      '    A_WHAT_APPEAL_DATE,'
      '    A_WHAT_APPEAL_ID,'
      '    STAT_LINE,'
      '    A_APPELANT_ID,'
      '    WRIT_TYPE,'
      '    COMMENT,'
      '    A_JUDGE_I_ID,'
      '    DUMMY03,'
      '    DUMMY04,'
      '    DUMMY05,'
      '    DUMMY06,'
      '    ENTRY_DATE,'
      '    SYS_COMPLAINT_TYPE,'
      '    SUIT_MONEY'
      'FROM'
      '    G1_CLAIM '
      'where'
      '  ID = :OLD_ID'
      '     '
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    CLAIM_TYPE_ID,'
      '    REQUIREMENTS,'
      '    RESULT_DATE,'
      '    RESULT_ESSENCE,'
      '    RESULT_ID,'
      '    DUMMY01,'
      '    DUMMY02,'
      '    CATEGORY_ID,'
      '    CATEGORY_ID2,'
      '    RESULT_ID2,'
      '    A_WHAT_APPEAL_DATE,'
      '    A_WHAT_APPEAL_ID,'
      '    STAT_LINE,'
      '    A_APPELANT_ID,'
      '    WRIT_TYPE,'
      '    COMMENT,'
      '    A_JUDGE_I_ID,'
      '    DUMMY03,'
      '    DUMMY04,'
      '    DUMMY05,'
      '    DUMMY06,'
      '    ENTRY_DATE,'
      '    SYS_COMPLAINT_TYPE,'
      '    SUIT_MONEY'
      'FROM'
      '    G1_CLAIM '
      'where'
      '  CASE_ID=:CaseId')
    AfterInsert = tClaimAfterInsert
    BeforePost = tClaimBeforePost
    Transaction = dmMain.trMain
    Database = dmMain.dbUni
    DataSource = dsLetter
    Left = 352
    Top = 9
    object tClaimID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tClaimCASE_ID: TFIBIntegerField
      FieldName = 'CASE_ID'
    end
    object tClaimCLAIM_TYPE_ID: TFIBIntegerField
      FieldName = 'CLAIM_TYPE_ID'
    end
    object tClaimREQUIREMENTS: TFIBStringField
      FieldName = 'REQUIREMENTS'
      Size = 1024
      EmptyStrToNull = True
    end
    object tClaimRESULT_DATE: TFIBDateTimeField
      FieldName = 'RESULT_DATE'
    end
    object tClaimRESULT_ESSENCE: TFIBStringField
      FieldName = 'RESULT_ESSENCE'
      Size = 6144
      EmptyStrToNull = True
    end
    object tClaimRESULT_ID: TFIBIntegerField
      FieldName = 'RESULT_ID'
    end
    object tClaimDUMMY01: TFIBIntegerField
      FieldName = 'DUMMY01'
    end
    object tClaimDUMMY02: TFIBIntegerField
      FieldName = 'DUMMY02'
    end
    object tClaimCATEGORY_ID: TFIBIntegerField
      FieldName = 'CATEGORY_ID'
    end
    object tClaimCATEGORY_ID2: TFIBIntegerField
      FieldName = 'CATEGORY_ID2'
    end
    object tClaimRESULT_ID2: TFIBIntegerField
      FieldName = 'RESULT_ID2'
    end
    object tClaimA_WHAT_APPEAL_DATE: TFIBDateTimeField
      FieldName = 'A_WHAT_APPEAL_DATE'
    end
    object tClaimA_WHAT_APPEAL_ID: TFIBIntegerField
      FieldName = 'A_WHAT_APPEAL_ID'
    end
    object tClaimSTAT_LINE: TFIBIntegerField
      FieldName = 'STAT_LINE'
    end
    object tClaimA_APPELANT_ID: TFIBIntegerField
      FieldName = 'A_APPELANT_ID'
    end
    object tClaimWRIT_TYPE: TFIBIntegerField
      FieldName = 'WRIT_TYPE'
    end
    object tClaimCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 2048
      EmptyStrToNull = True
    end
    object tClaimA_JUDGE_I_ID: TFIBIntegerField
      FieldName = 'A_JUDGE_I_ID'
    end
    object tClaimDUMMY03: TFIBIntegerField
      FieldName = 'DUMMY03'
    end
    object tClaimDUMMY04: TFIBIntegerField
      FieldName = 'DUMMY04'
    end
    object tClaimDUMMY05: TFIBIntegerField
      FieldName = 'DUMMY05'
    end
    object tClaimDUMMY06: TFIBIntegerField
      FieldName = 'DUMMY06'
    end
    object tClaimENTRY_DATE: TFIBDateTimeField
      FieldName = 'ENTRY_DATE'
    end
    object tClaimSYS_COMPLAINT_TYPE: TFIBIntegerField
      FieldName = 'SYS_COMPLAINT_TYPE'
    end
    object tClaimSUIT_MONEY: TFIBFloatField
      FieldName = 'SUIT_MONEY'
    end
  end
  object dsClaim: TDataSource
    DataSet = tClaim
    OnDataChange = dsClaimDataChange
    Left = 352
    Top = 39
  end
  object pG1_CLAIM_BP01: TpFIBQuery
    Transaction = dmMain.trMain
    Database = dmMain.dbUni
    SQL.Strings = (
      'SELECT'
      '    CA__STAT_LINE,'
      '    CA__CATEGORY_ID,'
      '    CA__CATEGORY_ID2,'
      '    CA__REQ_ESSENCE,'
      '    CA__RESULT_ESSENCE,'
      '    CA__SUIT_MONEY,'
      '    CL__STAT_LINE,'
      '    CA__STATE_SECRET_ID'
      'FROM'
      '    G1_CLAIM_BP01(:CA__CASE_TYPE_ID,'
      '    :CL__CATEGORY_ID,'
      '    :CA__RESULT_DATE,'
      '    :CA__CASE_ID,'
      '    :CL__ID,'
      '    :CL__CLAIM_TYPE_ID,'
      '    :CL__CATEGORY_ID2,'
      '    :CL__REQUIREMENTS,'
      '    :CL__RESULT_ESSENCE,'
      '    :CL__SUIT_MONEY,'
      '    :CL__STATE_SECRET_ID) ')
    Left = 577
    Top = 280
  end
end
