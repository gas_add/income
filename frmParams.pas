unit frmParams;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, FIBDataSet, pFIBDataSet, Grids, DBGrids, StdCtrls, ComCtrls,
  ExtCtrls, pFIBQuery;

type
  TfParam = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    PageControl1: TPageControl;
    tsParams: TTabSheet;
    tsJournals: TTabSheet;
    ComboBox1: TComboBox;
    DBGrid1: TDBGrid;
    tAutoPost: TpFIBDataSet;
    dsAutoPost: TDataSource;
    tAutoPostID: TFIBIntegerField;
    tAutoPostPOSTID: TFIBIntegerField;
    tAutoPostPOSTVALUE: TFIBStringField;
    tPostType: TpFIBDataSet;
    dsPostType: TDataSource;
    DBGrid2: TDBGrid;
    TabSheet1: TTabSheet;
    DBGrid3: TDBGrid;
    tJournal: TpFIBDataSet;
    dsJournal: TDataSource;
    tJournalOBJECTID: TFIBIntegerField;
    tJournalPOSTTYPENAME: TFIBStringField;
    tJournalCODE: TFIBStringField;
    tJournalJOURNALTYPE: TFIBSmallIntField;
    tJournalARCHIVED2: TFIBSmallIntField;
    tPostTypeOBJECTID: TFIBIntegerField;
    tPostTypePOSTTYPENAME: TFIBStringField;
    tPostTypeJOURNALID: TFIBSmallIntField;
    tPostTypeARCHIVED: TFIBSmallIntField;
    tPostTypeJournal: TStringField;
    tPostTypePOSTFORMID: TFIBSmallIntField;
    tPostTypeADDTOCASE: TFIBSmallIntField;
    TabSheet2: TTabSheet;
    Button3: TButton;
    Button4: TButton;
    procedure ComboBox1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure tAutoPostBeforePost(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure TabSheet1Enter(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fParam: TfParam;

implementation

uses dMain, fMRLetter;

{$R *.dfm}

procedure TfParam.ComboBox1Change(Sender: TObject);
begin
  tAutoPost.ReOpenWP([ComboBox1.ItemIndex]);
end;

procedure TfParam.FormShow(Sender: TObject);
begin
  ComboBox1.ItemIndex:=0;
  tAutoPost.ReOpenWP([0]);
  tPostType.ReOpenWP([]);
end;

procedure TfParam.FormHide(Sender: TObject);
begin
  tAutoPost.Close;
  tPostType.Close;
end;

procedure TfParam.tAutoPostBeforePost(DataSet: TDataSet);
begin
  tAutoPostPOSTID.Value:=ComboBox1.ItemIndex;
end;

procedure TfParam.Button1Click(Sender: TObject);
begin
  if tPostType.State in [dsinsert, dsedit]
    then tPostType.Post;
  if tAutoPost.State in [dsinsert, dsedit]
    then tAutoPost.Post;
  if tJournal.State in [dsinsert, dsedit]
    then tJournal.Post;
end;

procedure TfParam.Button2Click(Sender: TObject);
begin
  if tPostType.State in [dsinsert, dsedit]
    then tPostType.Cancel;
  if tAutoPost.State in [dsinsert, dsedit]
    then tAutoPost.Cancel;
  if tJournal.State in [dsinsert, dsedit]
    then tJournal.Cancel;
end;

procedure TfParam.Button3Click(Sender: TObject);
var
  q:TpFIBQuery;
  CaseCount, i: integer;
  s: string;
begin
  q:=TpFIBQuery.Create(Application);
  q.Database:=dmMain.dbUni;
  q.SQL.Text:='select count(id) from g1_case where stat_line is null and year_reg=2014';
  q.ExecQuery;
  CaseCount:=q.Fields[0].Value;
  q.Close;
  s:=format('� ���� ���������������� %s ��� � ���������� � ������������� ����� ������ ����������',[q.Fields[0].Value]);
  if CaseCount>0 then
  begin
    q.SQL.Text:=
      'select first 10 cs.full_number'+
      ' from g1_case cs '+
      ' where cs.stat_line is null and cs.year_reg=2014 order by cs.short_number';
    q.ExecQuery;
    while not q.Eof do
    begin
      s:=s+chr(10)+q.Fields[0].Value+';';
      q.Next;
    end;
  end;
  if CaseCount>10 then s:=s+chr(10)+'...';
  ShowMessage(s);
  q.Free;
end;

procedure TfParam.Button4Click(Sender: TObject);
var
  tC, q:TpFIBQuery;
  CaseCount, i: integer;
begin
  q:=TpFIBQuery.Create(Application);
  q.Database:=dmMain.dbUni;
  q.SQL.Text:='select count(id) from g1_case where stat_line is null';
  q.ExecQuery;
  CaseCount:=q.Fields[0].Value;
  q.Close;

  tC:=TpFIBQuery.Create(Application);
  tC.Database:=dmMain.dbUni;
  tC.SQL.Text:=
     'select cs.case_type_id, cl.category_id, cs.result_date, cl.case_id, cl.id, cl.claim_type_id,'+
     ' cl.category_id2, cl.requirements, cl.result_essence, cl.suit_money, cs.full_number'+
     ' from g1_case cs inner join g1_claim cl on (cs.id=cl.case_id)'+
     ' where cs.stat_line is null and cl.category_id is not null';
  tC.ExecQuery;
  while not tC.Eof do
  begin
    dmMain.dbUni.StartTransaction;

//    ShowMessage(tC.FldByName['full_number'].Value);
    for I := 0 to 9
      do frmMRLetter.pG1_CLAIM_BP01.Params[i].Value:=tC.Fields[i].Value;
    frmMRLetter.pG1_CLAIM_BP01.ExecQuery;

//  ������ ������ � g1_clime
    q.SQL.Text:='update g1_claim c set c.stat_line = :stat_line where c.id = :id';
    q.Prepare;
    q.Params[0].Value:=frmMRLetter.pG1_CLAIM_BP01.Fields[0].Value;
    q.Params[1].Value:=tC.FldByName['id'].Value;
    q.ExecQuery;
    q.Close;

    //  ������ ������ � g1_case
    q.SQL.Text:='update g1_case c set c.stat_line = :stat_line, c.category_id = :category_id, '+
       ' c.category_id2 = :stat_line, c.req_essence = :category_id2, c.result_essence = :result_essence,'+
       ' c.suit_money = :suit_money where c.id = :id ';
    q.Prepare;
    for I := 0 to 5 do q.Params[i].Value:=frmMRLetter.pG1_CLAIM_BP01.Fields[i].Value;
    q.Params[6].Value:=tC.FldByName['case_id'].Value;
    q.ExecQuery;
    q.Close;
    dmMain.dbUni.CommitRetaining;
    tC.Next;
  end;

  q.Free;
  tC.Free;


end;

procedure TfParam.TabSheet1Enter(Sender: TObject);
begin
  tJournal.ReOpenWP([]);
end;

end.
