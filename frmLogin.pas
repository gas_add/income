unit frmLogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrls, DB, JvDataSource, JvMemoryDataset,
  JvExControls, JvDBLookup, JvExMask, JvToolEdit, JvMaskEdit, JvDBControls;

type
  TfLogin = class(TForm)
    Label1: TLabel;
    vUser: TJvDBLookupCombo;
    fLogin: TJvMemoryData;
    dsfLogin: TJvDataSource;
    fLoginGROUPCONTENTID: TIntegerField;
    fLoginUSERPSW: TStringField;
    Label2: TLabel;
    Button1: TButton;
    vPassword: TMaskEdit;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    UserId: int64;
  end;

var
  fLogin: TfLogin;

implementation

uses dMain;

{$R *.dfm}

procedure TfLogin.Button1Click(Sender: TObject);
begin
  SetFocusedControl(Button1);
  if vPassword.Text=vUser.LookupSource.DataSet.FieldByName('USERPSW').Value
  then begin
    dmMain.UserId:=fLoginGROUPCONTENTID.Value;
    ModalResult:=mrOk;
  end
  else ModalResult:=mrCancel;
  fLogin.Close;
end;

procedure TfLogin.FormShow(Sender: TObject);
begin
  fLogin.Open;
  fLogin.Insert;
end;

end.
