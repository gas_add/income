unit fMRLetter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, JvExControls, JvWizard, JvWizardRouteMapList, JvToolEdit,
  JvDBControls, JvDBLookup, StdCtrls, JvExMask, JvMaskEdit,
  JvCheckedMaskEdit, JvDatePickerEdit, JvDBDatePickerEdit, Mask, DBCtrls,
  ExtCtrls, DB, FIBDataSet, pFIBDataSet, GridsEh,
  DBGridEh, FIBQuery, pFIBQuery, Menus, JvSpeedButton, StdActns, ActnList,
  XPStyleActnCtrls, ActnMan, ShellAPI, JvExStdCtrls, JvDBCombobox,
  JvDBLookupComboEdit, ComCtrls, JvExComCtrls, JvComCtrls, JvCombobox,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, DBAxisGridsEh,
  pFIBStoredProc;

type
  TfrmMRLetter = class(TForm)
    JvWizard1: TJvWizard;
    JvWizardInteriorPage1: TJvWizardInteriorPage;
    tLetter: TpFIBDataSet;
    dsLetter: TDataSource;
    JvWizardInteriorPage2: TJvWizardInteriorPage;
    pCaseIfo: TPanel;
    JvWizardInteriorPage3: TJvWizardInteriorPage;
    JvWizardRouteMapList1: TJvWizardRouteMapList;
    JvWizardInteriorPage5: TJvWizardInteriorPage;
    Panel5: TPanel;
    Label8: TLabel;
    Label1: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label4: TLabel;
    vDateReg: TJvDBDatePickerEdit;
    vRegNumber: TDBEdit;
    cbAutoNum: TCheckBox;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    vPostType: TJvDBLookupCombo;
    JvDBDateEdit1: TJvDBDateEdit;
    Label7: TLabel;
    vCaseType: TJvDBLookupCombo;
    Label11: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    JvDBLookupCombo6: TJvDBLookupCombo;
    Label16: TLabel;
    JvDBLookupCombo7: TJvDBLookupCombo;
    Label17: TLabel;
    JvDBLookupCombo8: TJvDBLookupCombo;
    tCase: TpFIBDataSet;
    dsCase: TDataSource;
    tCaseID: TFIBIntegerField;
    tCaseFULL_NUMBER: TFIBStringField;
    tCaseCASE_FULL_NUMBER: TFIBStringField;
    tCaseSHORT_NUMBER: TFIBIntegerField;
    tCaseYEAR_REG: TFIBIntegerField;
    tCaseM_FULL_NUMBER: TFIBStringField;
    tCaseM_SHORT_NUMBER: TFIBIntegerField;
    tCaseM_YEAR_REG: TFIBIntegerField;
    tCaseCASE_TYPE_ID: TFIBIntegerField;
    tCaseENTRY_DATE: TFIBDateTimeField;
    tCaseENTRY_TYPE_ID: TFIBIntegerField;
    tCasePREV_FULL_NUMBER: TFIBStringField;
    tCaseFOLDER_ID: TFIBIntegerField;
    tCaseO_SEND_DATE: TFIBDateTimeField;
    tCaseO_ACT_EXEC_DATE: TFIBDateTimeField;
    tCaseS_DECISION_147_DATE: TFIBDateTimeField;
    tCaseS_DECISION_152_DATE: TFIBDateTimeField;
    tCaseS_PR_HEAR_RESULT_ID: TFIBIntegerField;
    tCaseS_PR_HEAR_RESULT_ID2: TFIBIntegerField;
    tCaseS_DECISION_153_DATE: TFIBDateTimeField;
    tCaseS_CLAIM_CHANGED_DATE: TFIBDateTimeField;
    tCaseA_CASE_NUMBER_I: TFIBStringField;
    tCaseA_JUDGE_I_ID: TFIBIntegerField;
    tCaseA_RESULT_I_DATE: TFIBDateTimeField;
    tCaseA_RESULT_I_ID: TFIBIntegerField;
    tCaseA_ORDER_TYPE_ID: TFIBIntegerField;
    tCaseA_APPELANT_ID: TFIBIntegerField;
    tCaseA_WHAT_APPEAL_DATE: TFIBDateTimeField;
    tCaseA_WHAT_APPEAL_ID: TFIBIntegerField;
    tCaseA_SUBSTANTIATION_ID: TFIBIntegerField;
    tCaseA_RETURNED_DATE: TFIBDateTimeField;
    tCaseA_RET_JUDGE_ID: TFIBIntegerField;
    tCaseSUIT_MONEY: TFIBFloatField;
    tCaseDUTY: TFIBFloatField;
    tCaseREQ_ESSENCE: TFIBStringField;
    tCaseDECISION_DATE: TFIBDateTimeField;
    tCaseDECISION_ID: TFIBIntegerField;
    tCaseDECISION_ID2: TFIBIntegerField;
    tCaseRESULT_DATE: TFIBDateTimeField;
    tCaseRESULT_TYPE_ID: TFIBIntegerField;
    tCaseCOURT_STAFF_ID: TFIBIntegerField;
    tCaseREQ_DISAFF_DATE: TFIBDateTimeField;
    tCaseREQ_DISAFF: TFIBStringField;
    tCaseDISORDER_DATE: TFIBDateTimeField;
    tCaseRESULT_ID: TFIBIntegerField;
    tCaseRESULT_ID2: TFIBIntegerField;
    tCaseRESULT_ESSENCE: TFIBStringField;
    tCaseRETURN_OFFICE_DATE: TFIBDateTimeField;
    tCasePAYMENT_BY_RESULT: TFIBFloatField;
    tCasePAYMENT_DUTY: TFIBFloatField;
    tCasePAYMENT_COSTS: TFIBFloatField;
    tCasePAYMENT_PENALTY: TFIBFloatField;
    tCaseVALIDITY_DATE: TFIBDateTimeField;
    tCasePREPARE_ARCH_DATE: TFIBDateTimeField;
    tCaseARCHIVE_DATE: TFIBDateTimeField;
    tCaseVOLUME_TO_SHELVE: TFIBStringField;
    tCaseRETENTION_PERIOD: TFIBStringField;
    tCaseDESTROY_DATE: TFIBDateTimeField;
    tCaseVOLUME_ARCHIVE: TFIBStringField;
    tCaseVOLUME_DECREE: TFIBStringField;
    tCaseVOLUME_PLACE: TFIBStringField;
    tCaseCATEGORY_ID: TFIBIntegerField;
    tCaseSTAT_LINE: TFIBIntegerField;
    tCaseJUDGE_ID: TFIBIntegerField;
    tCaseCLERK: TFIBIntegerField;
    tCaseCOMMENT: TFIBStringField;
    tCaseSORT_ORDER: TFIBIntegerField;
    tCaseSYS_STAT3_IS_MATERIAL: TFIBIntegerField;
    tCaseORIGIN_DATE: TFIBDateTimeField;
    tCasePASS_WRITEXEC_DATE: TFIBDateTimeField;
    tCaseWRITEXEC_COMMENT: TFIBStringField;
    tCaseRESULT_N_DATE: TFIBDateTimeField;
    tCaseRESULT_N_ID: TFIBIntegerField;
    tCaseS_PR_HEAR_DATE: TFIBDateTimeField;
    tCaseS_HEARING_DATE: TFIBDateTimeField;
    tCaseSYS_EVENT_DATE: TFIBDateTimeField;
    tCaseSYS_EVENT_NAME_ID: TFIBIntegerField;
    tCaseSYS_EVENT_RESULT_ID: TFIBIntegerField;
    tCaseFIRST_HEARING_DATE: TFIBDateTimeField;
    tCaseCIRCUM_DEF_DATE: TFIBDateTimeField;
    tCaseCIRCUM_DEF_ID: TFIBIntegerField;
    tCaseCOMPLICATION_DATE: TFIBDateTimeField;
    tCasePERIOD_OF_VALIDITY_ID: TFIBIntegerField;
    tCaseADJUTANT_ID: TFIBIntegerField;
    tCaseCASE_ATTR: TFIBIntegerField;
    tCaseTHEFT_DAMAGE: TFIBFloatField;
    tCaseANOTHER_DAMAGE: TFIBFloatField;
    tCaseAMOUNT_DAMAGE: TFIBFloatField;
    tCaseA: TFIBIntegerField;
    tCaseRET_WRITEXEC_DATE: TFIBDateTimeField;
    tCaseEXEC_DATE: TFIBDateTimeField;
    tCaseEXEC_REASON: TFIBStringField;
    tCaseEXEC_TYPE_COMMENT: TFIBStringField;
    tCaseDUTY_WHO: TFIBStringField;
    tCasePASS_WRITEXEC_DATE2: TFIBDateTimeField;
    tCasePASS_EXEC_DATE: TFIBDateTimeField;
    tCaseEXEC_MONEY: TFIBFloatField;
    tCaseEXEC_MONEY2: TFIBFloatField;
    tCaseWEIGHT: TFIBFloatField;
    tPart: TpFIBDataSet;
    dsPart: TDataSource;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Label6: TLabel;
    DBGridEh2: TDBGridEh;
    Panel2: TPanel;
    Label3: TLabel;
    Label12: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    JvDBLookupCombo1: TJvDBLookupCombo;
    DBEdit4: TDBEdit;
    DBEdit8: TDBEdit;
    DBMemo1: TDBMemo;
    tPartID: TFIBIntegerField;
    tPartCASE_ID: TFIBIntegerField;
    tPartPARTS_TYPE_ID: TFIBIntegerField;
    tPartNAME: TFIBStringField;
    tPartBIRTH_DATE: TFIBDateTimeField;
    tPartPASSPORT: TFIBStringField;
    tPartADDRESS: TFIBStringField;
    tPartREQUIREMENTS: TFIBStringField;
    tPartCOMMENTS: TFIBStringField;
    tPartNATIVE: TFIBStringField;
    tPartPLACE_EMPLOY: TFIBStringField;
    tPartREPRESENT: TFIBStringField;
    tPartPARTS_STATUS_ID: TFIBIntegerField;
    tPartCOSTS: TFIBFloatField;
    tPartCOSTS_REASON: TFIBStringField;
    tPartCOSTS_DAYS: TFIBIntegerField;
    Label20: TLabel;
    JvDBLookupCombo3: TJvDBLookupCombo;
    Panel4: TPanel;
    Label21: TLabel;
    Label22: TLabel;
    Label27: TLabel;
    JvDBDatePickerEdit2: TJvDBDatePickerEdit;
    Label28: TLabel;
    Label29: TLabel;
    JvDBDatePickerEdit3: TJvDBDatePickerEdit;
    Label30: TLabel;
    JvDBDatePickerEdit4: TJvDBDatePickerEdit;
    Label31: TLabel;
    DBMemo3: TDBMemo;
    DBNavigator1: TDBNavigator;
    tPartParts_type: TStringField;
    pFIBQuery1: TpFIBQuery;
    pmCaseNumber: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    pmImageDoc: TPopupMenu;
    JvDBLookupCombo2: TJvDBLookupCombo;
    ActionManager1: TActionManager;
    Gr1CaseDocumentList: TAction;
    aLoadImage: TFileOpen;
    aDelImage: TAction;
    aViewImage: TAction;
    tLetterOBJECTID: TFIBIntegerField;
    tLetterLETTERDATE: TFIBDateTimeField;
    tLetterNUMBER: TFIBIntegerField;
    tLetterINCNUMBER: TFIBStringField;
    tLetterLETTERNAME: TFIBStringField;
    tLetterLETTERTYPE: TFIBIntegerField;
    tLetterPASSDATE: TFIBDateTimeField;
    tLetterINSTRUCTION: TFIBStringField;
    tLetterPLANDATE: TFIBDateTimeField;
    tLetterEXECUTIONDATE: TFIBDateTimeField;
    tLetterDESCRIPTION: TFIBStringField;
    tLetterOUTDATE: TFIBDateTimeField;
    tLetterSYSTEMUSER: TFIBIntegerField;
    tLetterPOSTTYPE: TFIBIntegerField;
    tLetterDOCTYPE: TFIBIntegerField;
    tLetterONINCOMINGLETTER: TFIBIntegerField;
    tLetterCOURTD: TFIBIntegerField;
    tLetterDOCTEXT: TFIBIntegerField;
    tLetterLETTEROUTTYPE: TFIBSmallIntField;
    tLetterADDLETTERPAGECOUNT: TFIBIntegerField;
    tLetterDOCUMENTPAGECOUNT: TFIBIntegerField;
    tLetterOTHERPAGECOUNT: TFIBIntegerField;
    tLetterCARDID: TFIBIntegerField;
    tLetterCASEID: TFIBIntegerField;
    tLetterSENDER: TFIBStringField;
    tLetterRECIPIENT: TFIBIntegerField;
    tLetterCREATOR: TFIBIntegerField;
    JvDBLookupComboEdit1: TJvDBLookupComboEdit;
    JvDBLookupComboEdit2: TJvDBLookupComboEdit;
    JvDBLookupComboEdit3: TJvDBLookupComboEdit;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    Label5: TLabel;
    JvDBLookupCombo4: TJvDBLookupCombo;
    JvWizardInteriorPage4: TJvWizardInteriorPage;
    Panel3: TPanel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    JvSpeedButton1: TJvSpeedButton;
    JvSpeedButton2: TJvSpeedButton;
    JvSpeedButton3: TJvSpeedButton;
    vDocTextTypeDoc: TJvDBLookupCombo;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    vCaseNumber: TDBEdit;
    DBGridEh1: TDBGridEh;
    DBNavigator2: TDBNavigator;
    tDocText: TpFIBDataSet;
    tDocTextOBJECTID: TFIBIntegerField;
    tDocTextATTACH: TFIBBlobField;
    tDocTextFILENAME: TFIBStringField;
    tDocTextCARDID: TFIBIntegerField;
    tDocTextDOCUMENTID: TFIBIntegerField;
    tDocTextDOCTYPE: TFIBIntegerField;
    tDocTextDOCNAME: TFIBStringField;
    tDocTextLETTERID: TFIBIntegerField;
    tDocument: TpFIBDataSet;
    tDocumentCASENUM: TFIBStringField;
    dsDocText: TDataSource;
    dsDocument: TDataSource;
    Button3: TButton;
    Button1: TButton;
    tLetterINTYPE: TFIBSmallIntField;
    Label32: TLabel;
    JvDBComboBox1: TJvDBComboBox;
    tExcistParty: TpFIBDataSet;
    DataSource1: TDataSource;
    tExcistPartyFULL_NUMBER: TFIBStringField;
    tExcistPartyENTRY_DATE: TFIBDateTimeField;
    tExcistPartyR: TFIBStringField;
    tExcistPartyJUDGE: TFIBStringField;
    Label33: TLabel;
    lJournal: TLabel;
    lCaseID: TLabel;
    bCaseId: TButton;
    eCaseId: TEdit;
    bCaseId2: TButton;
    wiComplaint: TJvWizardInteriorPage;
    tLetterCOMPLAINTID: TFIBIntegerField;
    pcComplaint: TJvPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label34: TLabel;
    JvDBLookupCombo5: TJvDBLookupCombo;
    Label35: TLabel;
    DBEdit7: TDBEdit;
    Label36: TLabel;
    JvDBLookupCombo9: TJvDBLookupCombo;
    JvDBLookupCombo10: TJvDBLookupCombo;
    Label37: TLabel;
    Label38: TLabel;
    DBEdit11: TDBEdit;
    Label39: TLabel;
    JvDBLookupCombo11: TJvDBLookupCombo;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    Label40: TLabel;
    JvDBLookupCombo12: TJvDBLookupCombo;
    Label41: TLabel;
    DBEdit12: TDBEdit;
    Label42: TLabel;
    JvDBLookupCombo13: TJvDBLookupCombo;
    Label43: TLabel;
    JvDBLookupCombo14: TJvDBLookupCombo;
    Label44: TLabel;
    DBEdit13: TDBEdit;
    Label45: TLabel;
    JvDBLookupCombo15: TJvDBLookupCombo;
    Label46: TLabel;
    DBEdit14: TDBEdit;
    Label47: TLabel;
    JvDBLookupCombo16: TJvDBLookupCombo;
    Label48: TLabel;
    eSMS_TELNUM: TDBEdit;
    Label49: TLabel;
    DBEdit16: TDBEdit;
    tPartTELEFON_ADD: TFIBStringField;
    tPartEMAIL_ADD: TFIBStringField;
    tClaim: TpFIBDataSet;
    dsClaim: TDataSource;
    tClaimID: TFIBIntegerField;
    tClaimCASE_ID: TFIBIntegerField;
    tClaimCLAIM_TYPE_ID: TFIBIntegerField;
    tClaimREQUIREMENTS: TFIBStringField;
    tClaimRESULT_DATE: TFIBDateTimeField;
    tClaimRESULT_ESSENCE: TFIBStringField;
    tClaimRESULT_ID: TFIBIntegerField;
    tClaimDUMMY01: TFIBIntegerField;
    tClaimDUMMY02: TFIBIntegerField;
    tClaimCATEGORY_ID: TFIBIntegerField;
    tClaimCATEGORY_ID2: TFIBIntegerField;
    tClaimRESULT_ID2: TFIBIntegerField;
    tClaimA_WHAT_APPEAL_DATE: TFIBDateTimeField;
    tClaimA_WHAT_APPEAL_ID: TFIBIntegerField;
    tClaimSTAT_LINE: TFIBIntegerField;
    tClaimA_APPELANT_ID: TFIBIntegerField;
    tClaimWRIT_TYPE: TFIBIntegerField;
    tClaimCOMMENT: TFIBStringField;
    tClaimA_JUDGE_I_ID: TFIBIntegerField;
    tClaimDUMMY03: TFIBIntegerField;
    tClaimDUMMY04: TFIBIntegerField;
    tClaimDUMMY05: TFIBIntegerField;
    tClaimDUMMY06: TFIBIntegerField;
    tClaimENTRY_DATE: TFIBDateTimeField;
    tClaimSYS_COMPLAINT_TYPE: TFIBIntegerField;
    tClaimSUIT_MONEY: TFIBFloatField;
    Label50: TLabel;
    JvDBLookupCombo17: TJvDBLookupCombo;
    ePassport: TDBEdit;
    Label51: TLabel;
    pG1_CLAIM_BP01: TpFIBQuery;
    tCaseCATEGORY_ID2: TFIBIntegerField;
    procedure JvWizardInteriorPage1NextButtonClick(Sender: TObject;
      var Stop: Boolean);
    procedure cbAutoNumClick(Sender: TObject);
    procedure tPartAfterInsert(DataSet: TDataSet);
    procedure tCaseAfterInsert(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure JvDBLookupComboUserDropDown(Sender: TObject);
    procedure JvWizardInteriorPage2NextButtonClick(Sender: TObject;
      var Stop: Boolean);
    procedure JvDBLookupComboUserCloseUp(Sender: TObject);
    procedure JvWizardInteriorPage3NextButtonClick(Sender: TObject;
      var Stop: Boolean);
    procedure JvWizardInteriorPage5FinishButtonClick(Sender: TObject;
      var Stop: Boolean);
    procedure JvWizard1CancelButtonClick(Sender: TObject);
    procedure JvDBLookupComboEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6Exit(Sender: TObject);
    procedure CatalogListReopen(Sender: TObject);
    procedure Catalog2Reopen(Sender: TObject);
    procedure JvWizardInteriorPage4EnterPage(Sender: TObject;
      const FromPage: TJvWizardCustomPage);
    procedure aLoadImageBeforeExecute(Sender: TObject);
    procedure aDelImageExecute(Sender: TObject);
    procedure aViewImageExecute(Sender: TObject);
    procedure aLoadImageAccept(Sender: TObject);
    procedure JvWizardInteriorPage5EnterPage(Sender: TObject;
      const FromPage: TJvWizardCustomPage);
    procedure tDocTextAfterScroll(DataSet: TDataSet);
    procedure tDocTextBeforePost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure vPostTypeChange(Sender: TObject);
    procedure bCaseId2Click(Sender: TObject);
    procedure wiComplaintEnterPage(Sender: TObject;
      const FromPage: TJvWizardCustomPage);
    procedure wiComplaintExitPage(Sender: TObject;
      const FromPage: TJvWizardCustomPage);
    procedure wiComplaintNextButtonClick(Sender: TObject;
      var Stop: Boolean);
    procedure JvWizardInteriorPage3EnterPage(Sender: TObject;
      const FromPage: TJvWizardCustomPage);
    procedure tClaimAfterInsert(DataSet: TDataSet);
    procedure dsClaimDataChange(Sender: TObject; Field: TField);
    procedure tClaimBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    EditMode,
    Step1Finish,
    Step2Finish,
    Step3Finish,
    Step4Finish,
    StepComplaintFinish,
    DocCreate: boolean;
    SubjectId,
    CaseId,
    CardId,
    LetterId,
    JournalID: int64;
    Catalog,             //0-'G1','U1','Adm','Adm1',4-'M'
    PostFormId: integer;   
    SubjectName: string;
    //function InsertEvent(aEvent:integer): boolean;

  public
    function CreateIncomingDoc(var aLetterID: Int64): boolean;
    function EditIncomingDoc(aLetter: int64): boolean;
    { Public declarations }
  end;

var
  frmMRLetter: TfrmMRLetter;

const
  CatalogName: array[0..4] of string = ('G1','U1','Adm','Adm1','M');
  ColFullCaseNum: array[0..4] of string = ('FULL_NUMBER','FULL_NUMBER','CASE_NUMBER','CASE_NUMBER','FULL_NUMBER');
  ColShortCaseNum: array[0..4] of string = ('SHORT_NUMBER','SHORT_NUMBER','ORDINAL_NUMBER','ORDINAL_NUMBER','REG_NUMBER');
  ColYear: array[0..4] of string = ('YEAR_REG','YEAR_REG','REG_YEAR','REG_YEAR','REG_YEAR');

function ifCase(const aVal: array of Variant): Variant;  


implementation

uses dMain, Math, DateUtils, frmSubjectList, RegExpr, dComplaint;

{$R *.dfm}

function ifCase(const aVal: array of Variant): Variant;
var
  k, m: integer;
  aCond: Variant;
begin
  aCond:=aVal[0];
  m:=High(aVal) div 2;
  for k:=1 to m do if aCond=aVal[k*2-1] then Result:=aVal[k*2];
end ;

function TfrmMRLetter.CreateIncomingDoc(var aLetterID: Int64): boolean;
var  s, InsertEvent: string;
begin
  EditMode:=false;

  dmMain.trMain.StartTransaction;
  tLetter.Open;
  tLetter.Insert;
  tLetterLETTERDATE.Value:=Date;
  tLetterCREATOR.Value:=dmMain.UserId;

  Result:=ShowModal=mrOk;

  if Result
  then begin

// ����������� ����� ��������� ���������
      if cbAutoNum.Checked
      then begin
        tLetter.Edit;
        if Sequentially = 1 //�������� ���������
        then begin
          dmMain.qAllAdd.SQL.Text:='SELECT MAX(l.NUMBER)+1 FROM LETTER l '+
                                   ' where (EXTRACT(YEAR FROM l.LETTERDATE)=:aYear)';
          dmMain.qAllAdd.ExecWP([YearOf(tLetterLETTERDATE.Value)]);
        end
        else begin // ��������� � �������� �������
          dmMain.qAllAdd.SQL.Text:='SELECT coalesce(MAX(l.NUMBER),0)+1 FROM LETTER l'
                                  +'  where l.posttype in (select p.objectid from posttype p where p.journalid=:journalid)'
                                  +'    and (EXTRACT(YEAR FROM l.LETTERDATE)=:aYear)';
          dmMain.qAllAdd.ExecWP([JournalID, YearOf(tLetterLETTERDATE.Value)]);
        end;
        if dmMain.qAllAdd.Fields[0].AsVariant <> null
        then tLetterNUMBER.AsVariant:=dmMain.qAllAdd.Fields[0].AsVariant
        else tLetterNUMBER.AsVariant:=1;
        dmMain.qAllAdd.Close;
        tLetter.Post;
      end ;


    aLetterId:=tLetterOBJECTID.Value;
    S:='��������������� �������� �������� � '+ dmMain.GetPostTypePrefix(tLetterPOSTTYPE.Value)+'-'+tLetterNUMBER.AsString;
    case PostFormId of
      1: begin
        tLetter.Edit;
        tLetterDESCRIPTION.Value := '�������� �'+tCaseM_FULL_NUMBER.AsString;
        tLetter.Post;
        if tCaseJUDGE_ID.AsString<>''
        then begin
          InsertEvent:= 'INSERT INTO G1_EVENT (ID, state_id, jump_condition_id, CASE_ID, EVENT_DATE, EVENT_NAME_ID, EVENT_TIME, JUDGE_ID, UNI_USER_ID)'+
                      'VALUES ( gen_id(global_generator, 1), 142, 157,'
                      + IntToStr(CaseId)+','''+ tLetterPASSDATE.AsString
                      +''', 50540201, only_time()+0.01,'+tCaseJUDGE_ID.AsString+','+ IntToStr(dmMain.UserId)+')';
          dmMain.dbUni.Execute(InsertEvent);
        end;
        s:=s+Chr(13)+Chr(10)+'�������� �'+tCaseM_FULL_NUMBER.AsString;
      end;
      2: begin
      end;
    end;

    dmMain.trMain.CommitRetaining;
    dmMain.trAdd.CommitRetaining;
    ShowMessage(s);
  end;
//  tDocument.Close;
//  tDocText.Close;
  if tPart.Active then tPart.Close;
  if tCase.Active then tCase.Close;
  if tClaim.Active then tClaim.Close;
  tLetter.Close;
//  tDocType.Close;
end;
// ����� �����
function TfrmMRLetter.EditIncomingDoc(aLetter: int64): boolean;
begin
  EditMode:=true;
  LetterId:=aLetter;
  dmMain.trMain.StartTransaction;
  tLetter.OpenWP(['ObjectId='+IntToStr(LetterId)]);
  tLetter.Edit;

  Result:=ShowModal=mrOk;

  if Result
  then begin
    dmMain.trMain.CommitRetaining;
    dmMain.trAdd.CommitRetaining;
  end;


  tDocument.Close;
  tDocText.Close;
  if tPart.Active then tPart.Close;
  if tCase.Active then tCase.Close;
  if tClaim.Active then tClaim.Close;
  tLetter.Close;
end;

procedure TfrmMRLetter.FormShow(Sender: TObject);
var R: boolean;
begin
  dmMain.tPostType.ReOpenWP([]);
  dmMain.tAutoPostEss.ReOpenWP([]);
  dmMain.tAutoPostName.ReOpenWP([]);
  dmMain.tAutoPostRes.ReOpenWP([]);
  DocCreate:=false;
  Step1Finish:=false;
  Step2Finish:=false;
  Step3Finish:=false;
  Step4Finish:=false;
  StepComplaintFinish:=false;
  //��������� ��������
  CatalogListReopen(Self);
  //���������� ��������� �����
  if EditMode
  then frmMRLetter.Caption:='�������������� ��������� ��������� �'
                         + dmMain.GetPostTypePrefix(tLetterPOSTTYPE.Value)
                         +'-'+tLetterNUMBER.AsString
  else frmMRLetter.Caption:='����������� ��������� ���������';

  //���������� ���������� JournalID, PostFormId.
  //������:
  //���������� �� ������ ������������ �������
  //���������� �������� ���������� � ����������� �� ���������� ���� ���������������
  vPostTypeChange(self);

  //��� ���� ��������� ������� ���� � �������
  //��� ������ �������������� ������� ������ ����� ����������� � ������ ������ �� �������� ������
  case PostFormId of
    1:  begin
      tCase.Open;
      tClaim.Open;
      tPart.Open;
      CaseId:=tLetterCASEID.Value;
      CatalogListReopen(Self);
    end;
    2: begin

    end;
  end;
  if (Scanner=1) and (PostFormId in [1,2])
  then begin
    tDocText.Open;
    tDocument.Open;
  end;

  SubjectId:=0;
  if not EditMode then lJournal.Caption:='';

  vPostType.ReadOnly:=EditMode;
  vRegNumber.Enabled:=EditMode;
  cbAutoNum.Enabled:=not EditMode;
  cbAutoNum.Checked:=not EditMode;

  //����� �������� ������������ ������ � ������ ��������������
  JvWizardRouteMapList1.Visible:=EditMode;
  //�������� 1-� ��������
  JvWizard1.ActivePage:=JvWizardInteriorPage1;
  // �������� ���� � ������� ���������� ������ ��� �������� ���������
  JvWizardInteriorPage2.Enabled:=PostFormId in [1];  //����
  JvWizardInteriorPage3.Enabled:=PostFormId in [1];  //������� �� ����
  //�������� ��������� ���������� ������ ���� ���� ������ � ������ ��� �������� ��������� � �����
  JvWizardInteriorPage4.Enabled:=(Scanner=1) and (PostFormId in [1,2]);
  //�������� ������ ���������� ������ ��� �����
  wiComplaint.Enabled:=PostFormId in [2];
end;

{
procedure TfrmMRLetter.RefreshFirstStep;
var posttype: integer;
begin
//  vDocType.Enabled:=false;
  if tLetterPOSTTYPE.AsVariant = NULL
  then posttype:=0
  else posttype:=tLetterPOSTTYPE.AsInteger;
//  vDocType.LookupSource.DataSet.Filtered:=false;
//  vDocType.LookupSource.DataSet.Filter:='POSTTYPE='+IntToStr(posttype);
//  vDocType.LookupSource.DataSet.Filtered:=true;
//  vDocType.Enabled:=vDocType.DropDownCount>0;
end;
}


procedure TfrmMRLetter.JvWizardInteriorPage1NextButtonClick(Sender: TObject;
  var Stop: Boolean);
var LetterYear: Integer;
  s, s2: string;
  r : TRegExpr;
begin

  //��� ����� ����������� ������� ���������� ����������
  if (SubjectId>0)and (tLetterSENDER.AsVariant<>SubjectName) then SubjectId:=0;
  //��������� ���������� ������������ ����������
  if    Stop
     or (tLetterLETTERDATE.AsVariant = Null)
     or (tLetterNUMBER.AsVariant=null and not cbAutoNum.Checked)
     or (tLetterPOSTTYPE.AsVariant = null)
     or (tLetterSENDER.AsString = '')
  then begin
    Stop:=true;
    ShowMessage('�� ��������� ������������ ��� ���������� ����');
    Exit;
  end;
  //� ������ �������� �������� ���� ������


  //�������� �� ������������ ������
  if not cbAutoNum.Checked
  then begin
    dmMain.qAllAdd.Close;
    dmMain.qAllAdd.SQL.Text:='select count(*) from LETTER where (NUMBER=:NUM)'
     +' and (POSTTYPE in (select objectid from posttype where journalid= :journalid))'
     +' and (EXTRACT(YEAR FROM LETTERDATE)=:Year)'
     +' and OBJECTID<>:id';
    LetterYear:=YearOf(tLetterLETTERDATE.Value);
    dmMain.qAllAdd.ExecWP('num;journalid;year;id',[tLetterNUMBER.Value, JournalID, LetterYear, tLetterOBJECTID.Value]);
    if dmMain.qAllAdd.Fields[0].Value > 0
    then begin
      Stop:=true;
      ShowMessage('��� ����� ������� �������� �������� ��� ���������������');
      dmMain.qAllAdd.Close;
      Exit;
    end;
    dmMain.qAllAdd.Close;
  end;

  //������������ �������� ��������
  if not EditMode and not Step1Finish
  then begin
    tLetterOBJECTID.Value:=dmMain.dbAdd.Gen_Id('LETTER_OBJECTID_GEN', 1);
    tLetter.Post;
    tLetter.Refresh;
    tLetter.Edit;
    if Scanner=1 then tDocText.ReOpenWP([]);
    frmMRLetter.Caption:= frmMRLetter.Caption
      +' �'+ dmMain.GetPostTypePrefix(tLetterPOSTTYPE.Value)
      +'-'+tLetterNUMBER.AsString;
  end
  else if (tLetter.State in [dsEdit, dsInsert])
       then begin
         tLetter.Post;
         tLetter.Refresh;
         tLetter.Edit;
       end;


  vPostType.ReadOnly:=true;

  //��������� ��������� �������� �������
  //������������ ��� ���������������
  if not EditMode and not Step1Finish then // �������� ����������� ���� ���� �����������
  case PostFormId of
    1: begin  //���� ������ ���������
        JvWizardInteriorPage2.Enabled:=true;
        JvWizardInteriorPage3.Enabled:=true;
        //���� ���� ������������ ����������, ��
        if Scanner=1 then JvWizardInteriorPage4.Enabled:=true;
        //��������� ����
        if not tCase.Active then tCase.Open;
        tCase.Insert;
        tCaseENTRY_TYPE_ID.Value:=50530000; //������� ����������� = �������
        tCaseENTRY_DATE.Value:=tLetterLETTERDATE.Value; //���� �����������
        tCaseORIGIN_DATE.Value:= tLetterLETTERDATE.Value; //���� ������ ������� �����

        //������� ������
        if not tClaim.Active then tClaim.Open;
        tClaim.Insert;


        //������� � ���������� ��� ����������� ����, � ������� ����� ���������� ����������� �� ����
        s:=tLetterSENDER.Value;
        r := TRegExpr.Create;
        try // ����������� ������������ ������� �������� ������
          r.Expression := '  +';
          r.Replace(s,' ',true);
          tLetterSENDER.Value:=s;
          r.Expression := '([�-��-�]+) +([�-�])[�-�.]* +([�-�])[�-�]*';
          if r.Exec (s)
            then s2 := r.Substitute ('$1 $2.$3.')
            else s2 := s;
        finally r.Free;
        end;

        s2 := AnsiUpperCase(s2);
        s:='';
        tExcistParty.ReOpenWP([AnsiUpperCase(tLetterSENDER.Value), s2]);
        while not tExcistParty.Eof
        do begin
          s:=s+'    '+tExcistPartyFULL_NUMBER.AsString+ ' ���� �����������: '+tExcistPartyENTRY_DATE.AsString
            +' '+tExcistPartyR.AsString+ ' �����: '+ tExcistPartyJUDGE.Value+ #10+#13;
          tExcistParty.Next;
        end;
        if s<>'' then ShowMessage(s);
    end;
    2: begin  //������ �� ����
        wiComplaint.Enabled:=true;
        //���� ���� ������������ ����������, ��
        if Scanner=1 then JvWizardInteriorPage4.Enabled:=true;
        //��������� ����
    end;
  end;
  Step1Finish:=true;
end;

procedure TfrmMRLetter.JvWizardInteriorPage2NextButtonClick(Sender: TObject;
  var Stop: Boolean);

 procedure fillCase;
   var i: integer;
 begin
    for i:=1 to dmMain.spG1_CASE_GBP.ParamCount do
      dmMain.spG1_CASE_GBP.Params[i-1].Value:=tCase.FieldByName(dmMain.spG1_CASE_GBP.ParamName(i-1)).Value;
    dmMain.spG1_CASE_GBP.ExecQuery;
    tCaseFULL_NUMBER.Value:=dmMain.spG1_CASE_GBP.FieldByName('M_CASE_NUMBER').Value;
    //tCaseSHORT_NUMBER.Value:=dmMain.spG1_CASE_GBP.FieldByName('M_ORDINAL_NUMBER').Value;
    //tCaseYEAR_REG.Value:=dmMain.spG1_CASE_GBP.FieldByName('M_REG_YEAR').Value;
    tCaseM_FULL_NUMBER.Value:=dmMain.spG1_CASE_GBP.FieldByName('M_CASE_NUMBER').Value;
    tCaseM_SHORT_NUMBER.Value:=dmMain.spG1_CASE_GBP.FieldByName('M_ORDINAL_NUMBER').Value;
    tCaseM_YEAR_REG.Value:=dmMain.spG1_CASE_GBP.FieldByName('M_REG_YEAR').Value;
    dmMain.spG1_CASE_GBP.Close;
 end;

begin
  //��������� ������������ ���������� ���������� ����
  if (tCaseCASE_TYPE_ID.AsVariant=null)
    or (tClaimREQUIREMENTS.AsVariant=null)
//    or (tCaseJUDGE_ID.AsVariant=null)
  then begin
    ShowMessage('�� ��������� ������������ ��� ���������� ����');
    Stop:=true;
  end
  else begin
      //��������� ����������
    if tCase.State in [dsInsert, dsEdit]
    then begin
      if tCase.State = dsInsert then fillCase;
      tCase.Post;
      tCase.Refresh;
    end;
    if tClaim.State in [dsInsert, dsEdit]
    then begin
      tClaim.Post;
      tClaim.Refresh;
    end;
    if not EditMode and not Step2Finish
    then  begin
      tLetterRECIPIENT.Value:=tCaseJUDGE_ID.Value;
      //��������� ������� �� ���� - �������� ����������� ��� �����
      //������������� � ���� ���������� � ����������� ����������
      tPart.Open;
      tPart.Insert;
      if tCaseCASE_TYPE_ID.AsInteger = 50520052
        then tPartPARTS_TYPE_ID.Value:= 50710100 //���������������� �����
        else tPartPARTS_TYPE_ID.Value:= 50710000; //�����

      if SubjectId > 0
      then with dmMain do begin
        qAllAdd.SQL.Text:='SELECT parts_status_id, name, native, address FROM Subject where ID=:ID';
        qAllAdd.ExecWP([SubjectId]);
        tPartPARTS_STATUS_ID.AsVariant:=qAllAdd.Fields[0].AsVariant;
        tPartNAME.AsVariant :=qAllAdd.Fields[1].AsVariant;
        tPartNATIVE.AsVariant :=qAllAdd.Fields[2].AsVariant;
        tPartADDRESS.AsVariant :=qAllAdd.Fields[3].AsVariant;
        qAllAdd.Close;
      end
      else tPartNAME.Value:=tLetterSENDER.Value;

      tPart.Post;
      Step2Finish:=True;
    end;
  end;
end;

procedure TfrmMRLetter.JvWizardInteriorPage3NextButtonClick(Sender: TObject;
  var Stop: Boolean);
begin
  if tPart.State in [dsEdit, dsInsert] then tPart.Post;
  Step3Finish:=true;
end;

{procedure TfrmMRLetter.JvWizardInteriorPage4EnterPage(Sender: TObject;
  const FromPage: TJvWizardCustomPage);
begin
  if not EditMode and not DocCreate and (tLetterDOCTYPE.AsInteger=209)
  then begin
        //��������� ����. ���������
      CardId:=14; //G1
      tDocText.Active:=true;
      tDocText.Insert;
      tDocTextLETTERID.Value:=tLetterOBJECTID.Value;
      tDocTextCARDID.Value:= CardId;
      tDocTextDOCTYPE.Value:=tLetterDOCTYPE.Value;
      tDocTextDOCNAME.Value:=vDocType.Text;
      tDocText.Post;
      DocCreate:=true;
  end;
end;
}
{
procedure TfrmMRLetter.JvWizardInteriorPage4NextButtonClick(Sender: TObject;
  var Stop: Boolean);
begin
  if tDocText.State in [dsEdit, dsInsert] then tDocText.Post;

  if not EditMode and not Step4Finish
  then begin
  //��������� ������������ ���������
    if tLetterDOCTYPE.AsInteger=209
      then tLetterLETTERNAME.AsString:='������� ���������/��������� '+tLetterSENDER.AsString+ ' '+tCaseREQ_ESSENCE.AsString;
  // ������ ���������|��������� ������ ������ [� ������ ����������] ���������
    //� ���������� ���������� � ����
    tLetterDESCRIPTION.Value:=tDocumentCASENUM.Value;

    //���� �������� ������ ����������� ������
    tLetterPASSDATE.Value:=Date;
    //���� ��������� �������, �� ����������� ���� 1 ���.
    //if tLetterPOSTTYPE.Value = 8 then  tLetterPLANDATE.Value:= tLetterLETTERDATE.Value+
    Step4Finish:=true;
  end;
end;
}
procedure TfrmMRLetter.JvWizardInteriorPage5FinishButtonClick(
  Sender: TObject; var Stop: Boolean);
var
  InsertEvent: string;
  passdate: string;
begin
// ��������:
// 1. ���� ���� "��������" ���������, �� ������ ���� ��������� � ���� "���� ��������"
// 2. ����������� ������ ������ ��������. ���� �������� �� ����� ���� ������ ���� �����������

  if (tLetterRECIPIENT.AsString<>'') and (tLetterPASSDATE.AsString='')
  then begin
    Stop:=true;
    ShowMessage('���� "��������" ���������, ������������� ���� "���� ��������" ���� ������ ���� ���������');
    exit;
  end;

  if tLetterPASSDATE.AsDateTime<tLetterLETTERDATE.AsDateTime
  then begin
    Stop:=true;
    ShowMessage('���� �������� �� ����� ���� ������ ���� �����������');
    exit;
  end;


// ���� �������������� �������� - ������� ���������, �������� ���������� ���������� ����� � ���� ����� �� ��������
// �� ������������� ���� ����� � �������� �� ���� ���� �������� � ���� �������� ��������� � ������ ��������������,
// � ������������� � ������ �����������, �������� �� ��������������, �� ������� ������ � �������� ���� � �������� ���� �����
  if (PostFormId = 1) {}   //������� ���������
     and (tCaseJUDGE_ID.AsVariant=Null) //����� �� ��������
     and (tLetterRECIPIENT.AsString<>'') //���������� ��������
  then begin
    dmMain.qAllUni.SQL.Text:='SELECT count(MAINID) FROM Grouptype WHERE CONTENTID = 1540006 AND MAINID=:MAINID';
    dmMain.qAllUni.ExecWP([tLetterRECIPIENT.AsString]);
    if dmMain.qAllUni.Fields[0].AsInteger=1 // ���������� - ��� �����
    then begin
      tCase.Edit;
      tCaseJUDGE_ID.AsVariant:=tLetterRECIPIENT.AsVariant;
      tCase.Post;
      if EditMode
      then begin
        if tLetterPASSDATE.AsString=''
          then passdate:='null'
          else passdate:=''''+tLetterPASSDATE.AsString+'''';
        InsertEvent:= 'INSERT INTO G1_EVENT (ID, state_id, jump_condition_id, CASE_ID, EVENT_DATE, EVENT_NAME_ID, EVENT_TIME, JUDGE_ID, UNI_USER_ID)'+
                      'VALUES ( gen_id(global_generator, 1), 142, 157,'
                      + IntToStr(CaseId)+', '+ passdate
                      + ', 50540201, only_time()+0.01,'+tCaseJUDGE_ID.AsString+','+ IntToStr(dmMain.UserId)+')';
        dmMain.dbUni.Execute(InsertEvent);
      end;       
    end;
    dmMain.qAllUni.Close
  end;

  if tLetter.State in [dsInsert, dsEdit] then tLetter.Post;
  if tCase.State in [dsInsert, dsEdit] then tCase.Post;
  if tClaim.State in [dsInsert, dsEdit] then tClaim.Post;
//���� ���� ������������
  if (Scanner=1) and (tDocText.State in [dsInsert, dsEdit]) then tDocText.Post;
  if tPart.State in [dsInsert, dsEdit] then tPart.Post;
  dmMain.trMain.CommitRetaining;
  dmMain.trAdd.CommitRetaining;
end;

procedure TfrmMRLetter.JvWizard1CancelButtonClick(Sender: TObject);
begin
  if tLetter.State in [dsInsert, dsEdit] then tLetter.Cancel;
  if tCase.State in [dsInsert, dsEdit] then tCase.Cancel;
  if tClaim.State in [dsInsert, dsEdit] then tClaim.Cancel;
//  if tDocText.State in [dsInsert, dsEdit] then tDocText.Cancel;
  if tPart.State in [dsInsert, dsEdit] then tPart.Cancel;
  dmMain.trMain.RollbackRetaining;
  dmMain.dbAdd.RollbackRetaining;
  dmMain.dbUni.RollbackRetaining;
end;

{
procedure TfrmMRLetter.vPostTypeChange(Sender: TObject);
begin
  tLetterDOCTYPE.AsVariant:=NULL;
  RefreshFirstStep;
end;
}

procedure TfrmMRLetter.cbAutoNumClick(Sender: TObject);
begin
  vRegNumber.Enabled:=not cbAutoNum.Checked;
  if cbAutoNum.Checked then vRegNumber.Text:='';
end;

procedure TfrmMRLetter.tPartAfterInsert(DataSet: TDataSet);
begin
  tPartID.Value:=dmMain.dbUni.Gen_Id('GLOBAL_GENERATOR', 1);
  tPartCASE_ID.Value:=tCaseID.Value;
end;

procedure TfrmMRLetter.tCaseAfterInsert(DataSet: TDataSet);
begin
  CaseId:=dmMain.dbUni.Gen_Id('GLOBAL_GENERATOR', 1);
  tCaseID.Value:=CaseId;
  tLetterCASEID.Value:=CaseId;
  tLetterCARDID.Value:=14; //����������� ���� 1 ���������
end;

procedure TfrmMRLetter.tClaimAfterInsert(DataSet: TDataSet);
begin
  tClaimID.Value:=dmMain.dbUni.Gen_Id('GLOBAL_GENERATOR', 1);
  tClaimCASE_ID.Value:=CaseId;
  tClaimCLAIM_TYPE_ID.Value:=913320001;
end;

procedure TfrmMRLetter.tClaimBeforePost(DataSet: TDataSet);
begin
  pG1_CLAIM_BP01.Params[0].AsVariant:=tCaseCASE_TYPE_ID.AsVariant;
  pG1_CLAIM_BP01.Params[1].AsVariant:=tClaimCATEGORY_ID.AsVariant;
  pG1_CLAIM_BP01.Params[2].AsVariant:=tCaseRESULT_DATE.AsVariant;
  pG1_CLAIM_BP01.Params[3].AsVariant:=tCaseID.AsVariant;
  pG1_CLAIM_BP01.Params[4].AsVariant:=tClaimID.AsVariant;
  pG1_CLAIM_BP01.Params[5].AsVariant:=tClaimCLAIM_TYPE_ID.AsVariant;
  pG1_CLAIM_BP01.Params[6].AsVariant:=tClaimCATEGORY_ID2.AsVariant;
  pG1_CLAIM_BP01.Params[7].AsVariant:=tClaimREQUIREMENTS.AsVariant;
  pG1_CLAIM_BP01.Params[8].AsVariant:=tClaimRESULT_ESSENCE.AsVariant;
  pG1_CLAIM_BP01.Params[9].AsVariant:=tClaimSUIT_MONEY.AsVariant;
  pG1_CLAIM_BP01.ExecQuery;
  tCase.Edit;
  tCaseSTAT_LINE.AsVariant:=pG1_CLAIM_BP01.Fields[0].AsVariant;
  tCaseCATEGORY_ID.AsVariant:=pG1_CLAIM_BP01.Fields[1].AsVariant;
  tCaseCATEGORY_ID2.AsVariant:=pG1_CLAIM_BP01.Fields[2].AsVariant;
  tCaseREQ_ESSENCE.AsVariant:=pG1_CLAIM_BP01.Fields[3].AsVariant;
  tCaseRESULT_ESSENCE.AsVariant:=pG1_CLAIM_BP01.Fields[4].AsVariant;
  tCaseSUIT_MONEY.AsVariant:=pG1_CLAIM_BP01.Fields[5].AsVariant;
  tCase.Post;
  tClaimSTAT_LINE.AsVariant:=pG1_CLAIM_BP01.Fields[6].AsVariant;
  pG1_CLAIM_BP01.Close;

end;

procedure TfrmMRLetter.JvDBLookupComboUserDropDown(Sender: TObject);
var
  where: string;
  groupid: string;
begin
  if (Sender as TJvDBLookupCombo).DataField = 'JUDGE_ID' then groupid:='1540006';
  if (Sender as TJvDBLookupCombo).DataField = 'CLERK' then groupid:='1540007';
  if (Sender as TJvDBLookupCombo).DataField = 'ADJUTANT_ID' then groupid:='1540011';
  where:='GROUPCONTENTID IN (SELECT MAINID FROM Grouptype WHERE CONTENTID = '+groupid+')';
  dmMain.tUser.ReOpenWP([where]);
end;
{
procedure TfrmMRLetter.tCaseBeforePost(DataSet: TDataSet);
var
  i: integer;
begin

end;
}

procedure TfrmMRLetter.JvDBLookupComboUserCloseUp(Sender: TObject);
begin
  dmMain.tUser.ReOpenWP(['1=1']);
end;


//��������� ����������� ���� ����� ����
{
procedure TfrmMRLetter.tDocTextAfterInsert(DataSet: TDataSet);
begin
//  if CaseId >0 then with dmMain do
//    tDocument.ReOpenWP([ColFullCaseNum(Cardid), GetCatalogName(Cardid), CaseId]);
end;
}
{
procedure TfrmMRLetter.tDocTextBeforePost(DataSet: TDataSet);
var
  DOCUMENTID: integer;
  InsertStatment: string;
  DOC_TYPE: string;
begin
  if (CaseId >0)  and (tDocTextDOCUMENTID.AsVariant = Null)
  then begin
    //dmMain.qAllUni.SQL.Text:='Select UniDocType From DOCTYPE where OBJECTID = '+tDocTextDOCTYPE.AsString;
    DOC_TYPE:=vDocTextTypeDoc.LookupSource.DataSet.fieldbyname('UniDocType').AsString;

    DOCUMENTID:=dmMain.dbUni.Gen_Id('GLOBAL_GENERATOR', 1);
    InsertStatment:='INSERT INTO '+dmMain.GetCatalogName(CardId)+'_Document'
      +' (DOCUMENTID, CASE_ID, CREATED_DATE, DOCUMENTNAME, DOC_TYPE, IS_DRAFT_ID) VALUES ('
      +IntToStr(DOCUMENTID)+', '+IntToStr(CaseId)+', '''+tLetterLETTERDATE.AsString+''', '''
      +tDocTextDOCNAME.AsString+''', '+DOC_TYPE+', 60000)';
    dmMain.qAllUni.SQL.Text:=InsertStatment;
    dmMain.qAllUni.ExecQuery;
    dmMain.qAllUni.Close;
    tDocTextDOCUMENTID.Value:=DOCUMENTID;
    tDocTextLETTERID.Value:=tLetterOBJECTID.Value;
    tDocTextCARDID.Value:=CardId;
  end;
end;
}

{
procedure TfrmMRLetter.tDocTextBeforeScroll(DataSet: TDataSet);
begin
//  if tDocText.State in [dsEdit, dsInsert] then tDocText.Post
end;
}

{
procedure TfrmMRLetter.tDocTextAfterScroll(DataSet: TDataSet);
begin
  //��� ��������� �������� ������ � ������ ����������� ������ ���������
  vDocTextTypeDoc.ReadOnly:= (tDocText.State=dsEdit);

  //���� �������� � ������ ��������������
  if tDocText.State=dsEdit then
  //� �������� � ����� ��� ������������
  if tDocTextDOCUMENTID.AsVariant<>Null then with dmMain do
  begin
    //��������� ��������� ����� ��������� � ������������� ����. �������
    CardId:=tDocTextCARDID.Value;
    qAllUni.SQL.Text:='Select Case_Id from '+GetCatalogName(CardId)
      +'_Document where DocumentId='+ tDocTextDOCUMENTID.AsString;
    qAllUni.ExecQuery;
    CaseId:=qAllUni.Fields[0].AsInt64;
    qAllUni.Close;
    tDocument.ReOpenWP([ColFullCaseNum(Cardid), GetCatalogName(Cardid), CaseId]);
  end
  else begin
    //����� ���������� ��� � 0
    CaseId:=0;
    CardId:=0;
    tDocument.Close;
  end;
  //��������� �������, ���� ���������� ����������� ������ ��������� � ������� ���������.
  //� ���� ������ ������������ CarId = 14 ����������� ����
  if (tDocText.State=dsInsert) and (tLetterDOCTYPE.AsInteger=209)
  then begin
      CardId:=14; //G1
  end;

end;
}

{
procedure TfrmMRLetter.aViewImageExecute(Sender: TObject);
var
  tempdir: array[0..255] of char ;
  tmp: string;
begin
  if tDocTextFILENAME.AsString<> '' then
  begin
    GetTempPath( 255, tempdir);
    tmp:= tempdir;
    tmp:= tempdir + tDocTextFILENAME.AsString;
    (tDocTextATTACH as TBlobField).SaveToFile(tmp);
    ShellExecute( Handle, '', pChar(tmp),'', nil, SW_NORMAL);
  end;
end;

procedure TfrmMRLetter.aLoadImageBeforeExecute(Sender: TObject);
begin
  if not (tDocText.State in [dsEdit, dsInsert]) then abort;
end;

procedure TfrmMRLetter.aLoadImageAccept(Sender: TObject);
var
  ImageFile: string;
begin
  ImageFile:= aLoadImage.Dialog.FileName;
//  tDocText.Edit;
  tDocTextATTACH.LoadFromFile(ImageFile);
  tDocTextFILENAME.Value:=ExtractFileName(ImageFile);
//  tDocText.Post;
end;

procedure TfrmMRLetter.aDelImageExecute(Sender: TObject);
begin
  if not (tDocText.State in [dsEdit, dsInsert]) then abort;
//  tDocText.Edit;
  tDocTextATTACH.AsVariant:=Null;
  tDocTextFILENAME.AsVariant:=Null;
//  tDocText.Post;
end;
}

procedure TfrmMRLetter.JvDBLookupComboEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
//var  b: boolean;
begin
  //JvDBLookupComboEdit2.DoClick;
end;

procedure TfrmMRLetter.DBEdit6Exit(Sender: TObject);
begin
  try
    if (StrToInt(DBEdit6.Text)>0) and (DBEdit1.Text='')
      then tCaseDUTY_WHO.Value := tLetterSENDER.Value;
  except
  end;
end;

procedure TfrmMRLetter.dsClaimDataChange(Sender: TObject; Field: TField);
begin
  if Assigned(Field) and (Field.FieldName='CATEGORY_ID')
  then begin
    Catalog2Reopen(Self);
  end;
end;

procedure TfrmMRLetter.CatalogListReopen(Sender: TObject);
begin
  dmMain.tCategoryList.ReOpenWP([tCaseCASE_TYPE_ID.Value,'01.01.2015',now]);
end;

procedure TfrmMRLetter.Catalog2Reopen(Sender: TObject);
begin
  dmMain.tCategory2.ReOpenWP([tClaimCATEGORY_ID.Value]);
end;

procedure TfrmMRLetter.JvWizardInteriorPage4EnterPage(Sender: TObject;
  const FromPage: TJvWizardCustomPage);
begin
  if not EditMode and not DocCreate and (tLetterPOSTTYPE.AsInteger=1)
  then begin
        //��������� ����. ���������
      CardId:=14; //G1
      tDocText.Active:=true;
      tDocText.Insert;
      tDocTextLETTERID.Value:=tLetterOBJECTID.Value;
      tDocTextCARDID.Value:= CardId;
      tDocTextDOCTYPE.Value:=209;
      tDocTextDOCNAME.Value:='������� ���������';
      aLoadImage.Execute;
      tDocText.Post;
      DocCreate:=true;
  end;
end;

procedure TfrmMRLetter.aLoadImageBeforeExecute(Sender: TObject);
begin
  if not (tDocText.State in [dsEdit, dsInsert]) then abort;
end;

procedure TfrmMRLetter.aDelImageExecute(Sender: TObject);
begin
  if not (tDocText.State in [dsEdit, dsInsert]) then abort;
//  tDocText.Edit;
  tDocTextATTACH.AsVariant:=Null;
  tDocTextFILENAME.AsVariant:=Null;
//  tDocText.Post;
end;

procedure TfrmMRLetter.aViewImageExecute(Sender: TObject);
var
  tempdir: array[0..255] of char ;
  tmp: string;
begin
  if tDocTextFILENAME.AsString<> '' then
  begin
    GetTempPath( 255, tempdir);
    tmp:= tempdir;
    tmp:= tempdir + tDocTextFILENAME.AsString;
    (tDocTextATTACH as TBlobField).SaveToFile(tmp);
    ShellExecute( Handle, '', pChar(tmp),'', nil, SW_NORMAL);
  end;
end;

procedure TfrmMRLetter.aLoadImageAccept(Sender: TObject);
var
  ImageFile: string;
begin
  ImageFile:= aLoadImage.Dialog.FileName;
//  tDocText.Edit;
  tDocTextATTACH.LoadFromFile(ImageFile);
  tDocTextFILENAME.Value:=ExtractFileName(ImageFile);
//  tDocText.Post;
end;

procedure TfrmMRLetter.JvWizardInteriorPage5EnterPage(Sender: TObject;
  const FromPage: TJvWizardCustomPage);
var
  istec, otvetchik: string;
begin
  if not EditMode and not Step4Finish
  then begin
  //��������� ������������ ���������
    case PostFormId of
         // ������ ���������|��������� ������ ������ [� ������ ����������] ���������
      1: begin
        istec:='';
        otvetchik:='';
        tPart.First;
        while not tPart.Eof
        do begin
          if (tPartPARTS_TYPE_ID.AsInteger=50710000) or (tPartPARTS_TYPE_ID.AsInteger=50710010)
          then if istec='' then istec:=tPartNAME.Value
                           else istec:=', '+tPartNAME.Value;
          if (tPartPARTS_TYPE_ID.Value=50710001) or (tPartPARTS_TYPE_ID.Value=50710011)
          then if otvetchik='' then otvetchik:=' � '+tPartNAME.Value
                               else otvetchik:=', '+tPartNAME.Value;
          tPart.Next;
        end;
        tLetterLETTERNAME.AsString:='������� ��������� '+istec+ otvetchik+' '+tClaimREQUIREMENTS.AsString;
      end;

      2: tLetterLETTERNAME.AsString:='������ �� ����';
    end;
    //���� �������� ������ ����������� ������
    tLetterPASSDATE.Value:=Date;
    Step4Finish:=true;
  end;
end;

procedure TfrmMRLetter.tDocTextAfterScroll(DataSet: TDataSet);
begin
  //��� ��������� �������� ������ � ������ ����������� ������ ���������
  vDocTextTypeDoc.ReadOnly:= (tDocText.State=dsEdit);

  //���� �������� � ������ ��������������
  if tDocText.State=dsEdit then
  //� �������� � ����� ��� ������������
  if tDocTextDOCUMENTID.AsVariant<>Null then with dmMain do
  begin
    //��������� ��������� ����� ��������� � ������������� ����. �������
    CardId:=tDocTextCARDID.Value;
    qAllUni.SQL.Text:='Select Case_Id from '+GetCatalogName(CardId)
      +'_Document where DocumentId='+ tDocTextDOCUMENTID.AsString;
    qAllUni.ExecQuery;
    CaseId:=qAllUni.Fields[0].AsInt64;
    qAllUni.Close;
    tDocument.ReOpenWP([ColFullCaseNum(Cardid), GetCatalogName(Cardid), CaseId]);
  end
  else begin
    //����� ���������� ��� � 0
    CaseId:=0;
    CardId:=0;
    tDocument.Close;
  end;
  //��������� �������, ���� ���������� ����������� ������ ��������� � ������� ���������.
  //� ���� ������ ������������ CarId = 14 ����������� ����
  if (tDocText.State=dsInsert) and (tLetterPOSTTYPE.AsInteger=1)
  then begin
      CardId:=14; //G1
  end;
end;

procedure TfrmMRLetter.tDocTextBeforePost(DataSet: TDataSet);
var
  DOCUMENTID: integer;
  InsertStatment: string;
  DOC_TYPE: string;
begin
  if (CaseId >0)  and (tDocTextDOCUMENTID.AsVariant = Null)
  then begin
    //dmMain.qAllUni.SQL.Text:='Select UniDocType From DOCTYPE where OBJECTID = '+tDocTextDOCTYPE.AsString;
    DOC_TYPE:=vDocTextTypeDoc.LookupSource.DataSet.fieldbyname('UniDocType').AsString;

    DOCUMENTID:=dmMain.dbUni.Gen_Id('GLOBAL_GENERATOR', 1);
    InsertStatment:='INSERT INTO '+dmMain.GetCatalogName(CardId)+'_Document'
      +' (DOCUMENTID, CASE_ID, CREATED_DATE, DOCUMENTNAME, DOC_TYPE, IS_DRAFT_ID) VALUES ('
      +IntToStr(DOCUMENTID)+', '+IntToStr(CaseId)+', '''+tLetterLETTERDATE.AsString+''', '''
      +tDocTextDOCNAME.AsString+''', '+DOC_TYPE+', 60000)';
    dmMain.qAllUni.SQL.Text:=InsertStatment;
    dmMain.qAllUni.ExecQuery;
    dmMain.qAllUni.Close;
    tDocTextDOCUMENTID.Value:=DOCUMENTID;
    tDocTextLETTERID.Value:=tLetterOBJECTID.Value;
    tDocTextCARDID.Value:=CardId;
  end;
end;

procedure TfrmMRLetter.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  JvWizard1CancelButtonClick(Self);
end;

procedure TfrmMRLetter.Button3Click(Sender: TObject);
begin
  SubjectId:=frmSubjectList.GetSubject;
  if SubjectId > 0
  then with dmMain do begin
    qAllAdd.SQL.Text:='SELECT name FROM Subject where ID=:ID';
    qAllAdd.ExecWP([SubjectId]);
    SubjectName:=qAllAdd.Fields[0].AsVariant;
    tLetterSENDER.AsVariant:=qAllAdd.Fields[0].AsVariant;
    qAllAdd.Close;
  end;
end;

procedure TfrmMRLetter.Button1Click(Sender: TObject);
var SubjectId: integer;
begin
      SubjectId:=frmSubjectList.GetSubject;
      if SubjectId > 0
      then with dmMain do begin
        tPart.Append;
        qAllAdd.SQL.Text:='SELECT parts_status_id, name, native, address FROM Subject where ID=:ID';
        qAllAdd.ExecWP([SubjectId]);
        tPartPARTS_STATUS_ID.AsVariant:=qAllAdd.Fields[0].AsVariant;
        tPartNAME.AsVariant :=qAllAdd.Fields[1].AsVariant;
        tPartNATIVE.AsVariant :=qAllAdd.Fields[2].AsVariant;
        tPartADDRESS.AsVariant :=qAllAdd.Fields[3].AsVariant;
        qAllAdd.Close;
        tPart.Post;
      end;
end;

procedure TfrmMRLetter.vPostTypeChange(Sender: TObject);
var
  addtocase: integer;
  viewcase: boolean;
begin
  with dmMain do begin
    if tLetterPOSTTYPE.Value>0 then
    begin
      //���������� �� ������ ������������ �������
      qAllAdd.SQL.Text:='select j.objectid, j.posttypename, p.postformid  from posttype p'
                       +' inner join journal j on (p.journalid=j.objectid)'
                       +' where p.objectid=:v1';
      qAllAdd.ExecWP([tLetterPOSTTYPE.Value]);
      lJournal.Caption:=qAllAdd.Fields[1].AsString;
      JournalID:=qAllAdd.Fields[0].AsInteger;
      PostFormId:=qAllAdd.Fields[2].AsInteger;
      qAllAdd.Close;

      //���������� �������� ���������� � ����������� �� ���������� ���� ���������������
      qAllAdd.SQL.Text:='select addtocase from posttype where objectid=:v1';
      qAllAdd.ExecWP([tLetterPOSTTYPE.Value]);
      addtocase:=qAllAdd.Fields[0].AsInteger;
      qAllAdd.Close;
    end
    else begin
      addtocase:=0;
      lJournal.Caption:='';
      JournalID:=0;
      PostFormId:=0;
    end;

    if (addtocase=0) and lCaseID.Visible then
    begin
      viewcase:=false;
      lCaseID.Visible:=viewcase;
      eCaseId.Visible:=viewcase;
      bCaseId.Visible:=viewcase;
      bCaseId2.Visible:=viewcase;
      if not EditMode
      then begin
        tLetterCARDID.AsVariant:=Null;
        tLetterCASEID.AsVariant:=Null;
      end;
    end;
    if ((addtocase>0) and not(lCaseID.Visible)) then
    begin
      viewcase:=true;
      lCaseID.Visible:=viewcase;
      eCaseId.Visible:=viewcase;
      bCaseId.Visible:=viewcase;
      bCaseId2.Visible:=viewcase;
    end;

  end;
end;

procedure TfrmMRLetter.bCaseId2Click(Sender: TObject);
var
  card, num, year: integer;
  r: TRegExpr;
  s: String;
  v1,v2,v3: string;
begin
  r := TRegExpr.Create;
  try // ����������� ������������ ������� �������� ������
     s:=eCaseId.Text;
     r.Expression := '([0-9]{1,2})(/[0-9]{1,2})?-([0-9]{1,4})/([0-9]{4})';
     if r.Exec (s)
     then begin
        card:=StrToInt(r.Substitute ('$1'));
        num :=StrToInt(r.Substitute ('$3'));
        year:=StrToInt(r.Substitute ('$4'));
        case card of
          2,11: cardid:=14;
          1,10: cardid:=15;
          5:    cardid:=12;
          12:   cardid:=24;
          3,4:  cardid:=20;
        end;
        Catalog:=ifcase([cardid,14,0,15,1,12,2,24,3,20,4]);
        v1:= CatalogName[Catalog];
        v2:= ColShortCaseNum[Catalog]+'='+IntToStr(num);
        v3:= ColYear[Catalog]+'='+IntToStr(year);

        dmMain.qAllUni.SQL.Text:='select ID from @@CATALOG@_CASE'
          +' where @@ColShortCaseNum@ and @@ColYear@';
        dmMain.qAllUni.ExecWP([v1,v2,v3]);
        if dmMain.qAllUni.Eof
        then begin
          ShowMessage('���� �� �������');
        end
        else begin
          CaseId:=dmMain.qAllUni.Fields[0].AsInteger;
          tLetterCARDID.AsVariant:=CardId;
          tLetterCASEID.AsVariant:=CaseId;
        end;
        dmMain.qAllUni.Close;
    end
    else ShowMessage('���� �� ����������');
  finally r.Free;
  end;
end;

procedure TfrmMRLetter.wiComplaintEnterPage(Sender: TObject;
  const FromPage: TJvWizardCustomPage);
begin
  //� ����������� �� �������� ���������� ��������������� ������ � ���������� ����������
  pcComplaint.TabIndex:= Catalog;
  dmMain.PARTS_TYPEReopen(Catalog);
  dmComplaint.ComplKindReopen(Catalog);
  dmComplaint.tComplType.ReOpenWP([]);
  //��� ������� ������ ������� ����������� ������ � ������ � ������� ���������������� ��������
  if not StepComplaintFinish then tLetterCOMPLAINTID.Value:=dmComplaint.NewComplaint(Catalog);
  //��� ��������� ������ �������� ����������� ��� �������������� ���� � ��������� �������
  if StepComplaintFinish or EditMode then dmComplaint.OpenTable(Catalog);
end;

procedure TfrmMRLetter.wiComplaintExitPage(Sender: TObject;
  const FromPage: TJvWizardCustomPage);
begin
  //��������� ���������� ��������� ��������� � ���� ������
  dmComplaint.CloseTable(Catalog);
  StepComplaintFinish:=true;
end;

procedure TfrmMRLetter.wiComplaintNextButtonClick(Sender: TObject;
  var Stop: Boolean);
begin
  //��������� ��� �� ������������ ��������� ���������
  with dmComplaint do
  Stop:=((Catalog=0) and (tG1ComplDOCUMENT_TYPE_ID.AsInteger<=0)  and (tG1ComplPARTY_NAME.AsString<>''))
     or ((Catalog=1) and (tU1ComplDOCUMENT_TYPE_ID.AsInteger<=0)  and (tU1ComplPARTY_NAME.AsString<>''))
     or ((Catalog=2) and (tAdmComplDOCUMENT_TYPE_ID.AsInteger<=0)
                     and (tAdmComplDOCUMENT_KIND_ID.AsInteger<=0) and (tAdmComplPARTY_NAME.AsString<>''))
     or ((Catalog=3) and (tAdmComplDOCUMENT_KIND_ID.AsInteger<=0) and (tAdm1ComplDECLARANT_NAME.AsString<>''))
     or ((Catalog=4) and (tMComplDECLARANT_NAME.AsString<>''));
  if Stop then ShowMessage('�� ��������� ������������ ��� ���������� ���������');
end;

procedure TfrmMRLetter.JvWizardInteriorPage3EnterPage(Sender: TObject;
  const FromPage: TJvWizardCustomPage);
begin
  Catalog:=0;
  dmMain.PARTS_TYPEReopen(Catalog);
end;

end.


