object fParam: TfParam
  Left = 171
  Top = 135
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
  ClientHeight = 319
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 280
    Width = 635
    Height = 39
    Align = alBottom
    TabOrder = 0
    object Button1: TButton
      Left = 432
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 531
      Top = 8
      Width = 75
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 635
    Height = 280
    ActivePage = tsParams
    Align = alClient
    TabOrder = 1
    object tsParams: TTabSheet
      Caption = #1055#1086#1076#1089#1090#1072#1085#1086#1074#1082#1080
      object ComboBox1: TComboBox
        Left = 4
        Top = 6
        Width = 617
        Height = 21
        ItemIndex = 0
        TabOrder = 0
        Text = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        OnSelect = ComboBox1Change
        Items.Strings = (
          #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          #1056#1077#1079#1086#1083#1102#1094#1080#1103
          #1057#1091#1097#1085#1086#1089#1090#1100)
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 32
        Width = 625
        Height = 217
        DataSource = dsAutoPost
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'POSTVALUE'
            Title.Caption = #1047#1085#1072#1095#1077#1085#1080#1103' '#1072#1074#1090#1086#1087#1086#1076#1089#1090#1072#1085#1086#1074#1086#1082
            Visible = True
          end>
      end
    end
    object tsJournals: TTabSheet
      Caption = #1046#1091#1088#1085#1072#1083#1099
      ImageIndex = 1
      object DBGrid2: TDBGrid
        Left = 0
        Top = 32
        Width = 625
        Height = 217
        DataSource = dsJournal
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'OBJECTID'
            Width = 33
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODE'
            Title.Caption = #1055#1088#1077#1092#1080#1082#1089
            Width = 53
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'POSTTYPENAME'
            Width = 416
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ARCHIVED'
            Visible = True
          end>
      end
    end
    object TabSheet1: TTabSheet
      Caption = #1042#1080#1076' '#1082#1086#1088#1088#1077#1089#1087#1086#1085#1076#1077#1085#1094#1080#1080
      ImageIndex = 2
      OnEnter = TabSheet1Enter
      object DBGrid3: TDBGrid
        Left = 0
        Top = 32
        Width = 625
        Height = 217
        DataSource = dsPostType
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'OBJECTID'
            Title.Caption = #1048#1044
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'POSTTYPENAME'
            Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
            Width = 241
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Journal'
            Title.Caption = #1046#1091#1088#1085#1072#1083
            Width = 151
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'POSTFORMID'
            Title.Caption = #1060#1086#1088#1084#1072
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ADDTOCASE'
            Title.Caption = #1042' '#1076#1077#1083#1086
            Width = 39
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ARCHIVED'
            Title.Caption = #1040#1088#1093#1080#1074
            Width = 36
            Visible = True
          end>
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1059#1090#1080#1083#1080#1090#1099
      ImageIndex = 3
      object Button3: TButton
        Left = 3
        Top = 3
        Width = 366
        Height = 25
        Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1076#1077#1083'/'#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074' '#1089' '#1085#1077#1079#1072#1087#1086#1083#1085#1077#1085#1086#1081' '#1089#1090#1088#1086#1082#1086#1081' '#1057#1090#1072#1090#1086#1090#1095#1077#1090#1072
        Default = True
        ModalResult = 1
        TabOrder = 0
        OnClick = Button3Click
      end
      object Button4: TButton
        Left = 392
        Top = 3
        Width = 232
        Height = 25
        Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1087#1086#1083#1077' "'#1057#1090#1088#1086#1082#1072' '#1057#1090#1072#1090#1086#1090#1095#1077#1090#1072'"'
        Default = True
        ModalResult = 1
        TabOrder = 1
        OnClick = Button4Click
      end
    end
  end
  object tAutoPost: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE AUTOPOST'
      'SET '
      '    POSTID = :POSTID,'
      '    POSTVALUE = :POSTVALUE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    AUTOPOST'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO AUTOPOST('
      '    ID,'
      '    POSTID,'
      '    POSTVALUE'
      ')'
      'VALUES('
      '    :ID,'
      '    :POSTID,'
      '    :POSTVALUE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    POSTID,'
      '    POSTVALUE'
      'FROM'
      '    AUTOPOST '
      'where(  POSTID=:POSTID'
      '     ) and (     AUTOPOST.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    POSTID,'
      '    POSTVALUE'
      'FROM'
      '    AUTOPOST '
      'where POSTID=:POSTID')
    AutoUpdateOptions.UpdateTableName = 'AUTOPOST'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'AUTOPOST_ID_GEN'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforePost = tAutoPostBeforePost
    Transaction = dmMain.trAdd
    Database = dmMain.dbAdd
    AutoCommit = True
    Left = 32
    Top = 144
    object tAutoPostID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tAutoPostPOSTID: TFIBIntegerField
      FieldName = 'POSTID'
    end
    object tAutoPostPOSTVALUE: TFIBStringField
      FieldName = 'POSTVALUE'
      Size = 256
      EmptyStrToNull = True
    end
  end
  object dsAutoPost: TDataSource
    DataSet = tAutoPost
    Left = 32
    Top = 184
  end
  object tPostType: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE POSTTYPE'
      'SET '
      '    POSTTYPENAME = :POSTTYPENAME,'
      '    JOURNALID = :JOURNALID,'
      '    ARCHIVED = :ARCHIVED,'
      '    POSTFORMID = :POSTFORMID,'
      '    ADDTOCASE = :ADDTOCASE'
      'WHERE'
      '    OBJECTID = :OLD_OBJECTID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    POSTTYPE'
      'WHERE'
      '        OBJECTID = :OLD_OBJECTID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO POSTTYPE('
      '    OBJECTID,'
      '    POSTTYPENAME,'
      '    JOURNALID,'
      '    ARCHIVED,'
      '    POSTFORMID,'
      '    ADDTOCASE'
      ')'
      'VALUES('
      '    :OBJECTID,'
      '    :POSTTYPENAME,'
      '    :JOURNALID,'
      '    :ARCHIVED,'
      '    :POSTFORMID,'
      '    :ADDTOCASE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    OBJECTID,'
      '    POSTTYPENAME,'
      '    JOURNALID,'
      '    ARCHIVED,'
      '    POSTFORMID,'
      '    ADDTOCASE'
      'FROM'
      '    POSTTYPE '
      ''
      ' WHERE '
      '        POSTTYPE.OBJECTID = :OLD_OBJECTID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    OBJECTID,'
      '    POSTTYPENAME,'
      '    SHORTNAME,'
      '    ARCHIVED,'
      '    JOURNALID,'
      '    POSTFORMID,'
      '    ADDTOCASE'
      'FROM'
      '    POSTTYPE ')
    Transaction = dmMain.trAdd
    Database = dmMain.dbAdd
    AutoCommit = True
    Left = 128
    Top = 144
    object tPostTypeOBJECTID: TFIBIntegerField
      FieldName = 'OBJECTID'
    end
    object tPostTypePOSTTYPENAME: TFIBStringField
      FieldName = 'POSTTYPENAME'
      Size = 1024
      EmptyStrToNull = True
    end
    object tPostTypeJOURNALID: TFIBSmallIntField
      FieldName = 'JOURNALID'
    end
    object tPostTypeARCHIVED: TFIBSmallIntField
      FieldName = 'ARCHIVED'
    end
    object tPostTypeJournal: TStringField
      FieldKind = fkLookup
      FieldName = 'Journal'
      LookupDataSet = tJournal
      LookupKeyFields = 'OBJECTID'
      LookupResultField = 'POSTTYPENAME'
      KeyFields = 'JOURNALID'
      Size = 100
      Lookup = True
    end
    object tPostTypePOSTFORMID: TFIBSmallIntField
      FieldName = 'POSTFORMID'
    end
    object tPostTypeADDTOCASE: TFIBSmallIntField
      FieldName = 'ADDTOCASE'
    end
  end
  object dsPostType: TDataSource
    DataSet = tPostType
    Left = 128
    Top = 184
  end
  object tJournal: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JOURNAL'
      'SET '
      '    CODE = :CODE,'
      '    POSTTYPENAME = :POSTTYPENAME,'
      '    ARCHIVED = :ARCHIVED,'
      '    JOURNALTYPE = :JOURNALTYPE'
      'WHERE'
      '    OBJECTID = :OLD_OBJECTID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JOURNAL'
      'WHERE'
      '        OBJECTID = :OLD_OBJECTID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JOURNAL('
      '    OBJECTID,'
      '    CODE,'
      '    POSTTYPENAME,'
      '    ARCHIVED,'
      '    JOURNALTYPE'
      ')'
      'VALUES('
      '    :OBJECTID,'
      '    :CODE,'
      '    :POSTTYPENAME,'
      '    :ARCHIVED,'
      '    :JOURNALTYPE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    OBJECTID,'
      '    CODE,'
      '    POSTTYPENAME,'
      '    ARCHIVED,'
      '    JOURNALTYPE'
      'FROM'
      '    JOURNAL '
      ''
      ' WHERE '
      '        JOURNAL.OBJECTID = :OLD_OBJECTID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    OBJECTID,'
      '    CODE,'
      '    POSTTYPENAME,'
      '    ARCHIVED,'
      '    JOURNALTYPE'
      'FROM'
      '    JOURNAL ')
    Transaction = dmMain.trAdd
    Database = dmMain.dbAdd
    AutoCommit = True
    Left = 80
    Top = 144
    object tJournalOBJECTID: TFIBIntegerField
      DisplayLabel = #1048#1044
      FieldName = 'OBJECTID'
    end
    object tJournalCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 32
      EmptyStrToNull = True
    end
    object tJournalPOSTTYPENAME: TFIBStringField
      DisplayLabel = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
      FieldName = 'POSTTYPENAME'
      Size = 1024
      EmptyStrToNull = True
    end
    object tJournalJOURNALTYPE: TFIBSmallIntField
      FieldName = 'JOURNALTYPE'
    end
    object tJournalARCHIVED2: TFIBSmallIntField
      DisplayLabel = #1040#1088#1093#1080#1074#1085#1099#1081
      FieldName = 'ARCHIVED'
    end
  end
  object dsJournal: TDataSource
    DataSet = tJournal
    Left = 80
    Top = 184
  end
end
