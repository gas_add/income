object fPrintLetterList: TfPrintLetterList
  Left = 564
  Top = 513
  Width = 379
  Height = 154
  Caption = #1046#1091#1088#1085#1072#1083' '#1074#1093#1086#1076#1103#1097#1077#1081' '#1082#1086#1088#1088#1077#1089#1087#1086#1085#1076#1077#1085#1094#1080#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 9
    Top = 42
    Width = 106
    Height = 13
    Caption = #1044#1072#1090#1072' '#1087#1077#1088#1077#1076#1072#1095#1080' '#1076#1077#1083#1072
  end
  object dFrom: TDateTimePicker
    Left = 128
    Top = 40
    Width = 97
    Height = 21
    Date = 39134.654905787050000000
    Time = 39134.654905787050000000
    ShowCheckbox = True
    TabOrder = 0
  end
  object dTo: TDateTimePicker
    Left = 264
    Top = 40
    Width = 99
    Height = 21
    Date = 39134.654946099540000000
    Time = 39134.654946099540000000
    ShowCheckbox = True
    TabOrder = 1
  end
  object Button1: TButton
    Left = 264
    Top = 67
    Width = 99
    Height = 25
    Caption = #1057#1075#1077#1085#1077#1088#1080#1088#1086#1074#1072#1090#1100
    TabOrder = 2
    OnClick = Button1Click
  end
  object rtf: TEkRTF
    InFile = 'LetterIn.rtf'
    OutFile = 'LetterOut.rtf'
    TrueValue = 'True'
    FalseValue = 'False'
    Charset = DEFAULT_CHARSET
    Lang = 0
    Options = [eoGraphicsBinary, eoDotAsColon]
    DisableControls = True
    Left = 264
    Top = 8
    VarDataTypes = {00000000}
  end
  object q: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '  pt.CODE||'#39'-'#39'||'
      '  l.NUMBER as num,'
      '  l.LETTERDATE,'
      '  l.LETTERNAME,'
      '  l.SENDER,'
      '  l.RECIPIENT,'
      '  l.PASSDATE,'
      '  l.DESCRIPTION'
      'FROM'
      '  LETTER l'
      '  INNER JOIN POSTTYPE pt ON l.POSTTYPE = pt.OBJECTID'
      'where l.PASSDATE between :dFrom and :dTo'
      'order by l.RECIPIENT')
    Transaction = dmMain.trAdd
    Database = dmMain.dbAdd
    Left = 296
    Top = 8
    object qNUM: TFIBStringField
      FieldName = 'NUM'
      Size = 44
      EmptyStrToNull = True
    end
    object qLETTERDATE: TFIBDateTimeField
      FieldName = 'LETTERDATE'
    end
    object qLETTERNAME: TFIBStringField
      FieldName = 'LETTERNAME'
      Size = 300
      EmptyStrToNull = True
    end
    object qSENDER: TFIBStringField
      FieldName = 'SENDER'
      Size = 256
      EmptyStrToNull = True
    end
    object qRECIPIENT: TFIBIntegerField
      FieldName = 'RECIPIENT'
    end
    object qPASSDATE: TFIBDateTimeField
      FieldName = 'PASSDATE'
    end
    object qDESCRIPTION: TFIBStringField
      FieldName = 'DESCRIPTION'
      Size = 1024
      EmptyStrToNull = True
    end
    object qRecipientName: TStringField
      FieldKind = fkLookup
      FieldName = 'RecipientName'
      LookupDataSet = dmMain.tUser
      LookupKeyFields = 'GROUPCONTENTID'
      LookupResultField = 'USERNAME'
      KeyFields = 'RECIPIENT'
      Size = 100
      Lookup = True
    end
  end
end
