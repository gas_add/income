object dmMain: TdmMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 642
  Width = 722
  object dbUni: TpFIBDatabase
    DBName = '192.168.37.1:C:\DATALK\UNI_WORK2003.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'password=m'
      'sql_role_name=')
    DefaultTransaction = trMain
    SQLDialect = 3
    Timeout = 0
    LibraryName = 'fbclient.dll'
    WaitForRestoreConnect = 0
    Left = 44
    Top = 32
  end
  object dbAdd: TpFIBDatabase
    DBName = '192.168.37.1:C:\DATA\Justice\UNI_ADD.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'password=m')
    DefaultTransaction = trAdd
    SQLDialect = 3
    Timeout = 0
    LibraryName = 'fbclient.dll'
    WaitForRestoreConnect = 0
    Left = 352
    Top = 32
  end
  object trMain: TpFIBTransaction
    DefaultDatabase = dbUni
    TimeoutAction = TARollback
    Left = 48
    Top = 80
  end
  object tPostType: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    OBJECTID,'
      '    POSTTYPENAME,'
      '    ARCHIVED,'
      '    JOURNALID,'
      '    POSTFORMID,'
      '    ADDTOCASE'
      'FROM'
      '    POSTTYPE '
      'where archived = 0  '
      'order by POSTTYPENAME')
    Transaction = trAdd
    Database = dbAdd
    Left = 360
    Top = 144
    oFetchAll = True
    object tPostTypeOBJECTID: TFIBIntegerField
      FieldName = 'OBJECTID'
    end
    object tPostTypePOSTTYPENAME: TFIBStringField
      FieldName = 'POSTTYPENAME'
      Size = 1024
      EmptyStrToNull = True
    end
    object tPostTypeARCHIVED: TFIBSmallIntField
      FieldName = 'ARCHIVED'
    end
    object tPostTypeJOURNALID: TFIBSmallIntField
      FieldName = 'JOURNALID'
    end
    object tPostTypePOSTFORMID: TFIBSmallIntField
      FieldName = 'POSTFORMID'
    end
    object tPostTypeADDTOCASE: TFIBSmallIntField
      FieldName = 'ADDTOCASE'
    end
  end
  object dsPostType: TDataSource
    DataSet = tPostType
    Left = 360
    Top = 200
  end
  object tG1CaseType: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    CONTENTID,'
      '    PREFIX,'
      '    NAME'
      'FROM'
      '    CATALOGCONTENT '
      'where '
      '  CONTENTID in (50520001,50520002,50520003, 50520052)')
    Transaction = trMain
    Database = dbUni
    Left = 56
    Top = 152
    object tG1CaseTypeCONTENTID: TFIBIntegerField
      FieldName = 'CONTENTID'
    end
    object tG1CaseTypePREFIX: TFIBStringField
      FieldName = 'PREFIX'
      EmptyStrToNull = True
    end
    object tG1CaseTypeNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 250
      EmptyStrToNull = True
    end
  end
  object dsG1CaseType: TDataSource
    DataSet = tG1CaseType
    Left = 56
    Top = 208
  end
  object tUser: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    GROUPCONTENTID,'
      '    USERNAME,'
      '    USERPSW,'
      '    VA_CODE'
      'FROM'
      '    GROUPCONTENT '
      'where @@Where%1=1@'
      'order by USERNAME'
      '')
    Transaction = trMain
    Database = dbUni
    Left = 128
    Top = 152
    object tUserGROUPCONTENTID: TFIBIntegerField
      FieldName = 'GROUPCONTENTID'
    end
    object tUserUSERNAME: TFIBStringField
      FieldName = 'USERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object tUserUSERPSW: TFIBStringField
      FieldName = 'USERPSW'
      Size = 10
      EmptyStrToNull = True
    end
    object tUserVA_CODE: TFIBStringField
      FieldName = 'VA_CODE'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object dsUser: TDataSource
    DataSet = tUser
    Left = 128
    Top = 208
  end
  object spG1_CASE_GBP: TpFIBStoredProc
    Transaction = trMain
    Database = dbUni
    SQL.Strings = (
      'SELECT'
      '    ORDINAL_NUMBER,'
      '    REG_YEAR,'
      '    CASE_NUMBER,'
      '    M_ORDINAL_NUMBER,'
      '    M_REG_YEAR,'
      '    M_CASE_NUMBER'
      'FROM'
      '    G1_CASE_GBP(:CASE_TYPE_ID,'
      '    :ENTRY_TYPE_ID,'
      '    :ENTRY_DATE) ')
    StoredProcName = 'G1_CASE_GBP'
    Left = 56
    Top = 264
  end
  object tPARTS_TYPE: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    CONTENTID,'
      '    PREFIX,'
      '    NAME'
      'FROM'
      '    CATALOGCONTENT '
      'where '
      '  CATALOGID = :catalog '
      'order by NAME')
    Transaction = trMain
    Database = dbUni
    Left = 56
    Top = 488
    object tPARTS_TYPECONTENTID: TFIBIntegerField
      FieldName = 'CONTENTID'
    end
    object tPARTS_TYPEPREFIX: TFIBStringField
      FieldName = 'PREFIX'
      EmptyStrToNull = True
    end
    object tPARTS_TYPENAME: TFIBStringField
      FieldName = 'NAME'
      Size = 250
      EmptyStrToNull = True
    end
  end
  object dsPARTS_TYPE: TDataSource
    DataSet = tPARTS_TYPE
    Left = 56
    Top = 536
  end
  object tPARTS_STATUS: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    CONTENTID,'
      '    PREFIX,'
      '    NAME'
      'FROM'
      '    CATALOGCONTENT '
      'where '
      '  CATALOGID = 91023'
      'order by NAME')
    Transaction = trMain
    Database = dbUni
    Left = 120
    Top = 328
    object tPARTS_STATUSCONTENTID: TFIBIntegerField
      FieldName = 'CONTENTID'
    end
    object tPARTS_STATUSPREFIX: TFIBStringField
      FieldName = 'PREFIX'
      EmptyStrToNull = True
    end
    object tPARTS_STATUSNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 250
      EmptyStrToNull = True
    end
  end
  object dsPARTS_STATUS: TDataSource
    DataSet = tPARTS_STATUS
    Left = 120
    Top = 384
  end
  object qAllUni: TpFIBQuery
    Transaction = trMain
    Database = dbUni
    Left = 136
    Top = 56
  end
  object trAdd: TpFIBTransaction
    DefaultDatabase = dbAdd
    TimeoutAction = TARollback
    Left = 352
    Top = 80
  end
  object qAllAdd: TpFIBQuery
    Transaction = trAdd
    Database = dbAdd
    Left = 216
    Top = 56
  end
  object tAutoPostEss: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE AUTOPOST'
      'SET '
      '    POSTID = :POSTID,'
      '    POSTVALUE = :POSTVALUE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    AUTOPOST'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO AUTOPOST('
      '    ID,'
      '    POSTID,'
      '    POSTVALUE'
      ')'
      'VALUES('
      '    :ID,'
      '    :POSTID,'
      '    :POSTVALUE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    POSTID,'
      '    POSTVALUE'
      'FROM'
      '    AUTOPOST '
      ''
      ''
      ' WHERE '
      '        AUTOPOST.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    POSTID,'
      '    POSTVALUE'
      'FROM'
      '    AUTOPOST '
      'where POSTID=2'
      '')
    Transaction = trAdd
    Database = dbAdd
    Left = 344
    Top = 256
    oFetchAll = True
    object tAutoPostEssID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tAutoPostEssPOSTID: TFIBIntegerField
      FieldName = 'POSTID'
    end
    object tAutoPostEssPOSTVALUE: TFIBStringField
      FieldName = 'POSTVALUE'
      Size = 256
      EmptyStrToNull = True
    end
  end
  object dsAutoPostEss: TDataSource
    DataSet = tAutoPostEss
    Left = 344
    Top = 296
  end
  object tAutoPostRes: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE AUTOPOST'
      'SET '
      '    POSTID = :POSTID,'
      '    POSTVALUE = :POSTVALUE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    AUTOPOST'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO AUTOPOST('
      '    ID,'
      '    POSTID,'
      '    POSTVALUE'
      ')'
      'VALUES('
      '    :ID,'
      '    :POSTID,'
      '    :POSTVALUE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    POSTID,'
      '    POSTVALUE'
      'FROM'
      '    AUTOPOST '
      ''
      ''
      ' WHERE '
      '        AUTOPOST.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    POSTID,'
      '    POSTVALUE'
      'FROM'
      '    AUTOPOST '
      'where POSTID=1')
    Transaction = trAdd
    Database = dbAdd
    Left = 304
    Top = 256
    oFetchAll = True
    object tAutoPostResID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tAutoPostResPOSTID: TFIBIntegerField
      FieldName = 'POSTID'
    end
    object tAutoPostResPOSTVALUE: TFIBStringField
      FieldName = 'POSTVALUE'
      Size = 256
      EmptyStrToNull = True
    end
  end
  object dsAutoPostRes: TDataSource
    DataSet = tAutoPostRes
    Left = 304
    Top = 296
  end
  object tAutoPostName: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE AUTOPOST'
      'SET '
      '    POSTID = :POSTID,'
      '    POSTVALUE = :POSTVALUE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    AUTOPOST'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO AUTOPOST('
      '    ID,'
      '    POSTID,'
      '    POSTVALUE'
      ')'
      'VALUES('
      '    :ID,'
      '    :POSTID,'
      '    :POSTVALUE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    POSTID,'
      '    POSTVALUE'
      'FROM'
      '    AUTOPOST '
      ''
      ''
      ' WHERE '
      '        AUTOPOST.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    POSTID,'
      '    POSTVALUE'
      'FROM'
      '    AUTOPOST '
      'where POSTID=0')
    Transaction = trAdd
    Database = dbAdd
    Left = 264
    Top = 256
    oFetchAll = True
    object tAutoPostNameID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tAutoPostNamePOSTID: TFIBIntegerField
      FieldName = 'POSTID'
    end
    object tAutoPostNamePOSTVALUE: TFIBStringField
      FieldName = 'POSTVALUE'
      Size = 256
      EmptyStrToNull = True
    end
  end
  object dsAutoPostName: TDataSource
    DataSet = tAutoPostName
    Left = 264
    Top = 296
  end
  object tCategoryList: TpFIBDataSet
    SelectSQL.Strings = (
      '  select CONTENTID, PREFIX, NAME'
      '  from G1_CASE_F00_NEW(:CASE_TYPE_ID,:ENTRY_DATE,:RESULT_DATE)')
    Transaction = trMain
    Database = dbUni
    Left = 192
    Top = 152
    object tCategoryListCONTENTID: TFIBIntegerField
      FieldName = 'CONTENTID'
    end
    object tCategoryListPREFIX: TFIBStringField
      DisplayWidth = 4
      FieldName = 'PREFIX'
      EmptyStrToNull = True
    end
    object tCategoryListNAME: TFIBStringField
      DisplayWidth = 500
      FieldName = 'NAME'
      Size = 250
      EmptyStrToNull = True
    end
  end
  object dsCategoryList: TDataSource
    DataSet = tCategoryList
    Left = 192
    Top = 208
  end
  object tDocType: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    OBJECTID,'
      '    CODE,'
      '    DOCTYPENAME,'
      '    ARCHFLAG,'
      '    ORIGINALTYPE,'
      '    POSTTYPE,'
      '    FORM,'
      '    UNIDOCTYPE'
      'FROM'
      '    DOCTYPE '
      'where '
      '    ARCHFLAG = 0'
      ' and UNIDOCTYPE is not null   '
      'order by DOCTYPENAME ')
    Transaction = trAdd
    Database = dbAdd
    Left = 304
    Top = 144
    oFetchAll = True
    object tDocTypeOBJECTID: TFIBIntegerField
      FieldName = 'OBJECTID'
    end
    object tDocTypeCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 32
      EmptyStrToNull = True
    end
    object tDocTypeDOCTYPENAME: TFIBStringField
      FieldName = 'DOCTYPENAME'
      Size = 256
      EmptyStrToNull = True
    end
    object tDocTypeARCHFLAG: TFIBIntegerField
      FieldName = 'ARCHFLAG'
    end
    object tDocTypeORIGINALTYPE: TFIBIntegerField
      FieldName = 'ORIGINALTYPE'
    end
    object tDocTypePOSTTYPE: TFIBIntegerField
      FieldName = 'POSTTYPE'
    end
    object tDocTypeFORM: TFIBIntegerField
      FieldName = 'FORM'
    end
    object tDocTypeUNIDOCTYPE: TFIBIntegerField
      FieldName = 'UNIDOCTYPE'
    end
  end
  object dsDocType: TDataSource
    DataSet = tDocType
    Left = 304
    Top = 200
  end
  object tSetting: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE AUTOPOST'
      'SET '
      '    POSTID = :POSTID,'
      '    POSTVALUE = :POSTVALUE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    AUTOPOST'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO AUTOPOST('
      '    ID,'
      '    POSTID,'
      '    POSTVALUE'
      ')'
      'VALUES('
      '    :ID,'
      '    :POSTID,'
      '    :POSTVALUE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    POSTID,'
      '    POSTVALUE'
      'FROM'
      '    AUTOPOST '
      ''
      ''
      ' WHERE '
      '        AUTOPOST.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    VERS'
      'FROM'
      '    SETTINGS ')
    Transaction = trAdd
    Database = dbAdd
    Left = 232
    Top = 352
    object tSettingVERS: TFIBIntegerField
      FieldName = 'VERS'
    end
  end
  object dsSetting: TDataSource
    DataSet = tSetting
    Left = 232
    Top = 392
  end
  object tOutJournal: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    OBJECTID,'
      '    CODE,'
      '    POSTTYPENAME,'
      '    SHORTNAME'
      'FROM'
      '    POSTTYPE'
      'where archived = 0 and journaltype=1 '
      'order by POSTTYPENAME'
      '')
    Transaction = trAdd
    Database = dbAdd
    Left = 344
    Top = 376
  end
  object dsOutJournal: TDataSource
    DataSet = tOutJournal
    Left = 344
    Top = 432
  end
  object tCategory2: TpFIBDataSet
    SelectSQL.Strings = (
      '  select CONTENTID, PREFIX, NAME'
      '  from G1_CASE_F12(:CATEGORY_ID)')
    Transaction = trMain
    Database = dbUni
    DataSource = dsCategoryList
    Left = 248
    Top = 152
    object FIBIntegerField1: TFIBIntegerField
      FieldName = 'CONTENTID'
    end
    object FIBStringField1: TFIBStringField
      DisplayWidth = 2
      FieldName = 'PREFIX'
      EmptyStrToNull = True
    end
    object FIBStringField2: TFIBStringField
      FieldName = 'NAME'
      Size = 250
      EmptyStrToNull = True
    end
  end
  object dsCategory2: TDataSource
    DataSet = tCategory2
    Left = 248
    Top = 208
  end
end
