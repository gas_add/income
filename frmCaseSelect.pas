unit frmCaseSelect;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, FIBDataSet, pFIBDataSet, StdCtrls, FIBDatabase,
  pFIBDatabase, Grids, DBGrids, ExtCtrls;

type
  TfCaseSelect = class(TForm)
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    eCard: TComboBox;
    ePerson: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    tCase: TpFIBDataSet;
    dsCase: TDataSource;
    tCaseID: TIntegerField;
    tCaseCaseNumber: TIntegerField;
    tCaseShortNumber: TIntegerField;
    tCaseCaseYear: TIntegerField;
    procedure eCardChange(Sender: TObject);
    procedure ePersonChange(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    Catalog: integer;
    PartyName: string;
    procedure UpdateFilter;
    function WhereForPartyName: string;
    function WhereSection: string;
    function OrderSection: string;
  public
    { Public declarations }
  end;

var
  fCaseSelect: TfCaseSelect;

const
  CatalogName: array[0..4] of string = ('G1','U1','Adm','Adm1','M');
  ColFullCaseNum: array[0..4] of string = ('FULL_NUMBER','FULL_NUMBER','CASE_NUMBER','CASE_NUMBER','FULL_NUMBER');
  ColShortCaseNum: array[0..4] of string = ('SHORT_NUMBER','SHORT_NUMBER','ORDINAL_NUMBER','ORDINAL_NUMBER','REG_NUMBER');
  ColYear: array[0..4] of string = ('YEAR_REG','YEAR_REG','REG_YEAR','REG_YEAR','REG_YEAR');

function GetCase: integer;

implementation

uses dMain;

{$R *.dfm}

function GetCase: integer;
begin
  fCaseSelect:=TfCaseSelect.Create(Application);
  fCaseSelect.eCard.ItemIndex:=1;
  fCaseSelect.ePerson.Text:='';
  if fCaseSelect.ShowModal=mrCancel
  then begin
    Result:=-1;
  end
  else begin
     Result:= fCaseSelect.tCaseID.Value;
  end;

  fCaseSelect.Free;
end;


function TfCaseSelect.WhereForPartyName: string;
begin
  Case Catalog of
    0: Result:='(select count(id) from g1_parts p where p.case_id=g1_case.id and upper(p.name) like ''%'+PartyName+'%'')>0';
    1: Result:='((select count(id) from u1_parts p where p.case_id=u1_case.id and upper(p.name) like ''%'+PartyName+'%'')>0'+
              ' or (select count(id) from u1_defendant d where d.case_id=u1_case.id and upper(d.name) like ''%'+PartyName+'%'')>0)';
    2: Result:='(select count(id) from adm_parts p where p.case_id=adm_case.id and upper(p.party_name) like ''%'+PartyName+'%'')>0';
    3: Result:='upper(defendant_name) like ''%'+PartyName+'%''';
    4: Result:='((select count(id) from m_parts p where p.case_id=m_case.id and upper(p.name) like ''%'+PartyName+'%'')>0 or upper(name) like ''%'+PartyName+'%'')';
  end;
end;

function TfCaseSelect.WhereSection: string;
var R:string;
begin
  Result:='';
  if PartyName<>''
  then if Result='' then Result:='Where '+WhereForPartyName
                    else Result:=' and '+WhereForPartyName;
end;

function TfCaseSelect.OrderSection: string;
begin
  Result:=' Order by '+ColYear[Catalog]+' desc, '+ColShortCaseNum[Catalog]+' desc'
end;

procedure TfCaseSelect.UpdateFilter;
var f:string;
begin
  tCase.ReOpenWP([ColFullCaseNum[Catalog], ColShortCaseNum[Catalog], ColYear[Catalog],
                    CatalogName[Catalog], WhereSection, OrderSection[Catalog]]);
end;


procedure TfCaseSelect.eCardChange(Sender: TObject);
begin
  Catalog:=eCard.ItemIndex;
  UpdateFilter;
end;

procedure TfCaseSelect.ePersonChange(Sender: TObject);
begin
  if Length(ePerson.Text)>3
  then begin
    PartyName:=ePerson.Text;
    UpdateFilter;
  end
  else PartyName:='';
end;

procedure TfCaseSelect.DBGrid1DblClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

end.
