object fCaseSelect: TfCaseSelect
  Left = 377
  Top = 155
  Width = 701
  Height = 464
  Caption = 'fCaseSelect'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 693
    Height = 65
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 13
      Width = 55
      Height = 13
      Caption = #1050#1072#1088#1090#1086#1090#1077#1082#1072
    end
    object Label2: TLabel
      Left = 15
      Top = 35
      Width = 136
      Height = 13
      Caption = #1051#1080#1094#1086' '#1091#1095#1072#1089#1090#1074#1091#1102#1097#1077#1077' '#1074' '#1076#1077#1083#1077
    end
    object eCard: TComboBox
      Left = 160
      Top = 8
      Width = 353
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 0
      Text = #1043#1088#1072#1078#1076#1072#1085#1089#1082#1080#1077' '#1076#1077#1083#1072
      OnChange = eCardChange
      Items.Strings = (
        #1043#1088#1072#1078#1076#1072#1085#1089#1082#1080#1077' '#1076#1077#1083#1072
        #1059#1075#1086#1083#1086#1074#1085#1099#1077' '#1076#1077#1083#1072
        #1040#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1080#1074#1085#1099#1077' 1 '#1080#1085#1089#1090
        #1040#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1080#1074#1085#1099#1077' 1-'#1081' '#1087#1077#1088#1077#1089#1084#1086#1090#1088
        #1052#1072#1090#1077#1088#1080#1072#1083#1099)
    end
    object ePerson: TEdit
      Left = 160
      Top = 32
      Width = 353
      Height = 21
      TabOrder = 1
      Text = 'ePerson'
      OnChange = ePersonChange
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 65
    Width = 693
    Height = 324
    Align = alClient
    DataSource = dsCase
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CaseNumber'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ShortNumber'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CaseYear'
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 389
    Width = 693
    Height = 41
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      693
      41)
    object Button1: TButton
      Left = 520
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1054#1050
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 608
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object tCase: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SUBJECT'
      'SET '
      '    PARTS_STATUS_ID = :PARTS_STATUS_ID,'
      '    NAME = :NAME,'
      '    NATIVE = :NATIVE,'
      '    ADDRESS = :ADDRESS'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SUBJECT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SUBJECT('
      '    ID,'
      '    PARTS_STATUS_ID,'
      '    NAME,'
      '    NATIVE,'
      '    ADDRESS'
      ')'
      'VALUES('
      '    :ID,'
      '    :PARTS_STATUS_ID,'
      '    :NAME,'
      '    :NATIVE,'
      '    :ADDRESS'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    PARTS_STATUS_ID,'
      '    NAME,'
      '    NATIVE,'
      '    ADDRESS'
      'FROM'
      '    SUBJECT '
      'where(  @@wc%1=1@'
      '     ) and (     SUBJECT.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    @@ColFullCaseNum@ as CaseNumber,'
      '    @@ColShortCaseNum@ as ShortNumber,'
      '    @@ColYear@ as CaseYear'
      'FROM'
      '    @@CATALOG@_CASE  '
      '@@WhereSection@'
      '@@OrderSection@')
    AutoUpdateOptions.UpdateTableName = 'SUBJECT'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_SUBJECT_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmMain.trMain
    Database = dmMain.dbUni
    Left = 112
    Top = 224
    object tCaseID: TIntegerField
      FieldName = 'ID'
    end
    object tCaseCaseNumber: TIntegerField
      FieldName = 'CaseNumber'
    end
    object tCaseShortNumber: TIntegerField
      FieldName = 'ShortNumber'
    end
    object tCaseCaseYear: TIntegerField
      FieldName = 'CaseYear'
    end
  end
  object dsCase: TDataSource
    DataSet = tCase
    Left = 112
    Top = 256
  end
end
