unit frmPrinLetterList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, StdCtrls, ComCtrls, DB, ekbasereport, ekrtf,
  FIBDataSet, pFIBDataSet, FIBQuery, pFIBQuery;

type
  TfPrintLetterList = class(TForm)
    Label1: TLabel;
    dFrom: TDateTimePicker;
    dTo: TDateTimePicker;
    Button1: TButton;
    rtf: TEkRTF;
    q: TpFIBDataSet;
    qLETTERDATE: TFIBDateTimeField;
    qLETTERNAME: TFIBStringField;
    qSENDER: TFIBStringField;
    qRECIPIENT: TFIBIntegerField;
    qPASSDATE: TFIBDateTimeField;
    qDESCRIPTION: TFIBStringField;
    qRecipientName: TStringField;
    qNUM: TFIBStringField;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fPrintLetterList: TfPrintLetterList;

implementation

uses dMain;

{$R *.dfm}

procedure TfPrintLetterList.Button1Click(Sender: TObject);
begin
  try
    q.OpenWP([dFrom.Date, dTo.Date]);
    if dFrom.Date=dTo.Date
      then rtf.CreateVar('Date', DateToStr(dTo.Date)+'�.')
      else rtf.CreateVar('Date', '������ � '+DateToStr(dFrom.Date)+'�.- �� '+DateToStr(dTo.Date)+'�.');
    rtf.ExecuteOpen([q], SW_SHOWMAXIMIZED);

  finally
    rtf.ClearVars;
    if q.Active then q.Close;
    ModalResult:=mrOk;
  end;
end;

procedure TfPrintLetterList.FormShow(Sender: TObject);

begin
  dFrom.DateTime:=Date;
  dTo.DateTime:=Date;
end;

end.
