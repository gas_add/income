object frmLetterList: TfrmLetterList
  Left = 219
  Top = 117
  Caption = #1046#1091#1088#1085#1072#1083' '#1074#1093#1086#1076#1103#1097#1077#1081' '#1082#1086#1088#1088#1077#1089#1087#1086#1085#1076#1077#1085#1094#1080#1080
  ClientHeight = 529
  ClientWidth = 808
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 808
    Height = 41
    Align = alTop
    TabOrder = 0
    DesignSize = (
      808
      41)
    object Button2: TButton
      Left = 537
      Top = 6
      Width = 75
      Height = 25
      Action = aNewLetter
      Anchors = [akTop, akRight]
      TabOrder = 0
    end
    object Button3: TButton
      Left = 620
      Top = 6
      Width = 94
      Height = 25
      Action = aEditLetter
      Anchors = [akTop, akRight]
      TabOrder = 1
    end
    object Button5: TButton
      Left = 212
      Top = 6
      Width = 99
      Height = 25
      Action = aPrintLetterList
      TabOrder = 2
    end
    object Button6: TButton
      Left = 721
      Top = 6
      Width = 75
      Height = 25
      Action = aDeleteLetter
      Anchors = [akTop, akRight]
      TabOrder = 3
    end
    object Button7: TButton
      Left = 13
      Top = 6
      Width = 87
      Height = 25
      Action = aParams
      TabOrder = 4
    end
    object Button8: TButton
      Left = 106
      Top = 6
      Width = 99
      Height = 25
      Action = Action1
      TabOrder = 5
    end
    object Button9: TButton
      Left = 317
      Top = 6
      Width = 99
      Height = 25
      Action = aQrCode
      TabOrder = 6
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 808
    Height = 104
    Align = alTop
    TabOrder = 1
    DesignSize = (
      808
      104)
    object Label1: TLabel
      Left = 9
      Top = 21
      Width = 93
      Height = 13
      Caption = #1044#1072#1090#1072' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
    end
    object Label7: TLabel
      Left = 6
      Top = 60
      Width = 76
      Height = 13
      Caption = #1044#1072#1090#1072' '#1087#1077#1088#1077#1076#1072#1095#1080
    end
    object Label5: TLabel
      Left = 508
      Top = 61
      Width = 66
      Height = 13
      Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
    end
    object Label2: TLabel
      Left = 213
      Top = 21
      Width = 105
      Height = 13
      Caption = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1086#1085#1085#1099#1081' '#8470
    end
    object Label6: TLabel
      Left = 214
      Top = 60
      Width = 50
      Height = 13
      Caption = #1055#1077#1088#1077#1076#1072#1085#1086
    end
    object Label3: TLabel
      Left = 326
      Top = 22
      Width = 76
      Height = 13
      Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    end
    object Label4: TLabel
      Left = 509
      Top = 21
      Width = 112
      Height = 13
      Caption = #1042#1080#1076' '#1082#1086#1088#1088#1077#1089#1087#1086#1085#1076#1077#1085#1094#1080#1080
    end
    object Label8: TLabel
      Left = 396
      Top = 60
      Width = 72
      Height = 13
      Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1081' '#8470
    end
    object JvDBDatePickerEdit1: TJvDBDatePickerEdit
      Left = 6
      Top = 36
      Width = 97
      Height = 21
      AllowNoDate = True
      DataField = 'STARTLETTERDATE'
      DataSource = dsfLetterList
      DateFormat = 'dd.MM.yyyy'
      DateSeparator = '.'
      ShowCheckBox = True
      StoreDateFormat = True
      TabOrder = 0
      DesignSize = (
        95
        19)
    end
    object JvDBDatePickerEdit2: TJvDBDatePickerEdit
      Left = 111
      Top = 36
      Width = 97
      Height = 21
      AllowNoDate = True
      DataField = 'ENDLETTERDATE'
      DataSource = dsfLetterList
      DateFormat = 'dd.MM.yyyy'
      DateSeparator = '.'
      ShowCheckBox = True
      StoreDateFormat = True
      TabOrder = 1
      DesignSize = (
        95
        19)
    end
    object JvDBDatePickerEdit3: TJvDBDatePickerEdit
      Left = 6
      Top = 75
      Width = 97
      Height = 21
      AllowNoDate = True
      DataField = 'StartPASSDATE'
      DataSource = dsfLetterList
      DateFormat = 'dd.MM.yyyy'
      DateSeparator = '.'
      ShowCheckBox = True
      StoreDateFormat = True
      TabOrder = 2
      DesignSize = (
        95
        19)
    end
    object JvDBDatePickerEdit4: TJvDBDatePickerEdit
      Left = 111
      Top = 75
      Width = 97
      Height = 21
      AllowNoDate = True
      DataField = 'EndPASSDATE'
      DataSource = dsfLetterList
      DateFormat = 'dd.MM.yyyy'
      DateSeparator = '.'
      ShowCheckBox = True
      StoreDateFormat = True
      TabOrder = 3
      DesignSize = (
        95
        19)
    end
    object JvDBCheckBox1: TJvDBCheckBox
      Left = 8
      Top = 4
      Width = 161
      Height = 17
      Caption = #1057#1086#1082#1088#1072#1097#1077#1085#1085#1099#1081' '#1089#1087#1080#1089#1086#1082
      DataField = 'ShortList'
      DataSource = dsfLetterList
      TabOrder = 4
      ValueChecked = 'True'
      ValueUnchecked = 'False'
      Visible = False
    end
    object DBEdit1: TDBEdit
      Left = 213
      Top = 36
      Width = 109
      Height = 21
      DataField = 'Number'
      DataSource = dsfLetterList
      TabOrder = 5
    end
    object JvDBLookupCombo1: TJvDBLookupCombo
      Left = 213
      Top = 75
      Width = 180
      Height = 21
      DataField = 'RECIPIENT'
      DataSource = dsfLetterList
      LookupField = 'GROUPCONTENTID'
      LookupDisplay = 'USERNAME'
      LookupSource = dmMain.dsUser
      TabOrder = 6
    end
    object DBEdit2: TDBEdit
      Left = 325
      Top = 36
      Width = 180
      Height = 21
      DataField = 'LETTERNAME'
      DataSource = dsfLetterList
      TabOrder = 7
    end
    object JvDBLookupCombo2: TJvDBLookupCombo
      Left = 508
      Top = 36
      Width = 293
      Height = 21
      DataField = 'PostType'
      DataSource = dsfLetterList
      LookupField = 'OBJECTID'
      LookupDisplay = 'POSTTYPENAME'
      LookupSource = dmMain.dsPostType
      TabOrder = 8
    end
    object DBEdit3: TDBEdit
      Left = 396
      Top = 75
      Width = 109
      Height = 21
      DataField = 'OutNum'
      DataSource = dsfLetterList
      TabOrder = 9
    end
    object DBEdit4: TDBEdit
      Left = 508
      Top = 75
      Width = 292
      Height = 21
      DataField = 'SENDER'
      DataSource = dsfLetterList
      TabOrder = 10
    end
    object Button1: TButton
      Left = 639
      Top = 5
      Width = 75
      Height = 25
      Action = aFiltrRenew
      Anchors = [akTop, akRight]
      TabOrder = 11
    end
    object Button4: TButton
      Left = 721
      Top = 5
      Width = 75
      Height = 25
      Action = aFiltrClear
      Anchors = [akTop, akRight]
      TabOrder = 12
    end
  end
  object DBGridEh1: TDBGridEh
    Left = 0
    Top = 145
    Width = 808
    Height = 384
    Align = alClient
    DataSource = dsLetterList
    DynProps = <>
    FooterParams.Color = clWindow
    IndicatorOptions = [gioShowRowIndicatorEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    OnDblClick = aEditLetterExecute
    Columns = <
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'INCOMING'
        Footers = <>
        Width = 61
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'LETTERDATE'
        Footers = <>
        Width = 65
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'LETTERNAME'
        Footers = <>
        Width = 350
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'SENDER'
        Footers = <>
        Width = 136
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'RecipientName'
        Footers = <>
        Width = 127
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'DESCRIPTION'
        Footers = <>
        Width = 388
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object tLetterList: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT'
      '  l.ObjectID,'
      '  pt.CODE||'#39'-'#39'||l.NUMBER as Incoming,'
      '  l.LETTERDATE,'
      '  l.LETTERNAME,'
      '  l.PASSDATE,'
      '  l.DESCRIPTION,'
      '  l.SENDER,'
      '  l.RECIPIENT,'
      '  l.CardId,'
      '  l.CaseId'
      'FROM'
      '  LETTER l'
      '  LEFT JOIN POSTTYPE pt ON pt.OBJECTID = l.POSTTYPE'
      'where @@where%1=1@ and ObjectID=:ObjectID'
      'order by l.LETTERDATE,pt.CODE, l.NUMBER')
    SelectSQL.Strings = (
      'SELECT'
      '  l.ObjectID,'
      '  j.CODE||'#39'-'#39'||l.NUMBER as Incoming,'
      '  l.LETTERDATE,'
      '  l.LETTERNAME,'
      '  l.PASSDATE,'
      '  l.DESCRIPTION,'
      '  l.SENDER,'
      '  l.RECIPIENT,'
      '  l.CardId,'
      '  l.CaseId'
      'FROM'
      '  LETTER l'
      '  LEFT JOIN POSTTYPE pt ON pt.OBJECTID = l.POSTTYPE'
      '  LEFT join journal j on (pt.journalid=j.objectid)'
      'where @@where%1=1@ '
      'order by @@order%l.LETTERDATE,j.CODE, l.NUMBER@ ')
    Transaction = dmMain.trAdd
    Database = dmMain.dbAdd
    Left = 72
    Top = 33
    object tLetterListINCOMING: TFIBStringField
      DisplayLabel = #1042#1093'. '#8470
      FieldName = 'INCOMING'
      Size = 44
      EmptyStrToNull = True
    end
    object tLetterListLETTERDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1088#1077#1075'.'
      FieldName = 'LETTERDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object tLetterListLETTERNAME: TFIBStringField
      DisplayLabel = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
      FieldName = 'LETTERNAME'
      Size = 256
      EmptyStrToNull = True
    end
    object tLetterListSENDER: TFIBStringField
      DisplayLabel = #1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
      FieldName = 'SENDER'
      Size = 256
      EmptyStrToNull = True
    end
    object tLetterListPASSDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1087#1077#1088#1077#1076#1072#1095#1080
      FieldName = 'PASSDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object tLetterListDESCRIPTION: TFIBStringField
      DisplayLabel = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
      FieldName = 'DESCRIPTION'
      Size = 1024
      EmptyStrToNull = True
    end
    object tLetterListRecipientName: TFIBStringField
      DisplayLabel = #1055#1077#1088#1077#1076#1072#1085#1086
      FieldKind = fkLookup
      FieldName = 'RecipientName'
      LookupDataSet = dmMain.tUser
      LookupKeyFields = 'GROUPCONTENTID'
      LookupResultField = 'USERNAME'
      KeyFields = 'RECIPIENT'
      Size = 256
      EmptyStrToNull = True
      Lookup = True
    end
    object tLetterListRECIPIENT: TFIBIntegerField
      FieldName = 'RECIPIENT'
    end
    object tLetterListOBJECTID: TFIBIntegerField
      FieldName = 'OBJECTID'
    end
    object tLetterListCARDID: TFIBIntegerField
      FieldName = 'CARDID'
    end
    object tLetterListCASEID: TFIBIntegerField
      FieldName = 'CASEID'
    end
  end
  object dsLetterList: TDataSource
    DataSet = tLetterList
    Left = 72
    Top = 73
  end
  object fLetterList: TJvMemoryData
    Active = True
    FieldDefs = <
      item
        Name = 'ShortList'
        DataType = ftBoolean
      end
      item
        Name = 'STARTLETTERDATE'
        DataType = ftDate
      end
      item
        Name = 'ENDLETTERDATE'
        DataType = ftDate
      end
      item
        Name = 'PostType'
        DataType = ftInteger
      end
      item
        Name = 'Number'
        DataType = ftInteger
      end
      item
        Name = 'LETTERNAME'
        DataType = ftString
        Size = 256
      end
      item
        Name = 'StartPASSDATE'
        DataType = ftDate
      end
      item
        Name = 'EndPASSDATE'
        DataType = ftDate
      end
      item
        Name = 'RECIPIENT'
        DataType = ftInteger
      end
      item
        Name = 'SENDER'
        DataType = ftString
        Size = 256
      end
      item
        Name = 'DESCRIPTION'
        DataType = ftString
        Size = 1024
      end
      item
        Name = 'OutNum'
        DataType = ftString
        Size = 50
      end>
    Left = 24
    Top = 33
    object fLetterListShortList: TBooleanField
      FieldName = 'ShortList'
    end
    object fLetterListSTARTLETTERDATE: TDateField
      FieldName = 'STARTLETTERDATE'
    end
    object fLetterListENDLETTERDATE: TDateField
      FieldName = 'ENDLETTERDATE'
    end
    object fLetterListPostType: TIntegerField
      FieldName = 'PostType'
    end
    object fLetterListNumber: TIntegerField
      FieldName = 'Number'
    end
    object fLetterListLETTERNAME: TStringField
      FieldName = 'LETTERNAME'
      Size = 256
    end
    object fLetterListStartPASSDATE: TDateField
      FieldName = 'StartPASSDATE'
    end
    object fLetterListEndPASSDATE: TDateField
      FieldName = 'EndPASSDATE'
    end
    object fLetterListRECIPIENT: TIntegerField
      FieldName = 'RECIPIENT'
    end
    object fLetterListSENDER: TStringField
      FieldName = 'SENDER'
      Size = 256
    end
    object fLetterListDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 1024
    end
    object fLetterListOutNum: TStringField
      FieldName = 'OutNum'
      Size = 50
    end
  end
  object dsfLetterList: TJvDataSource
    DataSet = fLetterList
    Left = 24
    Top = 65
  end
  object ActionManager1: TActionManager
    ActionBars = <
      item
      end
      item
      end>
    Left = 425
    Top = 6
    StyleName = 'XP Style'
    object aFiltrClear: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
      OnExecute = aFiltrClearExecute
    end
    object aFiltrRenew: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnExecute = aFiltrRenewExecute
    end
    object aNewLetter: TAction
      Caption = #1053#1086#1074#1099#1081
      OnExecute = aNewLetterExecute
    end
    object aEditLetter: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      OnExecute = aEditLetterExecute
    end
    object aPrintLetterList: TAction
      Caption = #1055#1077#1095#1072#1090#1100' '#1078#1091#1088#1085#1072#1083#1072
      OnExecute = aPrintLetterListExecute
    end
    object aDeleteLetter: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = aDeleteLetterExecute
    end
    object aParams: TAction
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
      OnExecute = aParamsExecute
    end
    object Action1: TAction
      Caption = #1054#1090#1095#1077#1090#1099
      OnExecute = Action1Execute
    end
    object aQrCode: TAction
      Caption = #1064#1090#1088#1080#1093#1082#1086#1076
      OnExecute = aQrCodeExecute
    end
  end
  object rtf: TEkRTF
    InFile = 'LetterIn.rtf'
    OutFile = 'LetterOut.rtf'
    TrueValue = 'True'
    FalseValue = 'False'
    UDFList = rtfUDF
    UnicodeByDefault = False
    Charset = DEFAULT_CHARSET
    Lang = 0
    Options = [eoGraphicsBinary, eoDotAsColon]
    DisableControls = True
    Left = 120
    Top = 32
    VarDataTypes = {00000000}
  end
  object rtfUDF: TEkUDFList
    Functions = <
      item
        OnCalculate = rtfUDFFunctions0Calculate
        Name = 'DrowQrCode'
        ArgMinCount = 0
        ArgMaxCount = 0
        ResultType = 2
      end>
    Left = 152
    Top = 32
  end
end
