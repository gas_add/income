program BaseDoc;

uses
  Forms,
  dMain in 'dMain.pas' {dmMain: TDataModule},
  fMRLetter in 'fMRLetter.pas' {frmMRLetter},
  fLetterList in 'fLetterList.pas' {frmLetterList},
  frmLogin in 'frmLogin.pas' {fLogin},
  frmPrinLetterList in 'frmPrinLetterList.pas' {fPrintLetterList},
  frmParams in 'frmParams.pas' {fParam},
  frmMRSubject in '..\������ - ������� �� ����\frmMRSubject.pas' {fMRSubject},
  frmSubjectList in '..\������ - ������� �� ����\frmSubjectList.pas' {fSabjectList},
  frmViewCaseList in '..\������ - ������� �� ����\frmViewCaseList.pas' {fViewCase},
  DinamicForm in '..\Common\DinamicForm.pas',
  frmReport in '..\Common\frmReport.pas' {fReport},
  frmReportList in '..\Common\frmReportList.pas' {fReportList},
  frmCaseSelect in 'frmCaseSelect.pas' {fCaseSelect},
  dComplaint in 'dComplaint.pas' {dmComplaint: TDataModule},
  RegExpr in '..\Common\RegExpr.pas',
  QuricolAPI in '..\Common\QuricolAPI.pas',
  QuricolCode in '..\Common\QuricolCode.pas';

{$R *.res}

{type
  Tcourt = (dinsk, leninsk);

const
  Court = dinsk;
}
begin
  Application.Initialize;
  Application.Title := '�������� ���������������';
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TfrmLetterList, frmLetterList);
  Application.CreateForm(TfLogin, fLogin);
  Application.CreateForm(TfPrintLetterList, fPrintLetterList);
  Application.CreateForm(TfParam, fParam);
  Application.CreateForm(TfViewCase, fViewCase);
  Application.CreateForm(TfReport, fReport);
  Application.CreateForm(TfReportList, fReportList);
  Application.CreateForm(TdmComplaint, dmComplaint);
  Application.CreateForm(TfrmMRLetter, frmMRLetter);
  //  Application.CreateForm(TfrmFiltrBaseDoc, frmFiltrBaseDoc);
//  Application.CreateForm(TfrmNewBaseDoc, frmNewBaseDoc);
//  Application.CreateForm(TdmParticipant, dmParticipant);

  Application.Run;
end.
