object dmComplaint: TdmComplaint
  OldCreateOrder = False
  Left = 307
  Top = 110
  Height = 241
  Width = 792
  object tAdmCompl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADM_COMPLAINT'
      'SET '
      '    CASE_ID = :CASE_ID,'
      '    REGISTERED_DATE = :REGISTERED_DATE,'
      '    DOCUMENT_TYPE_ID = :DOCUMENT_TYPE_ID,'
      '    PARTY_TYPE_ID = :PARTY_TYPE_ID,'
      '    PART_ID = :PART_ID,'
      '    PASS_UP_DATE = :PASS_UP_DATE,'
      '    HIGHER_ECHELON_ID = :HIGHER_ECHELON_ID,'
      '    HIGHER_ECHELON_DATE = :HIGHER_ECHELON_DATE,'
      '    HIGHER_ECHELON_RESULT_ID = :HIGHER_ECHELON_RESULT_ID,'
      '    RETURNED_DATE = :RETURNED_DATE,'
      '    COMMENTS = :COMMENTS,'
      '    PARTY_NAME = :PARTY_NAME,'
      '    EVENT_ID = :EVENT_ID,'
      '    DOCUMENT_KIND_ID = :DOCUMENT_KIND_ID,'
      '    HIGHER_ECHELON_RESULT_ID2 = :HIGHER_ECHELON_RESULT_ID2,'
      '    HIGHER_ECHELON_RESULT_ID3 = :HIGHER_ECHELON_RESULT_ID3'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADM_COMPLAINT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADM_COMPLAINT('
      '    ID,'
      '    CASE_ID,'
      '    REGISTERED_DATE,'
      '    DOCUMENT_TYPE_ID,'
      '    PARTY_TYPE_ID,'
      '    PART_ID,'
      '    PASS_UP_DATE,'
      '    HIGHER_ECHELON_ID,'
      '    HIGHER_ECHELON_DATE,'
      '    HIGHER_ECHELON_RESULT_ID,'
      '    RETURNED_DATE,'
      '    COMMENTS,'
      '    PARTY_NAME,'
      '    EVENT_ID,'
      '    DOCUMENT_KIND_ID,'
      '    HIGHER_ECHELON_RESULT_ID2,'
      '    HIGHER_ECHELON_RESULT_ID3'
      ')'
      'VALUES('
      '    :ID,'
      '    :CASE_ID,'
      '    :REGISTERED_DATE,'
      '    :DOCUMENT_TYPE_ID,'
      '    :PARTY_TYPE_ID,'
      '    :PART_ID,'
      '    :PASS_UP_DATE,'
      '    :HIGHER_ECHELON_ID,'
      '    :HIGHER_ECHELON_DATE,'
      '    :HIGHER_ECHELON_RESULT_ID,'
      '    :RETURNED_DATE,'
      '    :COMMENTS,'
      '    :PARTY_NAME,'
      '    :EVENT_ID,'
      '    :DOCUMENT_KIND_ID,'
      '    :HIGHER_ECHELON_RESULT_ID2,'
      '    :HIGHER_ECHELON_RESULT_ID3'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    REGISTERED_DATE,'
      '    DOCUMENT_TYPE_ID,'
      '    PARTY_TYPE_ID,'
      '    PART_ID,'
      '    PASS_UP_DATE,'
      '    HIGHER_ECHELON_ID,'
      '    HIGHER_ECHELON_DATE,'
      '    HIGHER_ECHELON_RESULT_ID,'
      '    RETURNED_DATE,'
      '    COMMENTS,'
      '    PARTY_NAME,'
      '    EVENT_ID,'
      '    DOCUMENT_KIND_ID,'
      '    HIGHER_ECHELON_RESULT_ID2,'
      '    HIGHER_ECHELON_RESULT_ID3'
      'FROM'
      '    ADM_COMPLAINT '
      'where(  ID=:ComplaintId'
      '     ) and (     ADM_COMPLAINT.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    REGISTERED_DATE,'
      '    DOCUMENT_TYPE_ID,'
      '    PARTY_TYPE_ID,'
      '    PART_ID,'
      '    PASS_UP_DATE,'
      '    HIGHER_ECHELON_ID,'
      '    HIGHER_ECHELON_DATE,'
      '    HIGHER_ECHELON_RESULT_ID,'
      '    RETURNED_DATE,'
      '    COMMENTS,'
      '    PARTY_NAME,'
      '    EVENT_ID,'
      '    DOCUMENT_KIND_ID,'
      '    HIGHER_ECHELON_RESULT_ID2,'
      '    HIGHER_ECHELON_RESULT_ID3'
      'FROM'
      '    ADM_COMPLAINT '
      'where ID=:ComplaintId')
    Transaction = dmMain.trMain
    Database = dmMain.dbUni
    DataSource = frmMRLetter.dsLetter
    Left = 64
    Top = 48
    oFetchAll = True
    object tAdmComplID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tAdmComplCASE_ID: TFIBIntegerField
      FieldName = 'CASE_ID'
    end
    object tAdmComplREGISTERED_DATE: TFIBDateTimeField
      FieldName = 'REGISTERED_DATE'
    end
    object tAdmComplDOCUMENT_TYPE_ID: TFIBIntegerField
      FieldName = 'DOCUMENT_TYPE_ID'
    end
    object tAdmComplPARTY_TYPE_ID: TFIBIntegerField
      FieldName = 'PARTY_TYPE_ID'
    end
    object tAdmComplPART_ID: TFIBIntegerField
      FieldName = 'PART_ID'
    end
    object tAdmComplPASS_UP_DATE: TFIBDateTimeField
      FieldName = 'PASS_UP_DATE'
    end
    object tAdmComplHIGHER_ECHELON_ID: TFIBIntegerField
      FieldName = 'HIGHER_ECHELON_ID'
    end
    object tAdmComplHIGHER_ECHELON_DATE: TFIBDateTimeField
      FieldName = 'HIGHER_ECHELON_DATE'
    end
    object tAdmComplHIGHER_ECHELON_RESULT_ID: TFIBIntegerField
      FieldName = 'HIGHER_ECHELON_RESULT_ID'
    end
    object tAdmComplRETURNED_DATE: TFIBDateTimeField
      FieldName = 'RETURNED_DATE'
    end
    object tAdmComplCOMMENTS: TFIBStringField
      FieldName = 'COMMENTS'
      Size = 250
      EmptyStrToNull = True
    end
    object tAdmComplPARTY_NAME: TFIBStringField
      FieldName = 'PARTY_NAME'
      Size = 250
      EmptyStrToNull = True
    end
    object tAdmComplEVENT_ID: TFIBIntegerField
      FieldName = 'EVENT_ID'
    end
    object tAdmComplDOCUMENT_KIND_ID: TFIBIntegerField
      FieldName = 'DOCUMENT_KIND_ID'
    end
    object tAdmComplHIGHER_ECHELON_RESULT_ID2: TFIBIntegerField
      FieldName = 'HIGHER_ECHELON_RESULT_ID2'
    end
    object tAdmComplHIGHER_ECHELON_RESULT_ID3: TFIBIntegerField
      FieldName = 'HIGHER_ECHELON_RESULT_ID3'
    end
  end
  object dsAdmCompl: TDataSource
    DataSet = tAdmCompl
    Left = 64
    Top = 104
  end
  object tAdm1Compl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADM1_COMPLAINT'
      'SET '
      '    CASE_ID = :CASE_ID,'
      '    APPEAL_DECREE_ID = :APPEAL_DECREE_ID,'
      '    APPEAL_RESULT_DATE = :APPEAL_RESULT_DATE,'
      '    COMMENTS = :COMMENTS,'
      '    DECLARANT_NAME = :DECLARANT_NAME,'
      '    DECLARANT_STATUS_ID = :DECLARANT_STATUS_ID,'
      '    PASS_UP_DATE = :PASS_UP_DATE,'
      '    REGISTERED_DATE = :REGISTERED_DATE,'
      '    RETURNED_DATE = :RETURNED_DATE,'
      '    APPEAL_DECREE_ID2 = :APPEAL_DECREE_ID2,'
      '    DOCUMENT_KIND_ID = :DOCUMENT_KIND_ID,'
      '    APPEAL_DECREE_ID3 = :APPEAL_DECREE_ID3'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADM1_COMPLAINT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADM1_COMPLAINT('
      '    ID,'
      '    CASE_ID,'
      '    APPEAL_DECREE_ID,'
      '    APPEAL_RESULT_DATE,'
      '    COMMENTS,'
      '    DECLARANT_NAME,'
      '    DECLARANT_STATUS_ID,'
      '    PASS_UP_DATE,'
      '    REGISTERED_DATE,'
      '    RETURNED_DATE,'
      '    APPEAL_DECREE_ID2,'
      '    DOCUMENT_KIND_ID,'
      '    APPEAL_DECREE_ID3'
      ')'
      'VALUES('
      '    :ID,'
      '    :CASE_ID,'
      '    :APPEAL_DECREE_ID,'
      '    :APPEAL_RESULT_DATE,'
      '    :COMMENTS,'
      '    :DECLARANT_NAME,'
      '    :DECLARANT_STATUS_ID,'
      '    :PASS_UP_DATE,'
      '    :REGISTERED_DATE,'
      '    :RETURNED_DATE,'
      '    :APPEAL_DECREE_ID2,'
      '    :DOCUMENT_KIND_ID,'
      '    :APPEAL_DECREE_ID3'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    APPEAL_DECREE_ID,'
      '    APPEAL_RESULT_DATE,'
      '    COMMENTS,'
      '    DECLARANT_NAME,'
      '    DECLARANT_STATUS_ID,'
      '    PASS_UP_DATE,'
      '    REGISTERED_DATE,'
      '    RETURNED_DATE,'
      '    APPEAL_DECREE_ID2,'
      '    DOCUMENT_KIND_ID,'
      '    APPEAL_DECREE_ID3'
      'FROM'
      '    ADM1_COMPLAINT '
      'where(  ID=:ComplaintId'
      '     ) and (     ADM1_COMPLAINT.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    APPEAL_DECREE_ID,'
      '    APPEAL_RESULT_DATE,'
      '    COMMENTS,'
      '    DECLARANT_NAME,'
      '    DECLARANT_STATUS_ID,'
      '    PASS_UP_DATE,'
      '    REGISTERED_DATE,'
      '    RETURNED_DATE,'
      '    APPEAL_DECREE_ID2,'
      '    DOCUMENT_KIND_ID,'
      '    APPEAL_DECREE_ID3'
      'FROM'
      '    ADM1_COMPLAINT '
      'where ID=:ComplaintId')
    Transaction = dmMain.trMain
    Database = dmMain.dbUni
    DataSource = frmMRLetter.dsLetter
    Left = 136
    Top = 48
    oFetchAll = True
    object tAdm1ComplID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tAdm1ComplCASE_ID: TFIBIntegerField
      FieldName = 'CASE_ID'
    end
    object tAdm1ComplAPPEAL_DECREE_ID: TFIBIntegerField
      FieldName = 'APPEAL_DECREE_ID'
    end
    object tAdm1ComplAPPEAL_RESULT_DATE: TFIBDateTimeField
      FieldName = 'APPEAL_RESULT_DATE'
    end
    object tAdm1ComplCOMMENTS: TFIBStringField
      FieldName = 'COMMENTS'
      Size = 512
      EmptyStrToNull = True
    end
    object tAdm1ComplDECLARANT_NAME: TFIBStringField
      FieldName = 'DECLARANT_NAME'
      Size = 250
      EmptyStrToNull = True
    end
    object tAdm1ComplDECLARANT_STATUS_ID: TFIBIntegerField
      FieldName = 'DECLARANT_STATUS_ID'
    end
    object tAdm1ComplPASS_UP_DATE: TFIBDateTimeField
      FieldName = 'PASS_UP_DATE'
    end
    object tAdm1ComplREGISTERED_DATE: TFIBDateTimeField
      FieldName = 'REGISTERED_DATE'
    end
    object tAdm1ComplRETURNED_DATE: TFIBDateTimeField
      FieldName = 'RETURNED_DATE'
    end
    object tAdm1ComplAPPEAL_DECREE_ID2: TFIBIntegerField
      FieldName = 'APPEAL_DECREE_ID2'
    end
    object tAdm1ComplDOCUMENT_KIND_ID: TFIBIntegerField
      FieldName = 'DOCUMENT_KIND_ID'
    end
    object tAdm1ComplAPPEAL_DECREE_ID3: TFIBIntegerField
      FieldName = 'APPEAL_DECREE_ID3'
    end
  end
  object dsAdm1Compl: TDataSource
    DataSet = tAdm1Compl
    Left = 136
    Top = 104
  end
  object tG1Compl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE G1_COMPLAINT'
      'SET '
      '    CASE_ID = :CASE_ID,'
      '    REGISTERED_DATE = :REGISTERED_DATE,'
      '    DOCUMENT_TYPE_ID = :DOCUMENT_TYPE_ID,'
      '    PARTY_TYPE_ID = :PARTY_TYPE_ID,'
      '    PARTY_NAME = :PARTY_NAME,'
      '    PASS_UP_DATE = :PASS_UP_DATE,'
      '    COURT_II_ID = :COURT_II_ID,'
      '    COURT_II_DATE = :COURT_II_DATE,'
      '    RESULT_II_ID = :RESULT_II_ID,'
      '    RETURNED_DATE = :RETURNED_DATE,'
      '    COMMENTS = :COMMENTS,'
      '    TIMING_UP = :TIMING_UP'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    G1_COMPLAINT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO G1_COMPLAINT('
      '    ID,'
      '    CASE_ID,'
      '    REGISTERED_DATE,'
      '    DOCUMENT_TYPE_ID,'
      '    PARTY_TYPE_ID,'
      '    PARTY_NAME,'
      '    PASS_UP_DATE,'
      '    COURT_II_ID,'
      '    COURT_II_DATE,'
      '    RESULT_II_ID,'
      '    RETURNED_DATE,'
      '    COMMENTS,'
      '    TIMING_UP'
      ')'
      'VALUES('
      '    :ID,'
      '    :CASE_ID,'
      '    :REGISTERED_DATE,'
      '    :DOCUMENT_TYPE_ID,'
      '    :PARTY_TYPE_ID,'
      '    :PARTY_NAME,'
      '    :PASS_UP_DATE,'
      '    :COURT_II_ID,'
      '    :COURT_II_DATE,'
      '    :RESULT_II_ID,'
      '    :RETURNED_DATE,'
      '    :COMMENTS,'
      '    :TIMING_UP'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    REGISTERED_DATE,'
      '    DOCUMENT_TYPE_ID,'
      '    PARTY_TYPE_ID,'
      '    PARTY_NAME,'
      '    PASS_UP_DATE,'
      '    COURT_II_ID,'
      '    COURT_II_DATE,'
      '    RESULT_II_ID,'
      '    RETURNED_DATE,'
      '    COMMENTS,'
      '    TIMING_UP'
      'FROM'
      '    G1_COMPLAINT '
      'where(  ID=:ComplaintId'
      '     ) and (     G1_COMPLAINT.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    REGISTERED_DATE,'
      '    DOCUMENT_TYPE_ID,'
      '    PARTY_TYPE_ID,'
      '    PARTY_NAME,'
      '    PASS_UP_DATE,'
      '    COURT_II_ID,'
      '    COURT_II_DATE,'
      '    RESULT_II_ID,'
      '    RETURNED_DATE,'
      '    COMMENTS,'
      '    TIMING_UP'
      'FROM'
      '    G1_COMPLAINT '
      'where ID=:ComplaintId')
    Transaction = dmMain.trMain
    Database = dmMain.dbUni
    DataSource = frmMRLetter.dsLetter
    Left = 200
    Top = 48
    oFetchAll = True
    object tG1ComplID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tG1ComplCASE_ID: TFIBIntegerField
      FieldName = 'CASE_ID'
    end
    object tG1ComplREGISTERED_DATE: TFIBDateTimeField
      FieldName = 'REGISTERED_DATE'
    end
    object tG1ComplDOCUMENT_TYPE_ID: TFIBIntegerField
      FieldName = 'DOCUMENT_TYPE_ID'
    end
    object tG1ComplPARTY_TYPE_ID: TFIBIntegerField
      FieldName = 'PARTY_TYPE_ID'
    end
    object tG1ComplPARTY_NAME: TFIBStringField
      FieldName = 'PARTY_NAME'
      Size = 250
      EmptyStrToNull = True
    end
    object tG1ComplPASS_UP_DATE: TFIBDateTimeField
      FieldName = 'PASS_UP_DATE'
    end
    object tG1ComplCOURT_II_ID: TFIBIntegerField
      FieldName = 'COURT_II_ID'
    end
    object tG1ComplCOURT_II_DATE: TFIBDateTimeField
      FieldName = 'COURT_II_DATE'
    end
    object tG1ComplRESULT_II_ID: TFIBIntegerField
      FieldName = 'RESULT_II_ID'
    end
    object tG1ComplRETURNED_DATE: TFIBDateTimeField
      FieldName = 'RETURNED_DATE'
    end
    object tG1ComplCOMMENTS: TFIBStringField
      FieldName = 'COMMENTS'
      Size = 512
      EmptyStrToNull = True
    end
    object tG1ComplTIMING_UP: TFIBDateTimeField
      FieldName = 'TIMING_UP'
    end
  end
  object dsG1Compl: TDataSource
    DataSet = tG1Compl
    Left = 200
    Top = 104
  end
  object tU1Compl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE U1_COMPLAINT'
      'SET '
      '    CASE_ID = :CASE_ID,'
      '    REGISTERED_DATE = :REGISTERED_DATE,'
      '    DOCUMENT_TYPE_ID = :DOCUMENT_TYPE_ID,'
      '    PARTY_TYPE_ID = :PARTY_TYPE_ID,'
      '    PARTY_NAME = :PARTY_NAME,'
      '    PASS_UP_DATE = :PASS_UP_DATE,'
      '    COURT_II_ID = :COURT_II_ID,'
      '    COURT_II_DATE = :COURT_II_DATE,'
      '    VERDICT_II_ID = :VERDICT_II_ID,'
      '    VERDICT_II_ID2 = :VERDICT_II_ID2,'
      '    VERDICT_II_ID3 = :VERDICT_II_ID3,'
      '    RETURNED_DATE = :RETURNED_DATE,'
      '    COMMENTS = :COMMENTS,'
      '    TIMING_UP = :TIMING_UP'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    U1_COMPLAINT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO U1_COMPLAINT('
      '    ID,'
      '    CASE_ID,'
      '    REGISTERED_DATE,'
      '    DOCUMENT_TYPE_ID,'
      '    PARTY_TYPE_ID,'
      '    PARTY_NAME,'
      '    PASS_UP_DATE,'
      '    COURT_II_ID,'
      '    COURT_II_DATE,'
      '    VERDICT_II_ID,'
      '    VERDICT_II_ID2,'
      '    VERDICT_II_ID3,'
      '    RETURNED_DATE,'
      '    COMMENTS,'
      '    TIMING_UP'
      ')'
      'VALUES('
      '    :ID,'
      '    :CASE_ID,'
      '    :REGISTERED_DATE,'
      '    :DOCUMENT_TYPE_ID,'
      '    :PARTY_TYPE_ID,'
      '    :PARTY_NAME,'
      '    :PASS_UP_DATE,'
      '    :COURT_II_ID,'
      '    :COURT_II_DATE,'
      '    :VERDICT_II_ID,'
      '    :VERDICT_II_ID2,'
      '    :VERDICT_II_ID3,'
      '    :RETURNED_DATE,'
      '    :COMMENTS,'
      '    :TIMING_UP'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    REGISTERED_DATE,'
      '    DOCUMENT_TYPE_ID,'
      '    PARTY_TYPE_ID,'
      '    PARTY_NAME,'
      '    PASS_UP_DATE,'
      '    COURT_II_ID,'
      '    COURT_II_DATE,'
      '    VERDICT_II_ID,'
      '    VERDICT_II_ID2,'
      '    VERDICT_II_ID3,'
      '    RETURNED_DATE,'
      '    COMMENTS,'
      '    TIMING_UP'
      'FROM'
      '    U1_COMPLAINT'
      'where(  ID=:ComplaintId'
      '     ) and (     U1_COMPLAINT.ID = :OLD_ID'
      '     )'
      '     ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    REGISTERED_DATE,'
      '    DOCUMENT_TYPE_ID,'
      '    PARTY_TYPE_ID,'
      '    PARTY_NAME,'
      '    PASS_UP_DATE,'
      '    COURT_II_ID,'
      '    COURT_II_DATE,'
      '    VERDICT_II_ID,'
      '    VERDICT_II_ID2,'
      '    VERDICT_II_ID3,'
      '    RETURNED_DATE,'
      '    COMMENTS,'
      '    TIMING_UP'
      'FROM'
      '    U1_COMPLAINT'
      'where ID=:ComplaintId ')
    Transaction = dmMain.trMain
    Database = dmMain.dbUni
    DataSource = frmMRLetter.dsLetter
    Left = 272
    Top = 48
    oFetchAll = True
    object tU1ComplID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tU1ComplCASE_ID: TFIBIntegerField
      FieldName = 'CASE_ID'
    end
    object tU1ComplREGISTERED_DATE: TFIBDateTimeField
      FieldName = 'REGISTERED_DATE'
    end
    object tU1ComplDOCUMENT_TYPE_ID: TFIBIntegerField
      FieldName = 'DOCUMENT_TYPE_ID'
    end
    object tU1ComplPARTY_TYPE_ID: TFIBIntegerField
      FieldName = 'PARTY_TYPE_ID'
    end
    object tU1ComplPARTY_NAME: TFIBStringField
      FieldName = 'PARTY_NAME'
      Size = 250
      EmptyStrToNull = True
    end
    object tU1ComplPASS_UP_DATE: TFIBDateTimeField
      FieldName = 'PASS_UP_DATE'
    end
    object tU1ComplCOURT_II_ID: TFIBIntegerField
      FieldName = 'COURT_II_ID'
    end
    object tU1ComplCOURT_II_DATE: TFIBDateTimeField
      FieldName = 'COURT_II_DATE'
    end
    object tU1ComplVERDICT_II_ID: TFIBIntegerField
      FieldName = 'VERDICT_II_ID'
    end
    object tU1ComplVERDICT_II_ID2: TFIBIntegerField
      FieldName = 'VERDICT_II_ID2'
    end
    object tU1ComplVERDICT_II_ID3: TFIBIntegerField
      FieldName = 'VERDICT_II_ID3'
    end
    object tU1ComplRETURNED_DATE: TFIBDateTimeField
      FieldName = 'RETURNED_DATE'
    end
    object tU1ComplCOMMENTS: TFIBStringField
      FieldName = 'COMMENTS'
      Size = 512
      EmptyStrToNull = True
    end
    object tU1ComplTIMING_UP: TFIBDateTimeField
      FieldName = 'TIMING_UP'
    end
  end
  object dsU1Compl: TDataSource
    DataSet = tU1Compl
    Left = 272
    Top = 104
  end
  object tMCompl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE M_COMPLAINT'
      'SET '
      '    CASE_ID = :CASE_ID,'
      '    PASS_UP_DATE = :PASS_UP_DATE,'
      '    ENTRY_DATE = :ENTRY_DATE,'
      '    RESULT_UP_ID = :RESULT_UP_ID,'
      '    JUDGE_ID = :JUDGE_ID,'
      '    RESULT_UP_DATE = :RESULT_UP_DATE,'
      '    RETURNED_DATE = :RETURNED_DATE,'
      '    DECLARANT_NAME = :DECLARANT_NAME,'
      '    COMMENTS = :COMMENTS'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    M_COMPLAINT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO M_COMPLAINT('
      '    ID,'
      '    CASE_ID,'
      '    PASS_UP_DATE,'
      '    ENTRY_DATE,'
      '    RESULT_UP_ID,'
      '    JUDGE_ID,'
      '    RESULT_UP_DATE,'
      '    RETURNED_DATE,'
      '    DECLARANT_NAME,'
      '    COMMENTS'
      ')'
      'VALUES('
      '    :ID,'
      '    :CASE_ID,'
      '    :PASS_UP_DATE,'
      '    :ENTRY_DATE,'
      '    :RESULT_UP_ID,'
      '    :JUDGE_ID,'
      '    :RESULT_UP_DATE,'
      '    :RETURNED_DATE,'
      '    :DECLARANT_NAME,'
      '    :COMMENTS'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    PASS_UP_DATE,'
      '    ENTRY_DATE,'
      '    RESULT_UP_ID,'
      '    JUDGE_ID,'
      '    RESULT_UP_DATE,'
      '    RETURNED_DATE,'
      '    DECLARANT_NAME,'
      '    COMMENTS'
      'FROM'
      '    M_COMPLAINT '
      'where(  ID=:ComplaintId'
      '     ) and (     M_COMPLAINT.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    PASS_UP_DATE,'
      '    ENTRY_DATE,'
      '    RESULT_UP_ID,'
      '    JUDGE_ID,'
      '    RESULT_UP_DATE,'
      '    RETURNED_DATE,'
      '    DECLARANT_NAME,'
      '    COMMENTS'
      'FROM'
      '    M_COMPLAINT '
      'where ID=:ComplaintId')
    Transaction = dmMain.trMain
    Database = dmMain.dbUni
    DataSource = frmMRLetter.dsLetter
    Left = 344
    Top = 48
    oFetchAll = True
    object tMComplID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tMComplCASE_ID: TFIBIntegerField
      FieldName = 'CASE_ID'
    end
    object tMComplPASS_UP_DATE: TFIBDateTimeField
      FieldName = 'PASS_UP_DATE'
    end
    object tMComplENTRY_DATE: TFIBDateTimeField
      FieldName = 'ENTRY_DATE'
    end
    object tMComplRESULT_UP_ID: TFIBIntegerField
      FieldName = 'RESULT_UP_ID'
    end
    object tMComplJUDGE_ID: TFIBIntegerField
      FieldName = 'JUDGE_ID'
    end
    object tMComplRESULT_UP_DATE: TFIBDateTimeField
      FieldName = 'RESULT_UP_DATE'
    end
    object tMComplRETURNED_DATE: TFIBDateTimeField
      FieldName = 'RETURNED_DATE'
    end
    object tMComplDECLARANT_NAME: TFIBStringField
      FieldName = 'DECLARANT_NAME'
      Size = 250
      EmptyStrToNull = True
    end
    object tMComplCOMMENTS: TFIBStringField
      FieldName = 'COMMENTS'
      Size = 512
      EmptyStrToNull = True
    end
  end
  object dsMCompl: TDataSource
    DataSet = tMCompl
    Left = 344
    Top = 104
  end
  object tComplKind: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE M_COMPLAINT'
      'SET '
      '    CASE_ID = :CASE_ID,'
      '    PASS_UP_DATE = :PASS_UP_DATE,'
      '    ENTRY_DATE = :ENTRY_DATE,'
      '    RESULT_UP_ID = :RESULT_UP_ID,'
      '    JUDGE_ID = :JUDGE_ID,'
      '    RESULT_UP_DATE = :RESULT_UP_DATE,'
      '    RETURNED_DATE = :RETURNED_DATE,'
      '    DECLARANT_NAME = :DECLARANT_NAME,'
      '    COMMENTS = :COMMENTS'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    M_COMPLAINT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO M_COMPLAINT('
      '    ID,'
      '    CASE_ID,'
      '    PASS_UP_DATE,'
      '    ENTRY_DATE,'
      '    RESULT_UP_ID,'
      '    JUDGE_ID,'
      '    RESULT_UP_DATE,'
      '    RETURNED_DATE,'
      '    DECLARANT_NAME,'
      '    COMMENTS'
      ')'
      'VALUES('
      '    :ID,'
      '    :CASE_ID,'
      '    :PASS_UP_DATE,'
      '    :ENTRY_DATE,'
      '    :RESULT_UP_ID,'
      '    :JUDGE_ID,'
      '    :RESULT_UP_DATE,'
      '    :RETURNED_DATE,'
      '    :DECLARANT_NAME,'
      '    :COMMENTS'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASE_ID,'
      '    PASS_UP_DATE,'
      '    ENTRY_DATE,'
      '    RESULT_UP_ID,'
      '    JUDGE_ID,'
      '    RESULT_UP_DATE,'
      '    RETURNED_DATE,'
      '    DECLARANT_NAME,'
      '    COMMENTS'
      'FROM'
      '    M_COMPLAINT '
      'where(  ID=:ComplaintId'
      '     ) and (     M_COMPLAINT.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    CONTENTID,'
      '    PREFIX,'
      '    NAME'
      'FROM'
      '    CATALOGCONTENT '
      'where '
      '  @@where@ '
      'order by NAME')
    Transaction = dmMain.trMain
    Database = dmMain.dbUni
    DataSource = frmMRLetter.dsLetter
    Left = 416
    Top = 48
    oFetchAll = True
    object tComplKindCONTENTID: TFIBIntegerField
      FieldName = 'CONTENTID'
    end
    object tComplKindPREFIX: TFIBStringField
      FieldName = 'PREFIX'
      EmptyStrToNull = True
    end
    object tComplKindNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 250
      EmptyStrToNull = True
    end
  end
  object dsComplKind: TDataSource
    DataSet = tComplKind
    Left = 416
    Top = 104
  end
  object tComplType: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    CONTENTID,'
      '    PREFIX,'
      '    NAME'
      'FROM'
      '    CATALOGCONTENT '
      'where '
      '  CATALOGID = 4029 '
      'order by NAME')
    Transaction = dmMain.trMain
    Database = dmMain.dbUni
    Left = 480
    Top = 48
    object tComplTypeCONTENTID: TFIBIntegerField
      FieldName = 'CONTENTID'
    end
    object tComplTypePREFIX: TFIBStringField
      FieldName = 'PREFIX'
      EmptyStrToNull = True
    end
    object tComplTypeNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 250
      EmptyStrToNull = True
    end
  end
  object dsComplType: TDataSource
    DataSet = tComplType
    Left = 480
    Top = 104
  end
end
