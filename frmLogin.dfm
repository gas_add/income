object fLogin: TfLogin
  Left = 476
  Top = 338
  Width = 381
  Height = 174
  Caption = #1040#1074#1090#1086#1088#1080#1079#1072#1094#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 37
    Width = 73
    Height = 13
    Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
  end
  object Label2: TLabel
    Left = 8
    Top = 63
    Width = 38
    Height = 13
    Caption = #1055#1072#1088#1086#1083#1100
  end
  object vUser: TJvDBLookupCombo
    Left = 88
    Top = 32
    Width = 273
    Height = 21
    DataField = 'GROUPCONTENTID'
    DataSource = dsfLogin
    LookupField = 'GROUPCONTENTID'
    LookupDisplay = 'USERNAME'
    LookupSource = dmMain.dsUser
    TabOrder = 0
  end
  object Button1: TButton
    Left = 144
    Top = 104
    Width = 75
    Height = 25
    Caption = #1043#1086#1090#1086#1074#1086
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object vPassword: TMaskEdit
    Left = 88
    Top = 61
    Width = 273
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
  end
  object fLogin: TJvMemoryData
    FieldDefs = <
      item
        Name = 'GROUPCONTENTID'
        DataType = ftInteger
      end
      item
        Name = 'USERNAME'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'USERPSW'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'VA_CODE'
        DataType = ftString
        Size = 50
      end>
    Left = 24
    Top = 9
    object fLoginGROUPCONTENTID: TIntegerField
      FieldName = 'GROUPCONTENTID'
    end
    object fLoginUSERPSW: TStringField
      DisplayWidth = 20
      FieldName = 'USERPSW'
      Size = 10
    end
  end
  object dsfLogin: TJvDataSource
    DataSet = fLogin
    Left = 56
    Top = 9
  end
end
