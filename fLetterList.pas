unit fLetterList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, JvExControls, JvDBLookup, DBCtrls, JvDBCheckBox, DB,
  Mask, JvExMask, JvToolEdit, JvMaskEdit, JvCheckedMaskEdit,
  JvDatePickerEdit, JvDBDatePickerEdit, JvDataSource, JvMemoryDataset,
  FIBDataSet, pFIBDataSet, GridsEh, DBGridEh, ExtCtrls, XPStyleActnCtrls,
  ActnList, ActnMan, ToolWin, ActnCtrls, ekbasereport, ekrtf, Grids,
  DBGrids, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  DBAxisGridsEh, ekfunc;

type
  TfrmLetterList = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    DBGridEh1: TDBGridEh;
    tLetterList: TpFIBDataSet;
    dsLetterList: TDataSource;
    tLetterListLETTERDATE: TFIBDateTimeField;
    tLetterListLETTERNAME: TFIBStringField;
    tLetterListPASSDATE: TFIBDateTimeField;
    tLetterListDESCRIPTION: TFIBStringField;
    tLetterListSENDER: TFIBStringField;
    tLetterListRECIPIENT: TFIBIntegerField;
    tLetterListRecipientName: TFIBStringField;
    tLetterListINCOMING: TFIBStringField;
    Label1: TLabel;
    Label7: TLabel;
    Label5: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    fLetterList: TJvMemoryData;
    dsfLetterList: TJvDataSource;
    fLetterListLETTERNAME: TStringField;
    fLetterListDESCRIPTION: TStringField;
    fLetterListSENDER: TStringField;
    fLetterListRECIPIENT: TIntegerField;
    fLetterListENDLETTERDATE: TDateField;
    fLetterListSTARTLETTERDATE: TDateField;
    fLetterListStartPASSDATE: TDateField;
    fLetterListEndPASSDATE: TDateField;
    fLetterListNumber: TIntegerField;
    fLetterListPostType: TIntegerField;
    JvDBDatePickerEdit1: TJvDBDatePickerEdit;
    fLetterListShortList: TBooleanField;
    JvDBDatePickerEdit2: TJvDBDatePickerEdit;
    JvDBDatePickerEdit3: TJvDBDatePickerEdit;
    JvDBDatePickerEdit4: TJvDBDatePickerEdit;
    JvDBCheckBox1: TJvDBCheckBox;
    DBEdit1: TDBEdit;
    JvDBLookupCombo1: TJvDBLookupCombo;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    JvDBLookupCombo2: TJvDBLookupCombo;
    Label8: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    fLetterListOutNum: TStringField;
    Button1: TButton;
    Button4: TButton;
    ActionManager1: TActionManager;
    aFiltrClear: TAction;
    aFiltrRenew: TAction;
    aNewLetter: TAction;
    aEditLetter: TAction;
    Button2: TButton;
    Button3: TButton;
    tLetterListOBJECTID: TFIBIntegerField;
    tLetterListCARDID: TFIBIntegerField;
    tLetterListCASEID: TFIBIntegerField;
    Button5: TButton;
    aPrintLetterList: TAction;
    Button6: TButton;
    aDeleteLetter: TAction;
    Button7: TButton;
    aParams: TAction;
    rtf: TEkRTF;
    Button8: TButton;
    Action1: TAction;
    aQrCode: TAction;
    Button9: TButton;
    rtfUDF: TEkUDFList;
    procedure aNewLetterExecute(Sender: TObject);
    procedure aEditLetterExecute(Sender: TObject);
    procedure aFiltrClearExecute(Sender: TObject);
    procedure aFiltrRenewExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aPrintLetterListExecute(Sender: TObject);
    procedure aDeleteLetterExecute(Sender: TObject);
    procedure aParamsExecute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure aQrCodeExecute(Sender: TObject);
    procedure rtfUDFFunctions0Calculate(Sender: TObject; Args: TEkUDFArgs;
      ArgCount: Integer; UDFResult: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLetterList: TfrmLetterList;

implementation

uses dMain, fMRLetter, frmLogin, frmPrinLetterList, frmParams, frmReport,
  frmReportList, QuricolAPI, QuricolCode;

{$R *.dfm}

function GetTempFile(const Extension: string): string;
 var
   Buffer: array[0..MAX_PATH] of WideChar;
//   aFile: string;
begin
  Repeat
    GetTempPath(Length(Buffer), Buffer);
    GetTempFileName(Buffer, '~~', 0, Buffer);
    Result := ChangeFileExt(Buffer, Extension);
  Until not FileExists(Result);
end;

procedure TfrmLetterList.aNewLetterExecute(Sender: TObject);
var
  LetterId: Int64;
begin
  if frmMRLetter.CreateIncomingDoc(LetterId)
  then begin
    aFiltrRenew.Execute;
    tLetterList.Locate('OBJECTID', LetterId, []);
  end;
end;

procedure TfrmLetterList.aEditLetterExecute(Sender: TObject);
var
  LetterId: Int64;
begin
  LetterId:=tLetterListOBJECTID.Value;
  if frmMRLetter.EditIncomingDoc(LetterId)
  then begin
    aFiltrRenew.Execute;
    tLetterList.Locate('OBJECTID', LetterId,[]);
  end
end;

procedure TfrmLetterList.aFiltrClearExecute(Sender: TObject);
begin
  fLetterList.Cancel;
  fLetterList.Insert;
  fLetterListSTARTLETTERDATE.Value:=Date;
  fLetterListENDLETTERDATE.Value:=Date;
end;

procedure TfrmLetterList.aFiltrRenewExecute(Sender: TObject);
var
  where, order: string;
begin
  where:='1=1';
  if (fLetterListSTARTLETTERDATE.AsVariant <> NULL) and (fLetterListENDLETTERDATE.AsVariant <> NULL)
  then where:=where+' and LETTERDATE between '''+fLetterListSTARTLETTERDATE.AsString
       +''' and '''+fLetterListENDLETTERDATE.AsString+''''
  else begin
    if fLetterListSTARTLETTERDATE.AsVariant <> NULL
    then where:=where+' and LETTERDATE >= '''+fLetterListSTARTLETTERDATE.AsString+'''';
    if fLetterListENDLETTERDATE.AsVariant <> NULL
    then where:=where+' and LETTERDATE <= '''+fLetterListENDLETTERDATE.AsString+'''';
  end;

  if (fLetterListStartPASSDATE.AsVariant <> NULL) and (fLetterListEndPASSDATE.AsVariant <> NULL)
  then where:=where+' and PASSDATE between '''+fLetterListStartPASSDATE.AsString
       +''' and '''+fLetterListEndPASSDATE.AsString+''''
  else begin
    if fLetterListStartPASSDATE.AsVariant <> NULL
    then where:=where+' and PASSDATE >= '''+fLetterListStartPASSDATE.AsString+'''';
    if fLetterListEndPASSDATE.AsVariant <> NULL
    then where:=where+' and PASSDATE <= '''+fLetterListEndPASSDATE.AsString+'''';
  end;

  if fLetterListLETTERNAME.AsString<>''
  then where:=where+' and upper(LETTERNAME COLLATE PXW_CYRL) like ''%'+AnsiUpperCase(fLetterListLETTERNAME.AsString)+'%''';

  if fLetterListDESCRIPTION.AsString<>''
  then where:=where+' and upper(DESCRIPTION COLLATE PXW_CYRL) like''%'+AnsiUpperCase(fLetterListDESCRIPTION.AsString)+'%''';

  if fLetterListSENDER.AsString<>''
  then where:=where+' and upper(SENDER COLLATE PXW_CYRL) like ''%'+AnsiUpperCase(fLetterListSENDER.AsString)+'%''';

  if fLetterListOutNum.AsString<>''
  then where:=where+' and upper(INCNUMBER COLLATE PXW_CYRL) like''%'+AnsiUpperCase(fLetterListOutNum.AsString)+'%''';

  if fLetterListRECIPIENT.AsVariant<>NULL
  then where:=where+' and RECIPIENT ='+fLetterListRECIPIENT.AsString;

  if fLetterListNumber.AsString<>''
  then where:=where+' and NUMBER ='+fLetterListNumber.AsString;

  if fLetterListPostType.AsString<>''
  then where:=where+' and POSTTYPE ='+fLetterListPostType.AsString;
  if Sequentially = 1 //�������� ���������
    then order := 'l.LETTERDATE, l.NUMBER'
    else order := 'l.LETTERDATE,j.CODE, l.NUMBER';

  try
    tLetterList.ReOpenWP([where, order]);
  except
    ShowMessage('������ � ���������� �������');
  end;
end;

procedure TfrmLetterList.FormShow(Sender: TObject);
begin
  if fLogin.ShowModal<>mrOk then Application.Terminate;

  aFiltrClear.Execute;
  aFiltrRenew.Execute;
end;

procedure TfrmLetterList.rtfUDFFunctions0Calculate(Sender: TObject;
  Args: TEkUDFArgs; ArgCount: Integer; UDFResult: TObject);
var
  qrCode: string;
begin
  With UDFResult as TPicture do
  begin
    //�������� ���
    qRCode :=
          tLetterListCARDID.AsString
          + '-'+ tLetterListCASEID.AsString
          + '-0' //�������
          + '-0' // ��� ������� �� ����
          + '-'+ tLetterListOBJECTID.AsString
          + '-'+ dmMain.GetConfigValue('CourtCode');
    Bitmap := TQRCode.GetBitmapImage(qRCode, 4, 3);
  end;
end;

procedure TfrmLetterList.aPrintLetterListExecute(Sender: TObject);
var LetterPeriod: string;
begin
//  fPrintLetterList.ShowModal;
    rtf.ClearVars;
    rtf.OutFile:=GetTempFile('.rtf');
    rtf.InFile :='LetterIn.rtf';
    LetterPeriod:='';
    if fLetterListStartPASSDATE.Value=fLetterListEndPASSDATE.Value
      then LetterPeriod:=fLetterListEndPASSDATE.AsString+'�.'
      else LetterPeriod:='������ � '+fLetterListStartPASSDATE.AsString+'�.- �� '+fLetterListEndPASSDATE.AsString+'�.';
    rtf.CreateVar('Date', LetterPeriod);
    rtf.ExecuteOpen([tLetterList], SW_SHOWMAXIMIZED);
end;

procedure TfrmLetterList.aQrCodeExecute(Sender: TObject);
begin
  rtf.ClearVars;
  rtf.OutFile:=GetTempFile('.rtf');
  rtf.InFile :='qrCode.rtf';
  rtf.ExecuteOpen([],SW_SHOW);
end;

procedure TfrmLetterList.aDeleteLetterExecute(Sender: TObject);
begin
  if dmMain.IsAdmin
  then  if MessageDlg( '������� �������� �������� �'+
    tLetterListINCOMING.AsString+' �� ����?', mtConfirmation, [mbYes, mbNo], 0) = mrYes
  then begin
    if tLetterListCASEID.AsVariant<>null then ShowMessage('����������� ��������� ��� �� ���������� ������� �� ���������');
    dmMain.dbAdd.Execute('Delete from DocText where LetterId='+tLetterListOBJECTID.AsString);
    dmMain.dbAdd.Execute('Delete from Letter where ObjectId='+tLetterListOBJECTID.AsString);
    dmMain.dbAdd.CommitRetaining;
    aFiltrRenew.Execute;
  end
  else begin end
  else begin
    ShowMessage('� ��� ������������ ���� ��� ���������� ��������');
  end;
end;

procedure TfrmLetterList.aParamsExecute(Sender: TObject);
begin
  if fParam.ShowModal=mrOk
  then with dmMain
  do begin
    tAutoPostEss.OpenWP([]);
    tAutoPostRes.OpenWP([]);
    tAutoPostName.OpenWP([]);
  end;
end;

procedure TfrmLetterList.Action1Execute(Sender: TObject);
var ReportId: integer;
begin
  if frmReportList.GetSelectedReport(100, ReportId)
    then begin
      frmReport.ShowReport(ReportId);
    end;
end;

end.
